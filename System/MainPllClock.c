#include <math.h>
#include <string.h>
#include "MainPllClock.h"

// Setup system clocking from GP_CLKIN pin
static void GP_CLKIN_Setup(MAIN_PLL_CLOCK * const pMPC);
// Setup system clocking from Crystal oscillator
static void CRYSTAL_CLKIN_Setup(MAIN_PLL_CLOCK * const pMPC);
//  Setup Main Pll
static bool SetupMainPll(MAIN_PLL_CLOCK * const pMPC);
//  Main Pll dividers
static bool CalculateMainPllDividers(MAIN_PLL_CLOCK * const pMPC, MAIN_PLL_MODE PllMode);
//  Main Pll dividers for 'Integer Mode'
static bool CalculateMainPllDividersForIntegertMode(MAIN_PLL_CLOCK * const pMPC);
//  Main Pll dividers for 'Direct Mode'
static bool CalculateMainPllDividersForDirectMode(MAIN_PLL_CLOCK * const pMPC);
//  Main Pll dividers for 'Non-integer Mode'
static bool CalculateMainPllDividersForNonIntegertMode(MAIN_PLL_CLOCK * const pMPC);
// ������� ����� ����� ������� ��� �������� ���������� ������� PLL
static bool SelectCalculatedPllFclkout(MAIN_PLL_CLOCK * const pMPC);

// ��������� ������ ��������� PLL
ROOT		MAIN_PLL_CLOCK		MainPllClock;

extern uint32_t	dwDebugArray[1024];


//===================================================================================
// ������� ��������� �������������
//===================================================================================
void InitializeMainPllForCoreClock(MAIN_PLL_CLOCK * const pMPC)
{
	// �������� ��������
	if(pMPC == NULL) return;
	// ������� ��� ���������
	memset(&MainPllClock, 0x0, sizeof(MAIN_PLL_CLOCK));
	// ����������� � �������� ����������� �������
	MainPllClock.InputClockSource			= CLOCK_IN_SOURCE;
	MainPllClock.MinMainPllFclkout		= DESIRED_CORE_CLOCK_FREQ;
	MainPllClock.DesiredMainPllFclkout	= DESIRED_CORE_CLOCK_FREQ;
	MainPllClock.MaxMainPllFclkout		= MAX_CORE_CLOCK_FREQ;
}

//-----------------------------------------------------------------------------------
// ��������� ���������� ������ ��������� PLL ��� ������������ ���� ����������������
//-----------------------------------------------------------------------------------
void SetupMainPllForCoreClock(MAIN_PLL_CLOCK * const pMPC)
{
	// �������� ��������
	if(pMPC == NULL) return;
	// Clockin sources for clock generator
	if(		pMPC->InputClockSource == CLKIN_CLKIN)		GP_CLKIN_Setup(pMPC);
	else if( pMPC->InputClockSource == CLKIN_CRYSTAL)	CRYSTAL_CLKIN_Setup(pMPC);
}

// ----------------------------------------------------------------------------------
// Setup system clocking from GP_CLKIN pin
// ----------------------------------------------------------------------------------
void GP_CLKIN_Setup(MAIN_PLL_CLOCK * const pMPC)
{
	uint32_t CurrentCoreClock;

	// setup GP_CLKIN
	SCU_SFSP4_7_bit.EZI	= 0x1;	// Input buffer enable
	SCU_SFSP4_7_bit.ZIF	= 0x1;	// Input glitch filter enable (GP_CLKIN freq < 30 MHz)
	SCU_SFSP4_7_bit.EHS	= 0x0;	// Select Fast rate
	SCU_SFSP4_7_bit.MODE	= 0x1;	// select GP_CLKIN function on P4[7] pin

	// get current core clock
	CurrentCoreClock = Chip_Clock_GetRate(CLK_MX_MXCORE);

	// setup desired core clock
	if(CurrentCoreClock < 90000000)
	{
		// Switch main system clocking to GP_CLKIN
		Chip_Clock_SetBaseClock(CLK_BASE_MX, CLKIN_CLKIN, true, false);
		// Setup PLL for 100MHz and switch main system clocking
		Chip_Clock_SetupMainPLLHz(CLKIN_CLKIN, EXTERNAL_CLKIN_FREQ_IN, 100 * 1000000, 100 * 1000000);
		// Switch main system clocking to PLL1
		Chip_Clock_SetBaseClock(CLK_BASE_MX, CLKIN_MAINPLL, true, false);
		// Setup PLL for maximum clock
//		Chip_Clock_SetupMainPLLHz(CLKIN_CLKIN, EXTERNAL_CLKIN_FREQ_IN, DesiredCoreClock, MAX_CORE_CLOCK_FREQ);
	}
	if(90000000 < CurrentCoreClock)
	{
		//  Setup Main Pll
		if(!SetupMainPll(pMPC)) return;
		// Switch main system clocking to PLL1
		Chip_Clock_SetBaseClock(CLK_BASE_MX, CLKIN_MAINPLL, true, false);
	}
}

// ----------------------------------------------------------------------------------
// Setup system clocking from Crystal oscillator
// ----------------------------------------------------------------------------------
void CRYSTAL_CLKIN_Setup(MAIN_PLL_CLOCK * const pMPC)
{
/*	uint32_t CurrentCoreClock;

	// get current core clock
	CurrentCoreClock = Chip_Clock_GetRate(CLK_MX_MXCORE);

	// Enable the crystal oscillator
	Chip_Clock_EnableCrystal();

	// setup desired core clock
	if(CurrentCoreClock < 90000000)
	{
		// Switch main system clocking to crystal
		Chip_Clock_SetBaseClock(CLK_BASE_MX, CLKIN_CRYSTAL, true, false);
		// Setup PLL for 100MHz and switch main system clocking
		Chip_Clock_SetupMainPLLHz(CLKIN_CRYSTAL, CRYSTAL_MAIN_FREQ_IN, 100 * 1000000, 100 * 1000000);
		// Switch main system clocking to PLL1
		Chip_Clock_SetBaseClock(CLK_BASE_MX, CLKIN_MAINPLL, true, false);
		// Setup PLL for maximum clock
		Chip_Clock_SetupMainPLLHz(CLKIN_CRYSTAL, CRYSTAL_MAIN_FREQ_IN, DesiredCoreClock, MAX_CORE_CLOCK_FREQ);
	}
	if(90000000 < CurrentCoreClock)
	{
		// Setup PLL for desired core clock
		Chip_Clock_SetupMainPLLHz(CLKIN_CRYSTAL, CRYSTAL_MAIN_FREQ_IN, DesiredCoreClock, MAX_CORE_CLOCK_FREQ);
		// Switch main system clocking to PLL1
		Chip_Clock_SetBaseClock(CLK_BASE_MX, CLKIN_MAINPLL, true, false);
	}*/
}


// ----------------------------------------------------------------------------------
//  Setup Main Pll
// ----------------------------------------------------------------------------------
bool SetupMainPll(MAIN_PLL_CLOCK * const pMPC)
{
	uint32_t i;

	// ������ ������� �������� �������
	pMPC->ClockinFreq = Chip_Clock_GetClockInputHz(pMPC->InputClockSource);

	// �������� ���������� �������� ������ ��� ������� �������
	if(pMPC->InputClockSource == CLKIN_CRYSTAL)
		{ if((pMPC->ClockinFreq < 1000000) || (pMPC->ClockinFreq > 25000000)) return false; }
	else
		{ if((pMPC->ClockinFreq < 1000000) || (pMPC->ClockinFreq > 50000000)) return false; }
	// �������� ���������� �������� ������ ��� ������� ����
	if((pMPC->DesiredMainPllFclkout < 9750000) || (pMPC->DesiredMainPllFclkout > 204000000)) return false;
	//  Main Pll dividers for 'Interger Mode'
	for(i = 0x0; i < MAIN_PLL_MODES_QUANTITY; i++) CalculateMainPllDividers(pMPC, i);
	// ������� �������� ���������� ������� PLL
	if(!SelectCalculatedPllFclkout(pMPC)) return false;
	// ��������� ������ ������ ��������� PLL
	CGU_PLL1_CTRL = pMPC->PllCtrlReg[pMPC->MainPllMode].Value;
	while(!CGU_PLL1_STAT_bit.LOCK) { };
	//
	return true;
}

// ----------------------------------------------------------------------------------
//  Main Pll dividers
// ----------------------------------------------------------------------------------
bool CalculateMainPllDividers(MAIN_PLL_CLOCK * const pMPC, MAIN_PLL_MODE PllMode)
{
	// ����� ����� ������� �������� ������� PLL
	switch(PllMode)
	{
		case INTEGER_MODE_FOR_MAIN_PLL:
		{
			return CalculateMainPllDividersForIntegertMode(pMPC);
		}

		case NON_INTEGER_MODE_FOR_MAIN_PLL:
		{
			return CalculateMainPllDividersForNonIntegertMode(pMPC);
		}

		case DIRECT_MODE_FOR_MAIN_PLL:
		{
			return CalculateMainPllDividersForDirectMode(pMPC);
		}

		default: return false;
	}
}

// ----------------------------------------------------------------------------------
//  Main Pll dividers for 'Integer Mode'
// ----------------------------------------------------------------------------------
bool CalculateMainPllDividersForIntegertMode(MAIN_PLL_CLOCK * const pMPC)
{
	uint32_t m, n, p;
	uint32_t MSel, NSel, PSel;
	float Fclkout, Fcco;
	float delta, deltamin;

	//
	deltamin = pMPC->DesiredMainPllFclkout;
	NSel = MSel = PSel = (uint32_t)(-1);
	// ����������������� �������
	pMPC->CalculatedMainPllFclkouts[INTEGER_MODE_FOR_MAIN_PLL] = 0.0;
	// �������
	for(p = 0x0; p < 4; p++)
	{
		for(n = 0x0; n < 4; n++)
		{
			for(m = 0x0; m < 256; m++)
			{
				Fclkout = (m + 1.0f) * (float)pMPC->ClockinFreq / (n + 1.0f);
				Fcco = 2.0f * (float)(1 << p) * Fclkout;
				if((Fcco < 156000000.0f) || (Fcco > 320000000.0f)) continue;
				delta = fabs(Fclkout - pMPC->DesiredMainPllFclkout);
				if(delta < deltamin)
				{
					deltamin = delta;
					pMPC->MaxDeltaPllFclkouts[INTEGER_MODE_FOR_MAIN_PLL] = delta;
					NSel = n;
					MSel = m;
					PSel = p;

				}
			}
		}
	}
	// �������� ��� ��������
	if((NSel == -1) || (MSel == -1) || (PSel == -1)) return false;
	// ��������� ����������� ������� PLL
	pMPC->PllCtrlReg[INTEGER_MODE_FOR_MAIN_PLL].Bits.BYPASS		= 0x0;
	pMPC->PllCtrlReg[INTEGER_MODE_FOR_MAIN_PLL].Bits.FBSEL		= 0x1;
	pMPC->PllCtrlReg[INTEGER_MODE_FOR_MAIN_PLL].Bits.DIRECT		= 0x0;
	pMPC->PllCtrlReg[INTEGER_MODE_FOR_MAIN_PLL].Bits.PSEL			= PSel;
	pMPC->PllCtrlReg[INTEGER_MODE_FOR_MAIN_PLL].Bits.AUTOBLOCK	= 0x1;
	pMPC->PllCtrlReg[INTEGER_MODE_FOR_MAIN_PLL].Bits.NSEL			= NSel;
	pMPC->PllCtrlReg[INTEGER_MODE_FOR_MAIN_PLL].Bits.MSEL			= MSel;
	pMPC->PllCtrlReg[INTEGER_MODE_FOR_MAIN_PLL].Bits.CLK_SEL		= pMPC->InputClockSource;
	//
	pMPC->CalculatedMainPllFclkouts[INTEGER_MODE_FOR_MAIN_PLL] = (MSel + 1.0f) * (float)pMPC->ClockinFreq / (NSel + 1.0f);
	//
	return true;
}

// ----------------------------------------------------------------------------------
//  Main Pll dividers for 'Non-integer Mode'
// ----------------------------------------------------------------------------------
bool CalculateMainPllDividersForNonIntegertMode(MAIN_PLL_CLOCK * const pMPC)
{
	uint32_t m, n, p;
	uint32_t MSel, NSel, PSel;
	float Fclkout, Fcco;
	float delta, deltamin;

	//
	deltamin = pMPC->DesiredMainPllFclkout;
	NSel = MSel = PSel = (uint32_t)(-1);
	// ����������������� �������
	pMPC->CalculatedMainPllFclkouts[NON_INTEGER_MODE_FOR_MAIN_PLL] = 0.0;
	// �������
	for(p = 0x0; p < 4; p++)
	{
		for(n = 0x0; n < 4; n++)
		{
			for(m = 0x0; m < 256; m++)
			{
				Fcco = (m + 1.0f) * pMPC->ClockinFreq / (n + 1.0f);
				if((Fcco < 156000000.0f) || (Fcco > 320000000.0f)) continue;
				Fclkout = Fcco / (2.0f * (float)(1 << p));
				delta = fabs(Fclkout - pMPC->DesiredMainPllFclkout);
				if(delta < deltamin)
				{
					deltamin = delta;
					pMPC->MaxDeltaPllFclkouts[NON_INTEGER_MODE_FOR_MAIN_PLL] = delta;
					NSel = n;
					MSel = m;
					PSel = p;
				}
			}
		}
	}
	// �������� ��� ��������
	if((NSel == -1) || (MSel == -1) || (PSel == -1)) return false;
	// ��������� ����������� ������� PLL
	pMPC->PllCtrlReg[NON_INTEGER_MODE_FOR_MAIN_PLL].Bits.BYPASS		= 0x0;
	pMPC->PllCtrlReg[NON_INTEGER_MODE_FOR_MAIN_PLL].Bits.FBSEL		= 0x0;
	pMPC->PllCtrlReg[NON_INTEGER_MODE_FOR_MAIN_PLL].Bits.DIRECT		= 0x0;
	pMPC->PllCtrlReg[NON_INTEGER_MODE_FOR_MAIN_PLL].Bits.PSEL		= PSel;
	pMPC->PllCtrlReg[NON_INTEGER_MODE_FOR_MAIN_PLL].Bits.AUTOBLOCK	= 0x1;
	pMPC->PllCtrlReg[NON_INTEGER_MODE_FOR_MAIN_PLL].Bits.NSEL		= NSel;
	pMPC->PllCtrlReg[NON_INTEGER_MODE_FOR_MAIN_PLL].Bits.MSEL		= MSel;
	pMPC->PllCtrlReg[NON_INTEGER_MODE_FOR_MAIN_PLL].Bits.CLK_SEL	= pMPC->InputClockSource;

	//
	pMPC->CalculatedMainPllFclkouts[NON_INTEGER_MODE_FOR_MAIN_PLL] = (MSel + 1.0f) * (float)pMPC->ClockinFreq / ((NSel + 1.0f) * (2.0f * (float)(1 << PSel)));
	//
	return true;
}

// ----------------------------------------------------------------------------------
//  Main Pll dividers for 'Direct Mode'
// ----------------------------------------------------------------------------------
bool CalculateMainPllDividersForDirectMode(MAIN_PLL_CLOCK * const pMPC)
{
	uint32_t m, n;
	uint32_t MSel, NSel;
	float Fclkout, delta, deltamin;

	//
	deltamin = pMPC->DesiredMainPllFclkout;
	NSel = MSel = (uint32_t)(-1);
	//
	pMPC->CalculatedMainPllFclkouts[DIRECT_MODE_FOR_MAIN_PLL] = 0.0;
	// �������
	for(n = 0x0; n < 4; n++)
	{
		for(m = 0x0; m < 256; m++)
		{
			Fclkout = (m + 1.0f) * (float)pMPC->ClockinFreq / (n + 1.0f);
			if((Fclkout < 156000000.0f) || (Fclkout > 204000000.0f)) continue;
			delta = fabs(Fclkout - pMPC->DesiredMainPllFclkout);
			if(delta < deltamin)
			{
				deltamin = delta;
				pMPC->MaxDeltaPllFclkouts[DIRECT_MODE_FOR_MAIN_PLL] = delta;
				NSel = n;
				MSel = m;
			}
		}
	}
	// �������� ��� ��������
	if((NSel == -1) || (MSel == -1)) return false;
	// ��������� ����������� ������� PLL
	pMPC->PllCtrlReg[DIRECT_MODE_FOR_MAIN_PLL].Bits.BYPASS		= 0x0;
	pMPC->PllCtrlReg[DIRECT_MODE_FOR_MAIN_PLL].Bits.FBSEL			= 0x1;
	pMPC->PllCtrlReg[DIRECT_MODE_FOR_MAIN_PLL].Bits.DIRECT		= 0x1;
	pMPC->PllCtrlReg[DIRECT_MODE_FOR_MAIN_PLL].Bits.PSEL			= 0x0;
	pMPC->PllCtrlReg[DIRECT_MODE_FOR_MAIN_PLL].Bits.AUTOBLOCK	= 0x1;
	pMPC->PllCtrlReg[DIRECT_MODE_FOR_MAIN_PLL].Bits.NSEL			= NSel;
	pMPC->PllCtrlReg[DIRECT_MODE_FOR_MAIN_PLL].Bits.MSEL			= MSel;
	pMPC->PllCtrlReg[DIRECT_MODE_FOR_MAIN_PLL].Bits.CLK_SEL		= pMPC->InputClockSource;

	//
	pMPC->CalculatedMainPllFclkouts[DIRECT_MODE_FOR_MAIN_PLL] = (MSel + 1.0f) * (float)pMPC->ClockinFreq / (NSel + 1.0f);
	//
	return true;
}

// ----------------------------------------------------------------------------------
// ������� ����� ����� ��� �������� ���������� ������� PLL
// ----------------------------------------------------------------------------------
bool SelectCalculatedPllFclkout(MAIN_PLL_CLOCK * const pMPC)
{
	uint32_t i, n;
	float deltamin;

	//
	deltamin = pMPC->DesiredMainPllFclkout;
	n = -1;
	// �������
	for(i = 0x0; i < MAIN_PLL_MODES_QUANTITY; i++)
	{
		// ���� �� ������� ���������� �������
		if(pMPC->CalculatedMainPllFclkouts[i] > 1.0)
		{
			// ����� ���� ���������� �� �������� �������
			if(pMPC->MaxDeltaPllFclkouts[i] < deltamin)
			{
				n = i;
				deltamin = pMPC->MaxDeltaPllFclkouts[i];
				if(deltamin < 0.005f) break;
			}
		}
	}
	//
	if(n == -1) return false;
	// �����
	pMPC->MainPllMode				= n;			// ������ �����
	pMPC->RealMainPllFclkout	= pMPC->CalculatedMainPllFclkouts[n];	// ������ �������
	//
	return true;
}
