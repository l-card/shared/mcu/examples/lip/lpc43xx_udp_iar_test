#ifndef _MAIN_PLL_CLOCK_H_
#define _MAIN_PLL_CLOCK_H_

	#include <stdint.h>
	#include <stdbool.h>
	#include <intrinsics.h>
	#include <nxp/iolpc4333.h>
	#include "chip.h"

	// ��������� ������ ���������� ������� �������� ������� ��� ��������� PLL
	typedef enum _MAIN_PLL_MODE_
	{
		INTEGER_MODE_FOR_MAIN_PLL,
		NON_INTEGER_MODE_FOR_MAIN_PLL,
		DIRECT_MODE_FOR_MAIN_PLL,
		INVALID_MAIN_PLL_MODE,
		MAIN_PLL_MODES_QUANTITY = INVALID_MAIN_PLL_MODE
	}	MAIN_PLL_MODE;

	// PLL1_CTRL register
	typedef union
	{
		struct
		{
			uint32_t		PD				: 1;
			uint32_t		BYPASS		: 1;
			uint32_t						: 4;
			uint32_t		FBSEL			: 1;
			uint32_t		DIRECT		: 1;
			uint32_t		PSEL			: 2;
			uint32_t						: 1;
			uint32_t		AUTOBLOCK	: 1;
			uint32_t		NSEL			: 2;
			uint32_t						: 2;
			uint32_t		MSEL			: 8;
			uint32_t		CLK_SEL		: 5;
			uint32_t						: 3;
		}	Bits;
		uint32_t Value;			// ����� �������
	}	PLL1_CTRL_REG;

	// ��������� ��������� ������� �������� ������� ����������������
	typedef struct _MAIN_PLL_CLOCK_
	{
		MAIN_PLL_MODE		OUT_PAR	MainPllMode;			// �������� ���������� ����� ��� ������� �������� ������� ��������� PLL
		CHIP_CGU_CLKIN_T	IN_PAR	InputClockSource;		// ��� ��������� ��������� ������� ��� ��������� PLL
		float		IN_PAR	ClockinFreq;						// ������� �������� ������� ��� ��������� PLL
		float		IN_PAR	MinMainPllFclkout;				// ����������� �������� �������� ������� ��������� PLL
		float		IN_PAR	DesiredMainPllFclkout;			// �������� �������� �������� ������� ��������� PLL
		float		OUT_PAR	RealMainPllFclkout;				// ������� ������������� �������� �������� ������� ��������� PLL
		float		IN_PAR	MaxMainPllFclkout;				// ������������ �������� �������� ������� ��������� PLL
		float		OUT_PAR	CalculatedMainPllFclkouts[MAIN_PLL_MODES_QUANTITY];	// ��������� �������� ������� ��������� PLL ��� ������ ������� ����������
		float		OUT_PAR	MaxDeltaPllFclkouts[MAIN_PLL_MODES_QUANTITY];	// ������������ ���������� ��������� ������� �� ��������
		PLL1_CTRL_REG		PllCtrlReg[MAIN_PLL_MODES_QUANTITY];				// �������� ������������ �������� PLL1 ��� ������� ������
	}	MAIN_PLL_CLOCK;
	// ��������� ������ ��������� PLL
	extern	MAIN_PLL_CLOCK		MainPllClock;

	// ������� ��������� �������������
	void InitializeMainPllForCoreClock(MAIN_PLL_CLOCK * const pMPC);
	// ��������� ���������� ������ ��������� PLL ��� ������������ ���� ����������������
	void SetupMainPllForCoreClock(MAIN_PLL_CLOCK * const pMPC);

#endif		// _MAIN_PLL_CLOCK_H_

