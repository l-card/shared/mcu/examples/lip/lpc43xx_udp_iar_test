#ifndef __SYS_CONFIG_H_
#define __SYS_CONFIG_H_

	// Build for 43xx chip family
	#define CHIP_LPC43XX

	// Build for RMII interface
	#define USE_RMII

	// Un-comment DEBUG_ENABLE for IO support via the UART
//	#define DEBUG_ENABLE

	// Enable DEBUG_SEMIHOSTING along with DEBUG to enable IO support via semihosting
// #define DEBUG_SEMIHOSTING'

	// Board UART used for debug output
//	#define DEBUG_UART LPC_USART3

	// Clockin source for clock generator
//	#define	CLOCK_IN_SOURCE					(CLKIN_CRYSTAL)
	#define	CLOCK_IN_SOURCE					(CLKIN_CLKIN)
	// Crystal frequency into device
	#define	CRYSTAL_MAIN_FREQ_IN				(12000000)			//  in Hz
	// Crystal frequency into device for RTC/32K input
	#define	CRYSTAL_32K_FREQ_IN 				(32768)				//  in Hz
	// Frequency on external clockin pin
	#define	EXTERNAL_CLKIN_FREQ_IN 			(12000000)			 // in Hz

	// Desired Core Clock frequency in Hz
//	#define	DESIRED_CORE_CLOCK_FREQ			(200000000)			 // in Hz
//	#define	DESIRED_CORE_CLOCK_FREQ			(120000000)			 // in Hz
	#define	DESIRED_CORE_CLOCK_FREQ			(100000000)			 // in Hz
	// Default Core Clock frequency in Hz
	#define	MAX_CORE_CLOCK_FREQ				(MAX_CLOCK_FREQ)	//  in Hz

	// Audio and USB default PLL rates (configured in SystemInit())
	#define	CGU_AUDIO_PLL_RATE 				(0)
	#define	CGU_USB_PLL_RATE 					(480000000)

#endif /* __SYS_CONFIG_H_ */
