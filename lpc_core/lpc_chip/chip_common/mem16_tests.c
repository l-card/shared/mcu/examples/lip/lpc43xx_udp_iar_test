// **********************************************************************************
// Generic memory tests
// Various memory tests for testing external memory integrity. Includes
// inverse address, walking bit, and pattern tests.
// **********************************************************************************

#include "mem16_tests.h"

// ----------------------------------------------------------------------------------
// Walking 0 memory test
// ----------------------------------------------------------------------------------
bool mem16_test_walking0(MEM16_TEST_SETUP *pMemSetup)
{
	int i;
	uint16_t *addr;
	uint32_t fbytes;

	i = 0x0;
	fbytes = pMemSetup->bytes;
	addr = pMemSetup->start_addr;

	// Must be 16-bit algined
	if((((uint32_t)addr & 0x1) != 0) || ((fbytes & 0x1) != 0)) return false;

	// Write walking 0 pattern
	while(fbytes > 0)
	{
		*(addr++) = i++;
		fbytes -= sizeof(uint16_t);
/*		*(addr++) = (uint16_t)(~((uint16_t)(1 << i)));
		fbytes -= sizeof(uint16_t);
		i++;
		if (i >= 16) i = 0x0;*/
	}

	// Verify walking 0 pattern
	i = 0x0;
	fbytes = pMemSetup->bytes;
	addr = pMemSetup->start_addr;
	while(fbytes > 0)
	{
//		if((*addr) != (uint16_t)(~((uint16_t)(1 << i))))
		if((*addr) != i)
		{
			pMemSetup->fail_addr = addr;
			pMemSetup->is_val = *addr;
//			pMemSetup->ex_val = ~(1 << i);
			pMemSetup->ex_val = i;
			return false;
		}
		addr++;
		fbytes -= sizeof(uint16_t);
		i++;
//		if (i >= 16) i = 0x0;
	}

	return true;
}

// ----------------------------------------------------------------------------------
// Walking 1 memory test
// ----------------------------------------------------------------------------------
bool mem16_test_walking1(MEM16_TEST_SETUP *pMemSetup)
{
	int i = 0;
	uint16_t *addr;
	uint32_t fbytes;

	i = 0x0;
	fbytes = pMemSetup->bytes;
	addr = pMemSetup->start_addr;

	// Must be 32-bit algined
	if((((uint32_t) addr & 0x3) != 0) || ((fbytes & 0x3) != 0)) return false;

	// Write walking 1 pattern
	while(fbytes > 0)
	{
		*(addr++) = (1 << i);
		fbytes -= sizeof(uint16_t);
		i++;
		if (i >= 32) i = 0x0;
	}

	// Verify walking 1 pattern
	i = 0x0;
	fbytes = pMemSetup->bytes;
	addr = pMemSetup->start_addr;
	while (fbytes > 0)
	{
		if ((*addr) != (1 << i))
		{
			pMemSetup->fail_addr = addr;
			pMemSetup->is_val = *addr;
			pMemSetup->ex_val = (1 << i);
			return false;
		}
		addr++;
		fbytes -= sizeof(uint16_t);
		i++;
		if (i >= 32) i = 0x0;
	}

	return true;
}

// ----------------------------------------------------------------------------------
// Address memory test
// ----------------------------------------------------------------------------------
bool mem16_test_address(MEM16_TEST_SETUP *pMemSetup)
{
	uint16_t *addr;
	uint32_t fbytes;

	fbytes = pMemSetup->bytes;
	addr = pMemSetup->start_addr;

	// Must be 32-bit algined
	if((((uint32_t) addr & 0x3) != 0) || ((fbytes & 0x3) != 0)) return false;

	// Write address for memory location
	while (fbytes > 0)
	{
		*addr =  (uint16_t)((uint32_t)addr & 0x0000FFFF);
		addr++;
		fbytes -= sizeof(uint16_t);
	}

	// Verify address for memory location
	fbytes = pMemSetup->bytes;
	addr = pMemSetup->start_addr;
	while (fbytes > 0)
	{
		if(*addr != (uint16_t)((uint32_t)addr & 0x0000FFFF))
		{
			pMemSetup->fail_addr = addr;
			pMemSetup->is_val = *addr;
			pMemSetup->ex_val = (uint32_t) addr;
			return false;
		}
		addr++;
		fbytes -= sizeof(uint16_t);
	}

	return true;
}

// ----------------------------------------------------------------------------------
// Inverse address memory test
// ----------------------------------------------------------------------------------
bool mem16_test_invaddress(MEM16_TEST_SETUP *pMemSetup)
{
	uint16_t *addr;
	uint32_t fbytes;

	fbytes = pMemSetup->bytes;
	addr = pMemSetup->start_addr;

	// Must be 32-bit algined
	if((((uint32_t) addr & 0x3) != 0) || ((fbytes & 0x3) != 0)) return false;

	// Write inverse address for memory location
	while (fbytes > 0)
	{
		*addr =  ~((uint16_t)((uint32_t)addr & 0x0000FFFF));
		addr++;
		fbytes -= sizeof(uint16_t);
	}

	/* Verify inverse address for memory location */
	fbytes = pMemSetup->bytes;
	addr = pMemSetup->start_addr;
	while (fbytes > 0)
	{
		if (*addr != ~((uint16_t)((uint32_t)addr & 0x0000FFFF)))
		{
			pMemSetup->fail_addr = addr;
			pMemSetup->is_val = *addr;
			pMemSetup->ex_val = ~(uint32_t) addr;
			return false;
		}
		addr++;
		fbytes -= sizeof(uint16_t);
	}

	return true;
}

// ----------------------------------------------------------------------------------
// Pattern memory test
// ----------------------------------------------------------------------------------
bool mem16_test_pattern(MEM16_TEST_SETUP *pMemSetup)
{
	uint16_t pattern = 0x55AA;
	uint16_t *addr;
	uint32_t fbytes;

	fbytes = pMemSetup->bytes;
	addr = pMemSetup->start_addr;

	// Must be 32-bit algined
	if((((uint32_t) addr & 0x3) != 0) || ((fbytes & 0x3) != 0)) return false;

	// Write pattern for memory location
	while(fbytes > 0)
	{
		*(addr++) =  pattern;
		pattern = ~pattern;
		fbytes -= sizeof(uint16_t);
	}

	// Verify pattern for memory location
	pattern = 0x55AA;
	fbytes = pMemSetup->bytes;
	addr = pMemSetup->start_addr;
	while(fbytes > 0)
	{
		if ((*addr) != pattern)
		{
			pMemSetup->fail_addr = addr;
			pMemSetup->is_val = *addr;
			pMemSetup->ex_val = pattern;
			return false;
		}
		addr++;
		pattern = ~pattern;
		fbytes -= sizeof(uint16_t);
	}

	return true;
}

// ----------------------------------------------------------------------------------
// Pattern memory test with seed and increment value
// ----------------------------------------------------------------------------------
bool mem16_test_pattern_seed(MEM16_TEST_SETUP *pMemSetup, uint16_t seed, uint16_t incr)
{
	uint16_t *addr;
	uint32_t fbytes;
	uint16_t pattern = seed;

	fbytes = pMemSetup->bytes;
	addr = pMemSetup->start_addr;

	// Must be 32-bit algined
	if((((uint32_t) addr & 0x3) != 0) || ((fbytes & 0x3) != 0)) return false;

	// Write pattern for memory location
	while(fbytes > 0)
	{
		*(addr++) =  pattern;
		pattern += incr;
		fbytes -= sizeof(uint16_t);
	}

	// Verify pattern for memory location
	pattern = seed;
	fbytes = pMemSetup->bytes;
	addr = pMemSetup->start_addr;
	while(fbytes > 0)
	{
		if((*addr) != pattern)
		{
			pMemSetup->fail_addr = addr;
			pMemSetup->is_val = *addr;
			pMemSetup->ex_val = pattern;
			return false;
		}
		addr++;
		pattern += incr;
		fbytes -= sizeof(uint16_t);
	}

	return true;
}
