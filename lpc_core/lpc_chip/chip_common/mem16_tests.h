#ifndef _MEM16_TESTS_H_
#define _MEM16_TESTS_H_

#include "lpc_types.h"

// 16-bit memory  test address/size and result structure
typedef struct
{
	uint16_t *start_addr;	// Starting address for memory test
	uint32_t bytes;			// Size in bytes for memory test
	uint16_t *fail_addr;		// Failed address of test (returned only if failed)
	uint16_t is_val;			// Failed value of test (returned only if failed)
	uint16_t ex_val;			// Expected value of test (returned only if failed)
}	MEM16_TEST_SETUP;

// Walking 0 memory test
//	Writes a shifting 0 bit pattern to the entire memory range and
//	verifies the result after all memory locations are written
bool mem16_test_walking0(MEM16_TEST_SETUP *pMemSetup);

// Walking 1 memory test
// Writes a shifting 1 bit pattern to the entire memory range and
// verifies the result after all memory locations are written
bool mem16_test_walking1(MEM16_TEST_SETUP *pMemSetup);

// Address memory test
// Writes the address to each memory location and verifies the
// result after all memory locations are written
bool mem16_test_address(MEM16_TEST_SETUP *pMemSetup);

// Inverse address memory test
// Writes the inverse address to each memory location and verifies the
// result after all memory locations are written
bool mem16_test_invaddress(MEM16_TEST_SETUP *pMemSetup);

// Pattern memory test
// Writes the an alternating 0x55/0xAA pattern to each memory location
// and verifies the result after all memory locations are written
bool mem16_test_pattern(MEM16_TEST_SETUP *pMemSetup);

// Pattern memory test with seed and increment value
// Writes the an alternating pattern to each memory location based on a
// passed seed and increment value and verifies the result after all
// memory locations are written
bool mem_test_pattern_seed(MEM16_TEST_SETUP *pMemSetup, uint16_t seed, uint16_t incr);


#endif // _MEM16_TESTS_H_
