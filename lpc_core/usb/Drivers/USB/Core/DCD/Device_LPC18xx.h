#ifndef __USBDEVICE_LPC18XX_H__
#define __USBDEVICE_LPC18XX_H__

		#include "../../../../Common/Common.h"
		#include "../USBController.h"
		#include "../StdDescriptors.h"
		#include "../USBInterrupt.h"
		#include "../HAL/USB0_HAL.h"

/* Enable C linkage for C++ Compilers: */
		#if defined(__cplusplus)
extern "C" {
		#endif

/* Public Interface - May be used in end-application: */
/* Macros: */

/** Mask for the Options parameter of the @ref USB_Init() function. This indicates that the
 *  USB interface should be initialized in full speed (12Mb/s) mode.
 */
			#define USB_DEVICE_OPT_FULLSPEED               (0 << 0)

/* Function Prototypes: */
/**
 *  @brief Sends a Remote Wakeup request to the host. This signals to the host that the device should
 *  be taken out of suspended mode, and communications should resume.
 *
 *  Typically, this is implemented so that HID devices (mice, keyboards, etc.) can wake up the
 *  host computer when the host has suspended all USB devices to enter a low power state.
 *
 *  @note This macro should only be used if the device has indicated to the host that it
 *        supports the Remote Wakeup feature in the device descriptors, and should only be
 *        issued if the host is currently allowing remote wakeup events from the device (i.e.,
 *        the @ref USB_Device_RemoteWakeupEnabled flag is set). When the \c NO_DEVICE_REMOTE_WAKEUP
 *        compile time option is used, this macro is unavailable.
 *        \n\n
 *
 *  @note The USB clock must be running for this function to operate. If the stack is initialized with
 *        the @ref USB_OPT_MANUAL_PLL option enabled, the user must ensure that the PLL is running
 *        before attempting to call this function.
 *
 *  \see @ref Group_StdDescriptors for more information on the RMWAKEUP feature and device descriptors.
 *  @return Nothing.
 */
void USB_Device_SendRemoteWakeup(void);


			#if defined(USB_DEVICE_OPT_LOWSPEED)
static inline void USB_Device_SetLowSpeed(void) ATTR_ALWAYS_INLINE;

static inline void USB_Device_SetLowSpeed(void)
{
	//              UDCON |=  (1 << LSM);
}

static inline void USB_Device_SetFullSpeed(void) ATTR_ALWAYS_INLINE;

static inline void USB_Device_SetFullSpeed(void)
{
	//              UDCON &= ~(1 << LSM);
}

			#endif

/**
 * @brief	Set current USB device address.
 *  @param	corenum		: ID Number of USB Core to be processed.
 *  @param	Address		: new USB device address.
 *  @return Nothing.
 */
static inline void USB_Device_SetDeviceAddress(const uint8_t Address) ATTR_ALWAYS_INLINE;
static inline void USB_Device_SetDeviceAddress(const uint8_t Address)
{
	USB0_DEVICEADDR_bit.USBADRA	= 0x1;
	USB0_DEVICEADDR_bit.USBADR		= Address;

/*	USB_REG(corenum)->DEVICEADDR = USBDEV_ADDR(Address);
	USB_REG(corenum)->DEVICEADDR |= USBDEV_ADDR_AD;*/
}

/**
 * @brief	Get status of USB address whether it is set or not.
 *  @return true if set
 *  		otherwise false.
 */
static inline bool USB_Device_IsAddressSet(void) ATTR_ALWAYS_INLINE ATTR_WARN_UNUSED_RESULT;

static inline bool USB_Device_IsAddressSet(void)
{
	return true;			/* temporarily */
}


/* Disable C linkage for C++ Compilers: */
		#if defined(__cplusplus)
}
		#endif

#endif

/** @} */

