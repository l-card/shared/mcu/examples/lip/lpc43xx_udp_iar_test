#ifndef __ENDPOINT_COMMON_H__
#define __ENDPOINT_COMMON_H__

/* Includes: */
#include "../HAL/USB0_HAL.h"

/* Macros: */
/** Size of share memory buffer that a device uses to communicate with host. */
#define USB_DATA_BUFFER_TEM_LENGTH      512

/* Global Variables: */
/** Share memory buffer. */
/* Control EP buffer */
extern uint8_t usb_data_buffer[][USB_DATA_BUFFER_TEM_LENGTH];
/* Non-Control EP IN buffer */
extern uint8_t usb_data_buffer_IN[][USB_DATA_BUFFER_TEM_LENGTH];
/* Non-Control EP OUT buffer */
extern uint8_t usb_data_buffer_OUT[][USB_DATA_BUFFER_TEM_LENGTH];
/* Control EP buffer size */
extern volatile int32_t usb_data_buffer_size[];
/* Non-Control EP OUT buffer index */
extern volatile uint32_t usb_data_buffer_OUT_size[];
/** Indexer rolling along the share memory buffer. Used to determine the offset
 *  of next read/write activities on share memory buffer or the total amount of data
 *  ready to be sent.
 */
extern volatile uint32_t usb_data_buffer_index[];
extern volatile uint32_t usb_data_buffer_IN_index[];
extern volatile uint32_t usb_data_buffer_OUT_index[];
/** Store the current selected endpoint number, always the logical endpint number.
 *  Usually used as index of endpointhandle array.
 */
extern uint8_t endpointselected[];
/** Array to store the physical endpoint number or the actual endpoint number that need
 *  to be configured for any USB transactions.
 */
extern uint8_t endpointhandle0[];
extern uint8_t endpointhandle1[];

#define endpointhandle(corenum)				((corenum) ? endpointhandle1 : endpointhandle0)
#endif /* __ENDPOINT_COMMON_H__ */

/** @} */
