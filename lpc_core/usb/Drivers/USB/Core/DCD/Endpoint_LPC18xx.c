#include "usb0/Usb0Device.h"
#include "../Endpoint.h"
#include <string.h>

// ���������������� Contol Endpoint
static bool ConfigureControlEndpoint(ENDPOINT_CONFIG_PARS * const EndpointConfigPars);
// ���������������� �� Contol Endpoint
static bool ConfigureEndpoint(ENDPOINT_CONFIG_PARS * const EndpointConfigPars);

//
PRAGMA_ALIGN_2048
volatile	DEVICE_QUEUE_HEAD	dQueueHead[USED_PHYSICAL_ENDPOINTS0] ATTR_ALIGNED(2048) @ ".ahb_sram1";

// ���������� �������� ��� �������� ����� CONTROL IN
PRAGMA_ALIGN_32
DEVICE_TRANSFER_DESCRIPTOR		Control_IN_dTD		ATTR_ALIGNED(32) @ ".ahb_sram1";
// ���������� �������� ��� �������� ����� CONTROL OUT
PRAGMA_ALIGN_32
DEVICE_TRANSFER_DESCRIPTOR		Control_OUT_dTD	ATTR_ALIGNED(32) @ ".ahb_sram1";
// ����������� �������� ��� �������� ����� BULK IN
PRAGMA_ALIGN_32
DEVICE_TRANSFER_DESCRIPTOR		Bulk_IN_dTD			ATTR_ALIGNED(32) @ ".ahb_sram1";
// ����������� �������� ��� �������� ����� BULK OUT
PRAGMA_ALIGN_32
DEVICE_TRANSFER_DESCRIPTOR		Bulk_OUT_dTD		ATTR_ALIGNED(32) @ ".ahb_sram1";

PRAGMA_ALIGN_32
DEVICE_TRANSFER_DESCRIPTOR		Control_ACK_dTD	ATTR_ALIGNED(32) @ ".ahb_sram1";


// ����� ��� �������� ����� CONTROL IN
PRAGMA_ALIGN_4096
uint8_t								Control_IN_Buffer[4096]		ATTR_ALIGNED(32) @ ".ahb_sram1";
// ����� ��� �������� ����� CONTROL OUT
PRAGMA_ALIGN_4096
uint8_t								Control_OUT_Buffer[4096]	ATTR_ALIGNED(32) @ ".ahb_sram1";
// ������ ��� �������� ����� BULK IN
PRAGMA_ALIGN_4096
uint8_t								Bulk_IN_Buffer[0x2][4096]	ATTR_ALIGNED(32) @ ".ahb_sram1";
// ������ ��� �������� ����� BULK OUT
/*PRAGMA_ALIGN_4096
uint8_t								Bulk_OUT_Buffer[0x2][4096]	ATTR_ALIGNED(32) @ ".ahb_sram1";*/

// Device transfer completed event
// Event is required for using the device stack with any RTOS
PRAGMA_WEAK(EVENT_USB_Device_TransferComplete,Dummy_EVENT_USB_Device_TransferComplete)
void EVENT_USB_Device_TransferComplete(int logicalEP, int xfer_in) ATTR_WEAK ATTR_ALIAS(Dummy_EVENT_USB_Device_TransferComplete);

void Dcd_PrepareTD(DEVICE_TRANSFER_DESCRIPTOR *pDTD, DTD_PARS * const dTdPars);


uint32_t UsbHighSpeed;


//extern uint8_t	bDebugArray[1024];


/*void DcdInsertTD(uint32_t head, uint32_t newtd);
void DcdPrepareTD(DEVICE_TRANSFER_DESCRIPTOR *pDTD, uint8_t *pData, uint32_t length, uint8_t IOC);*/

// ----------------------------------------------------------------------------------
// ���������������� Control Endpoints
// ----------------------------------------------------------------------------------
bool Dcd_ConfigureControlEndpoints(void)
{
	bool ConfigSuccess;
	ENDPOINT_CONFIG_PARS ecp;

	ConfigSuccess = true;

	ecp.LogNumber		= ENDPOINT_CONTROL_LOG_NUMBER;
	ecp.Type				= EP_TYPE_CONTROL;
	ecp.Direction		= ENDPOINT_DIR_OUT;
	ecp.MaxPacketSize	= USB_Device_ControlEndpointSize;
	ConfigSuccess 	  &= Dcd_ConfigureEndpoint(&ecp);

	ecp.LogNumber		= ENDPOINT_CONTROL_LOG_NUMBER;
	ecp.Type				= EP_TYPE_CONTROL;
	ecp.Direction		= ENDPOINT_DIR_IN;
	ecp.MaxPacketSize	= USB_Device_ControlEndpointSize;
	ConfigSuccess 	  &= Dcd_ConfigureEndpoint(&ecp);

	return ConfigSuccess;
}

// ----------------------------------------------------------------------------------
// ���������������� ����������� Bulk Endpoints
// ----------------------------------------------------------------------------------
bool Dcd_ConfigureBulkEndpoints(void)
{
	bool ConfigSuccess;
	ENDPOINT_CONFIG_PARS ecp;

	ConfigSuccess = true;

	ecp.LogNumber		= ENDPOINT_BULK_LOG_NUMBER;
	ecp.Type				= EP_TYPE_BULK;
	ecp.Direction		= ENDPOINT_DIR_OUT;
	ecp.MaxPacketSize	= BULK_EP_SIZE;
	ConfigSuccess	  &= Dcd_ConfigureEndpoint(&ecp);

	ecp.LogNumber		= ENDPOINT_BULK_LOG_NUMBER;
	ecp.Type				= EP_TYPE_BULK;
	ecp.Direction		= ENDPOINT_DIR_IN;
	ecp.MaxPacketSize	= BULK_EP_SIZE;
	ConfigSuccess	  &= Dcd_ConfigureEndpoint(&ecp);

	return ConfigSuccess;
}

//-----------------------------------------------------------------------------------
// Configures the specified endpoint number with the given endpoint type, direction, bank size
// and banking mode. Once configured, the endpoint may be read from or written to, depending
// on its direction.
//-----------------------------------------------------------------------------------
bool Dcd_ConfigureEndpoint(ENDPOINT_CONFIG_PARS * const EndpointConfigPars)
{
	if(EndpointConfigPars->LogNumber == ENDPOINT_CONTROL_LOG_NUMBER)
		return ConfigureControlEndpoint(EndpointConfigPars);
	else
		return ConfigureEndpoint(EndpointConfigPars);
}

//-----------------------------------------------------------------------------------
// ���������������� Contol Endpoint
//-----------------------------------------------------------------------------------
static bool ConfigureControlEndpoint(ENDPOINT_CONFIG_PARS * const EndpointConfigPars)
{
	uint32_t EpPhyNumber;
	volatile DEVICE_QUEUE_HEAD * pdQueueHead;

	// �������� ����������
	if(EndpointConfigPars->LogNumber != ENDPOINT_CONTROL_LOG_NUMBER) return false;
	else if(EndpointConfigPars->Type == EP_TYPE_ISOCHRONOUS) return false;

	// ������� ���������� ����� �������� ����� (� ������ ���������� �������� ������)
	EpPhyNumber = 2 * EndpointConfigPars->LogNumber + (EndpointConfigPars->Direction == ENDPOINT_DIR_OUT ? 0x0 : 0x1);
	//
	pdQueueHead = &dQueueHead[EpPhyNumber];
	memset((void *)pdQueueHead, 0x0, sizeof(DEVICE_QUEUE_HEAD));

	// ����������� ��������� ������ ������: ��� FS � HS - 64 �����
	pdQueueHead->MaxPacketSize				= EndpointConfigPars->MaxPacketSize & 0x3FF;
	// � Setup �������� ����� �������� �� �����������
	pdQueueHead->IntOnSetup					= 0x1;
	// ��� ������� ����� ������������ �������������� ���������� �������� ������
	pdQueueHead->ZeroLengthTermination	= DISABLE_ZERO_LENGTH_PACKET;//ENABLE_ZERO_LENGTH_PACKET;
	//
	pdQueueHead->Overlay_dTD.Next_dTD	= NEXT_LINK_POINTER_IS_TERMINATED;
	// ���������� Contol Endpoint ����� � ����������� �� ���������� �������� ������
	if(EndpointConfigPars->Direction == ENDPOINT_DIR_OUT)
	{
		USB0_ENDPTCTRL0 			  &= ~0x0000FFFF;
		USB0_ENDPTCTRL0_bit.RXE		= 0x1;	// Rx endpoint enable
		USB0_ENDPTNAKEN_bit.EPRNE0 = 0x0;
	}
	else if(EndpointConfigPars->Direction == ENDPOINT_DIR_IN)
	{
		USB0_ENDPTCTRL0			  &= ~0xFFFF0000;
		USB0_ENDPTCTRL0_bit.TXE		= 0x1;
		USB0_ENDPTNAKEN_bit.EPTNE0	= 0x0;
	}
	else return false;

	return true;
}

//-----------------------------------------------------------------------------------
// ���������������� �� Contol Endpoint
//-----------------------------------------------------------------------------------
static bool ConfigureEndpoint(ENDPOINT_CONFIG_PARS * const EndpointConfigPars)
{
	uint32_t EpPhyNumber;
	volatile DEVICE_QUEUE_HEAD * pdQueueHead;
	__IO uint32_t 						* pEndPoint;		//
	__IO __usb_endptctrl_reg_bits * pEndPointBits;	// ������ � ������� ����� ������� ����

	// �������� ����������
	if(EndpointConfigPars->LogNumber == ENDPOINT_CONTROL_LOG_NUMBER) return false;
	else if(EndpointConfigPars->Type == EP_TYPE_ISOCHRONOUS) return false;

	// ������� ���������� ����� �������� ����� (� ������ ����������� �������� ������)
	EpPhyNumber = 2 * EndpointConfigPars->LogNumber + (EndpointConfigPars->Direction == ENDPOINT_DIR_OUT ? 0x0 : 0x1);
	//
	pdQueueHead = &dQueueHead[EpPhyNumber];
	memset((void *)pdQueueHead, 0x0, sizeof(DEVICE_QUEUE_HEAD));

	// ����������� ��������� ������ ������
	pdQueueHead->MaxPacketSize				= EndpointConfigPars->MaxPacketSize & 0x3FF;
	// ����� �� ����� Setup �������
	pdQueueHead->IntOnSetup					= 0x0;
	// ��� ������� �� ����� ������������ �������������� ���������� �������� ������
	pdQueueHead->ZeroLengthTermination	= DISABLE_ZERO_LENGTH_PACKET;
	//
	pdQueueHead->Overlay_dTD.Next_dTD	= NEXT_LINK_POINTER_IS_TERMINATED;

	// �������� ����� ������������ �������� ��� �������� �����
	pEndPoint		= (uint32_t *)(&(((__IO uint32_t *)&USB0_ENDPTCTRL0)[EndpointConfigPars->LogNumber]));
	pEndPointBits	= (__usb_endptctrl_reg_bits *)pEndPoint;

	// ���������� �������� ����� � ����������� �� ���������� �������� ������
	if(EndpointConfigPars->Direction == ENDPOINT_DIR_OUT)
	{
		*pEndPoint	&= ~0x0000FFFF;	// reset Tx values
		pEndPointBits->RXT	= EndpointConfigPars->Type;	// set Endpoint type
		pEndPointBits->RXR	= 0x1;	// Rx data toggle reset
		pEndPointBits->RXE	= 0x1;	// Rx endpoint enable
		// �������� ���������� NAK �� BULK OUT
		USB0_ENDPTNAKEN	  |= (0x1 << EP_Physical2BitPosition(EpPhyNumber));
	}
	else if(EndpointConfigPars->Direction == ENDPOINT_DIR_IN)
	{
		*pEndPoint	&= ~0xFFFF0000;	// reset Tx values
		pEndPointBits->TXT	= EndpointConfigPars->Type;	// set Endpoint type
		pEndPointBits->TXR	= 0x1;	// Tx data toggle reset
		pEndPointBits->TXE	= 0x1;	// Tx endpoint enable
	}
	else return false;

	return true;
}

//-----------------------------------------------------------------------------------
// receive a CONTROL OUT packet from the host
//-----------------------------------------------------------------------------------
bool Dcd_Receive_Control_Packet(uint8_t * const Buffer, uint32_t Size)
{
	DTD_PARS dTdPars;
	volatile DEVICE_QUEUE_HEAD * pdQueueHead;

	// �������� ���������
	if(!Buffer) return false;
	else if(Size > 0x1000) return false;

	// -=== Initialize dTD to send OUT packet ===-
	// get pointer to CONTROL OUT QH
	pdQueueHead = &dQueueHead[0x0];
	// ��������� ��������������� ����������� �������� ������
	dTdPars.NextLinkPointer	= 0xDEAD0000;
	dTdPars.LinkTerminate	= NEXT_LINK_POINTER_IS_TERMINATED;
	dTdPars.Totalbytes		= Size;
	dTdPars.IOC					= 0x1;	// ����� ������������ 'Interrupt On Complete'
	dTdPars.BufferPointer	= (uint32_t)Buffer;
	dTdPars.BufferQuantity	= 0x2;
	Dcd_PrepareTD(&Control_OUT_dTD, &dTdPars);

	// Point the endpoint OUT QH to the dTD
	pdQueueHead->Overlay_dTD.Next_dTD = (uint32_t)&Control_OUT_dTD;
	pdQueueHead->IsNakReceived		= false;
	pdQueueHead->IsOutReceived		= false;
	pdQueueHead->TransferCount		= Size;

	// Prime Tx buffer for CONTROL OUT endpoint
	USB0_ENDPTPRIME_bit.PERB0		= 0x1;
	// -================================================-

	// �� ������
	return true;
}

//-----------------------------------------------------------------------------------
// send a CONTROL IN packet to the host
//-----------------------------------------------------------------------------------
bool Dcd_Send_Control_Packet(uint8_t * const Buffer, uint32_t Size)
{
	DTD_PARS dTdPars;
	volatile DEVICE_QUEUE_HEAD * pdQueueHead;

	// �������� ���������
	if(!Buffer) return false;
	else if(Size > 0x1000) return false;

	// -=== Initialize dTD to send IN packet ===-
	// get pointer to CONTROL IN QH
	pdQueueHead = &dQueueHead[0x1];
	// ���� ����� - �������� ������ � ����.����� ��� CONTROL IN
	if(Control_IN_Buffer != Buffer)
		memmove(Control_IN_Buffer, Buffer, Size);
	// ��������� ��������������� ����������� �������� ������
	dTdPars.NextLinkPointer	= 0xDEAD0000;
	dTdPars.LinkTerminate	= NEXT_LINK_POINTER_IS_TERMINATED;
	dTdPars.Totalbytes		= Size;
	dTdPars.IOC					= 0x0;
	dTdPars.BufferPointer	= (uint32_t)Control_IN_Buffer;
	dTdPars.BufferQuantity	= 0x2;
	Dcd_PrepareTD(&Control_IN_dTD, &dTdPars);

	// Point the endpoint IN QH to the dTD
	pdQueueHead->Overlay_dTD.Next_dTD = (uint32_t)&Control_IN_dTD;
	pdQueueHead->IsNakReceived		= false;
	pdQueueHead->IsOutReceived		= false;
	pdQueueHead->TransferCount		= Size;

	// Prime Tx buffer for CONTROL IN endpoint
	USB0_ENDPTPRIME_bit.PETB0 = 0x1;
	// -================================================-

	// -=== Initialize dTD to receive OUT packet  ===-
	// The host won't actually send anything, but a dTD needs
	// to be setup to correctly deal with the 0 byte OUT
	// packet the host sends after receiving the IN data.

	// get pointer to CONTROL OUT QH
	pdQueueHead = &dQueueHead[0x0];
	// ��������� ��������������� ����������� �������� ������
	dTdPars.NextLinkPointer	= 0xDEAD0000;
	dTdPars.LinkTerminate	= NEXT_LINK_POINTER_IS_TERMINATED;
	dTdPars.Totalbytes		= 0x40;
	dTdPars.IOC					= 0x1;	// ����� ������������ 'Interrupt On Complete'
	dTdPars.BufferPointer	= (uint32_t)Control_OUT_Buffer;
	dTdPars.BufferQuantity	= 0x1;
	Dcd_PrepareTD(&Control_OUT_dTD, &dTdPars);

	// Point the endpoint OUT QH to the dTD
	pdQueueHead->Overlay_dTD.Next_dTD = (uint32_t)&Control_OUT_dTD;
	pdQueueHead->IsNakReceived		= false;
	pdQueueHead->IsOutReceived		= false;
	pdQueueHead->TransferCount		= dTdPars.Totalbytes;

	// Prime Rx buffer for control endpoint
	USB0_ENDPTPRIME_bit.PERB0 = 0x1;
	// -=============================================-

	// �� ������
	return true;
}

//-----------------------------------------------------------------------------------
// send an zero length CONTROL IN packet to the host
//-----------------------------------------------------------------------------------
void Dcd_Send_Zero_Length_Control_Packet(void)
{
	DTD_PARS dTdPars;
	volatile DEVICE_QUEUE_HEAD * pdQueueHead;

	// -=== Initialize dTD to send IN packet ===-
	// get pointer to CONTROL IN QH
	pdQueueHead = &dQueueHead[0x1];
	// ��������� ��������������� ����������� �������� ������
	dTdPars.NextLinkPointer	= 0xDEAD0000;
	dTdPars.LinkTerminate	= NEXT_LINK_POINTER_IS_TERMINATED;
	dTdPars.Totalbytes		= 0x0;
	dTdPars.IOC					= 0x0;
	dTdPars.BufferPointer	= (uint32_t)Control_IN_Buffer;
	dTdPars.BufferQuantity	= 0x1;
	Dcd_PrepareTD(&Control_IN_dTD, &dTdPars);

	// Point the endpoint IN QH to the dTD
	pdQueueHead->Overlay_dTD.Next_dTD = (uint32_t)&Control_IN_dTD;
	pdQueueHead->IsNakReceived		= false;
	pdQueueHead->IsOutReceived		= false;
	pdQueueHead->TransferCount		= dTdPars.Totalbytes;

	// Prime Tx buffer for CONTROL IN endpoint
	USB0_ENDPTPRIME_bit.PETB0 = 0x1;
	// -=========================================-
}

//-----------------------------------------------------------------------------------
// send an BULK IN packet to the host
//-----------------------------------------------------------------------------------
bool Dcd_Send_Bulk_Packet(uint8_t * const Buffer, uint32_t Size)
{
	uint32_t EpPhyNumber;
	DTD_PARS dTdPars;
	volatile DEVICE_QUEUE_HEAD * pdQueueHead;

	// �������� ���������
	if(!Buffer) return false;
	else if(Size > 0x1000) return false;

	// -=== Initialize dTD to send BULK IN packet ===-
	// ������� ���������� ����� �������� ����� (� ������ ����������� �������� ������)
	EpPhyNumber = 2 * ENDPOINT_BULK_LOG_NUMBER + 0x1;
	// get pointer to CONTROL IN QH
	pdQueueHead = &dQueueHead[EpPhyNumber];
	// �������� ������ � ����.����� ��� CONTROL IN
//	memcpy(Bulk_IN_Buffer, Buffer, Size);
	// ��������� ��������������� ����������� �������� ������
	dTdPars.NextLinkPointer	= 0xDEAD0000;
	dTdPars.LinkTerminate	= NEXT_LINK_POINTER_IS_TERMINATED;
	dTdPars.Totalbytes		= Size;
	dTdPars.IOC					= 0x1;	// ����� ������������ 'Interrupt On Complete'
	dTdPars.BufferPointer	= (uint32_t)Buffer;
//	dTdPars.BufferPointer	= (uint32_t)Bulk_IN_Buffer;
	dTdPars.BufferQuantity	= 0x5;
	Dcd_PrepareTD(&Bulk_IN_dTD, &dTdPars);

	// Point the endpoint BULK IN QH to the dTD
	pdQueueHead->Overlay_dTD.Next_dTD = (uint32_t)&Bulk_IN_dTD;
	pdQueueHead->IsNakReceived		= false;
	pdQueueHead->IsOutReceived		= false;
	pdQueueHead->TransferCount		= dTdPars.Totalbytes;

	// Prime Tx buffer for BULK IN endpoint
	USB0_ENDPTPRIME_bit.PETB2 = 0x1;
	// -================================================-

	// �� ������
	return true;
}

//-----------------------------------------------------------------------------------
// receive an BULK OUT packet from the host
//-----------------------------------------------------------------------------------
bool Dcd_Receive_Bulk_Packet(uint8_t * const Buffer, uint32_t Size)
{
	uint32_t EpPhyNumber;
	DTD_PARS dTdPars;
	volatile DEVICE_QUEUE_HEAD * pdQueueHead;

	// �������� ���������
	if(!Buffer) return false;
	else if(Size > 0x1000) return false;

	// -=== Initialize dTD to receive an BULK OUT ===-
	// ������� ���������� ����� �������� ����� (� ������ ����������� �������� ������)
	EpPhyNumber = 2 * ENDPOINT_BULK_LOG_NUMBER + 0x0;
	// get pointer to CONTROL OUT QH
	pdQueueHead = &dQueueHead[EpPhyNumber];
	// ��������� ��������������� ����������� �������� ������
	dTdPars.NextLinkPointer	= 0xDEAD0000;
	dTdPars.LinkTerminate	= NEXT_LINK_POINTER_IS_TERMINATED;
	dTdPars.Totalbytes		= Size;
	dTdPars.IOC					= 0x1;	// ����� ������������ 'Interrupt On Complete'
	dTdPars.BufferPointer	= (uint32_t)Buffer;
	dTdPars.BufferQuantity	= 0x5;
	Dcd_PrepareTD(&Bulk_OUT_dTD, &dTdPars);

	// Point the endpoint OUT QH to the dTD
	pdQueueHead->Overlay_dTD.Next_dTD = (uint32_t)&Bulk_OUT_dTD;
	pdQueueHead->IsNakReceived		= false;
	pdQueueHead->IsOutReceived		= false;
	pdQueueHead->TransferCount		= dTdPars.Totalbytes;

	// Prime Tx buffer for BULK OUT endpoint
	USB0_ENDPTPRIME_bit.PERB2 = 0x1;
	// -=============================================-

	// �� ������
	return true;
}


//-----------------------------------------------------------------------------------
// ���������� Device Transfer Descriptor (dTD)
//-----------------------------------------------------------------------------------
void Dcd_PrepareTD(DEVICE_TRANSFER_DESCRIPTOR *pDTD, DTD_PARS * const dTdPars)
{
	uint8_t i;

	pDTD->Next_dTD			= dTdPars->NextLinkPointer & (~0x0000001F);
	pDTD->Next_dTD		  |= dTdPars->LinkTerminate;
	pDTD->TotalBytes		= dTdPars->Totalbytes;
	pDTD->IntOnComplete	= dTdPars->IOC;
	pDTD->MultiplierOverride = 0x0;
	pDTD->Active			= 0x1;
	pDTD->Halted			= 0x0;
	if(!dTdPars->BufferQuantity) dTdPars->BufferQuantity = 0x1;
	for(i = 0x0; i < 0x5; i++)
	{
		if(i == 0x0) pDTD->BufferPage[i] = (uint32_t)dTdPars->BufferPointer;
		else if(i < dTdPars->BufferQuantity)
			pDTD->BufferPage[i] = ((uint32_t)dTdPars->BufferPointer + i * 0x1000) & 0xFFFFF000;
		else
			pDTD->BufferPage[i] = 0x00000000UL;
	}
}


/*
//-----------------------------------------------------------------------------------
//
//-----------------------------------------------------------------------------------
void DcdInsertTD(uint32_t head, uint32_t newtd)
{
	DEVICE_TRANSFER_DESCRIPTOR *pTD = (DEVICE_TRANSFER_DESCRIPTOR *) head;
	while(!(pTD->Next_dTD & NEXT_LINK_POINTER_IS_TERMINATED)) pTD = (DEVICE_TRANSFER_DESCRIPTOR *) pTD->Next_dTD;
	pTD->Next_dTD = newtd;
}

//-----------------------------------------------------------------------------------
//
//-----------------------------------------------------------------------------------
void DcdPrepareTD(DEVICE_TRANSFER_DESCRIPTOR *pDTD, uint8_t *pData, uint32_t Length, uint8_t IOC)
{
	// Zero out the device transfer descriptors
	memset((void *) pDTD, 0x0, sizeof(DEVICE_TRANSFER_DESCRIPTOR));

	pDTD->Next_dTD			= NEXT_LINK_POINTER_IS_TERMINATED;
	pDTD->TotalBytes		= Length;
	pDTD->IntOnComplete	= IOC;
	pDTD->Active			= 0x1;
	pDTD->BufferPage[0]	= (uint32_t) pData;
	pDTD->BufferPage[1]	= ((uint32_t) pData + 0x1000) & 0xfffff000;
	//	pDTD->BufferPage[2] = ((uint32_t) pData + 0x2000) & 0xfffff000;
	//	pDTD->BufferPage[3] = ((uint32_t) pData + 0x3000) & 0xfffff000;
	//	pDTD->BufferPage[4] = ((uint32_t) pData + 0x4000) & 0xfffff000;
}
*/
/*
//-----------------------------------------------------------------------------------
//
//-----------------------------------------------------------------------------------
bool DcdDataTransfer(uint8_t PhyEP, uint8_t *pData, uint32_t Length)
{
	DEVICE_TRANSFER_DESCRIPTOR * pDTD = (DEVICE_TRANSFER_DESCRIPTOR *) &dTransferDescriptor[PhyEP];
	volatile DEVICE_QUEUE_HEAD * pdQueueHead = &dQueueHead[PhyEP];
	__IO __usb_endptctrl_reg_bits * pEndPointBits;	// ������ � ������� ����� ������� ����

	while(USB0_ENDPTSTAT & _BIT(EP_Physical2BitPosition(PhyEP)))
	{
		// Endpoint is already primed
	}

	// Zero out the device transfer descriptors
	memset((void *)pDTD, 0x0, sizeof(DEVICE_TRANSFER_DESCRIPTOR));

	// �������� ����� ������������ �������� ��� �������� �����
	pEndPointBits	= (__usb_endptctrl_reg_bits *)(&(((__IO uint32_t *)&USB0_ENDPTCTRL0)[PhyEP/2]));
	//
	if(PhyEP & 0x1)
	{
		if(pEndPointBits->TXT == EP_TYPE_ISOCHRONOUS) return false;
	}
	else if(pEndPointBits->RXT == EP_TYPE_ISOCHRONOUS) return false;

	// The next DTD pointer is INVALID
	pDTD->Next_dTD			= NEXT_LINK_POINTER_IS_TERMINATED;

	pDTD->TotalBytes		= Length;
	pDTD->IntOnComplete	= 0x1;
	pDTD->Active			= 0x1;

	pDTD->BufferPage[0] = (uint32_t) pData;
	pDTD->BufferPage[1] = ((uint32_t) pData + 0x1000) & 0xfffff000;
	pDTD->BufferPage[2] = ((uint32_t) pData + 0x2000) & 0xfffff000;
	pDTD->BufferPage[3] = ((uint32_t) pData + 0x3000) & 0xfffff000;
	pDTD->BufferPage[4] = ((uint32_t) pData + 0x4000) & 0xfffff000;

	//
	pdQueueHead->Overlay_dTD.Halted		= 0x0;		// this should be in USBInt
	pdQueueHead->Overlay_dTD.Active		= 0x0;		// this should be in USBInt
	pdQueueHead->Overlay_dTD.Next_dTD	= (uint32_t) &dTransferDescriptor[PhyEP];

	//
	pdQueueHead->TransferCount = Length;

	// prime the endpoint for transmit
	USB0_ENDPTPRIME |= _BIT(EP_Physical2BitPosition(PhyEP));

	return true;
}
*/
//-----------------------------------------------------------------------------------
//
//-----------------------------------------------------------------------------------
void TransferCompleteISR(void)
{
	uint32_t EpPhyNumber;
	uint32_t ENDPTCOMPLETE;

	ENDPTCOMPLETE = USB0_ENDPTCOMPLETE;
	USB0_ENDPTCOMPLETE = ENDPTCOMPLETE;

	if(ENDPTCOMPLETE)
	{
		// -=== CONTROL OUT ===-
		EpPhyNumber = 2 * ENDPOINT_CONTROL_LOG_NUMBER + 0x0;
		if(ENDPTCOMPLETE & (0x1 << EP_Physical2BitPosition(EpPhyNumber)))
		{
			volatile DEVICE_QUEUE_HEAD * pdQueueHead;

			pdQueueHead = &dQueueHead[EpPhyNumber];
			if(pdQueueHead->Overlay_dTD.TotalBytes == 0x0)
			{
				pdQueueHead->IsOutReceived = true;
			}
		}

		// -=== BULK OUT ===-
		EpPhyNumber = 2 * ENDPOINT_BULK_LOG_NUMBER + 0x0;
		if(ENDPTCOMPLETE & (0x1 << EP_Physical2BitPosition(EpPhyNumber)))
		{
			// ������� ������ ��������� Rx dTD
			CodecFifoRxPars.IsTdBusy	= false;
			// �������� ���������� NAK �� BULK OUT
			USB0_ENDPTNAKEN |= (0x1 << EP_Physical2BitPosition(2*ENDPOINT_BULK_LOG_NUMBER + 0x0));
		}

		// -=== BULK IN ===-
		EpPhyNumber = 2 * ENDPOINT_BULK_LOG_NUMBER + 0x1;
		if(ENDPTCOMPLETE & (0x1 << EP_Physical2BitPosition(EpPhyNumber)))
		{
			// ������� ������ ��������� Tx dTD
			CodecFifoTxPars.IsTdBusy	= false;
			Ad7895FifoPars.IsTdBusy		= false;
			// �������� ���������� NAK �� BULK IN
			USB0_ENDPTNAKEN |= (0x1 << EP_Physical2BitPosition(2*ENDPOINT_BULK_LOG_NUMBER + 0x1));
		}
	}
}

//-----------------------------------------------------------------------------------
//
//-----------------------------------------------------------------------------------
void Endpoint_GetSetupPackage(uint8_t *pData)
{
	volatile DEVICE_QUEUE_HEAD * pdQueueHead = &dQueueHead[0];

	// clear corresponding bit ENDPTSETUPSTAT
	USB0_ENDPTSETUPSTAT = USB0_ENDPTSETUPSTAT;
	// Setup Tripwire bit (SUTW)
	USB0_USBCMD_bit.SUTW		= 0x1;
	// copy Setup Buffer
	memcpy(pData, (void *)pdQueueHead->SetupPackage, 0x8);
	// wait for SUTW to set
	while(!USB0_USBCMD_bit.SUTW) { };
	// clear Setup Tripwire bit (SUTW)
	USB0_USBCMD_bit.SUTW		= 0x0;
}

//-----------------------------------------------------------------------------------
//
//-----------------------------------------------------------------------------------
void DcdIrqHandler(void)
{
	uint32_t USBSTS_D;
	uint32_t t = USB0_USBINTR_D;
	__IO __usb_usbsts_reg_bits * pUSBSTS_D_bit = (__usb_usbsts_reg_bits *)&USBSTS_D;

	// Device Interrupt Status
	USBSTS_D = USB0_USBSTS_D & t;
	// avoid to clear disabled interrupt source
	if(USBSTS_D == 0x0) return;
	// Acknowledge Interrupt
	USB0_USBSTS_D = USBSTS_D;

	// Process Interrupt Sources
	if(pUSBSTS_D_bit->UI)
	{	// USB interrupt

		if(USB0_ENDPTSETUPSTAT)
		{
			//	memcpy(SetupPackage, dQueueHead[0].SetupPackage, 8);
			// Will be cleared by Endpoint_ClearSETUP
		}

		if(USB0_ENDPTCOMPLETE)
		{
			TransferCompleteISR();
		}
	}

	// NAK
	if(pUSBSTS_D_bit->NAKI)
	{
		uint32_t ENDPTNAK = USB0_ENDPTNAK;
      uint32_t en = USB0_ENDPTNAKEN;

      ENDPTNAK &= en;
		USB0_ENDPTNAK = ENDPTNAK;

		// handle NAK interrupts
		if(ENDPTNAK)
		{
			uint32_t EpPhyNumber;
			volatile DEVICE_QUEUE_HEAD * pdQueueHead;

			// BULK OUT Endpoint is NAKed
			EpPhyNumber = 2 * ENDPOINT_BULK_LOG_NUMBER + 0x0;
			if(ENDPTNAK & (0x1 << EP_Physical2BitPosition(EpPhyNumber)))
			{
				//
				pdQueueHead = &dQueueHead[EpPhyNumber];
				// ������ ������ � ������� �� BULK OUT
				pdQueueHead->IsNakReceived = true;
				// ��������� ���������� NAK �� BULK OUT
				USB0_ENDPTNAKEN &= ~(0x1 << EP_Physical2BitPosition(EpPhyNumber));
			}

			// BULK IN Endpoint is NAKed
			EpPhyNumber = 2 * ENDPOINT_BULK_LOG_NUMBER + 0x1;
			if(ENDPTNAK & (0x1 << EP_Physical2BitPosition(EpPhyNumber)))
			{
				//
				pdQueueHead = &dQueueHead[EpPhyNumber];
				// ������ ������ ��� ������ �� BULK IN
				pdQueueHead->IsNakReceived = true;
				// ��������� ���������� NAK �� BULK IN
				USB0_ENDPTNAKEN &= ~(0x1 << EP_Physical2BitPosition(EpPhyNumber));
			}
		}
	}

	// Start of Frame Interrupt
	if(pUSBSTS_D_bit->SRI)
	{
//		EVENT_USB_Device_StartOfFrame();
	}

	// USB reset received
	if(pUSBSTS_D_bit->URI)
	{
		HAL_USB0Reset();
		USB_DeviceState = DEVICE_STATE_Default;
		// ���������������� Control Endpoints
		Dcd_ConfigureControlEndpoints();
	}

	// Suspend
	if(pUSBSTS_D_bit->SLI)
	{
	}

	// Port change detect
	if(pUSBSTS_D_bit->PCI)
	{
		UsbHighSpeed = 0x1;
	}

	// USB error interrupt
	if(pUSBSTS_D_bit->UEI)
	{
	}
}


/*********************************************************************//**
 * @brief		Dummy USB device transfer complete event
 * @param[in]   logicalEP Logical endpoint number
 * @param[in]	xfer_in If this argument is 0 then xfer type is out, else in
 * @note		This event is required for running the stack with RTOS
 *      		and so it should never be removed!
 * @return	 	None
 **********************************************************************/
void Dummy_EVENT_USB_Device_TransferComplete(int logicalEP, int xfer_in)
{
	/**
	 * This is a dummy function
	 * If xfer_in zero then the endpoint it OUT
	 * else ep is IN.
	 **/
}
