#include "DeviceStandardReq.h"
#include "usb0/Usb0Device.h"
#include "adau1361/adau1361.h"
#include "ad7895/ad7895.h"

static void USB_Device_SetAddress(void);
static void USB_Device_SetConfiguration(void);
static void USB_Device_GetConfiguration(void);
static void USB_Device_GetDescriptor(void);
static void USB_Device_GetStatus(void);
static void USB_Device_ClearSetFeature(void);

static void Ven_Request_ResetModule(void);
static void Ven_Request_PutArray(void);
static void Ven_Request_GetArray(void);
static void Ven_Request_CodecStart(void);
static void Ven_Request_AdcStart(void);
static void Ven_Request_GetUsbSpeed(void);
static void Ven_Request_CodecStop(void);
static void Ven_Request_AdcStop(void);
static void Ven_Request_GetModuleName(void);
static void Ven_Request_GetSerialNumber(void);


// -=== USB Device Requests: bmRequestType ===-
typedef union _BM_REQUEST_TYPE_
{
	struct
	{
		uint8_t	DataTransferDirection	: 5;
		uint8_t	Type							: 2;
		uint8_t	Recipient					: 1;
	}	BITMAP;
	uint8_t Value;
}	BM_REQUEST_TYPE;

typedef enum _REQUEST_DIR_
{
	REQUEST_DIR_HOST_TO_DEVICE,
	REQUEST_DIR_DEVICE_TO_HOST
}	REQUEST_DIR;

typedef enum _REQUEST_TYPE_
{
	REQUEST_TYPE_STANDARD,
	REQUEST_TYPE_CLASS,
	REQUEST_TYPE_VENDOR
}	REQUEST_TYPE;

typedef enum _REQUEST_RECIPIENT_
{
	REQUEST_RECIPIENT_DEVICE,
	REQUEST_RECIPIENT_INTERFACE,
	REQUEST_RECIPIENT_ENDPOINT,
	REQUEST_RECIPIENT_OTHER
}	REQUEST_RECIPIENT;
// -==========================================-

typedef enum _VENDOR_REQUEST_NUMBER_
{
	VENDOR_REQUEST_RESET_MODULE,
	VENDOR_REQUEST_PUT_ARRAY,
	VENDOR_REQUEST_GET_ARRAY,
	VENDOR_REQUEST_CODEC_START,
	//
	VENDOR_REQUEST_ADC_START,
	VENDOR_REQUEST_GET_USB_SPEED		= 6,
	VENDOR_REQUEST_CODEC_STOP			= 9,
	VENDOR_REQUEST_ADC_STOP				= 10,
	VENDOR_REQUEST_GET_MODULE_NAME	= 11,
	VENDOR_REQUEST_GET_SERIAL_NUMBER	= 13
}	VENDOR_REQUEST_NUMBER;

typedef enum
{
	IS_CODEC_IN_START		= 0x1,
	IS_CODEC_OUT_START	= 0x2
} CODEC_ATTRIBURES;


uint8_t USB_Device_ConfigurationNumber;

/*PRAGMA_ALIGN_4096
uint32_t	InBuffer[0x1000]	 @	".sdram0";
PRAGMA_ALIGN_4096
uint32_t	OutBuffer[0x1000]	 @	".sdram0";*/

/*
#if !defined(NO_DEVICE_SELF_POWER)
bool    USB_Device_CurrentlySelfPowered;
#endif

#if !defined(NO_DEVICE_REMOTE_WAKEUP)
bool    USB_Device_RemoteWakeupEnabled;
#endif
*/

//extern	uint32_t	dwDebugArray[1024];

//-----------------------------------------------------------------------------------
// ���������� SETUP �����
//-----------------------------------------------------------------------------------
void USB_Device_ProcessSetupRequest(void)
{
	BM_REQUEST_TYPE	bmRequestType;

	//
	Endpoint_GetSetupPackage((uint8_t *)&USB_ControlRequest);
	//
	EVENT_USB_Device_ControlRequest();

	// ���������� �������
	bmRequestType.Value = USB_ControlRequest.bmRequestType;

	// ����������� USB �������
	if(bmRequestType.BITMAP.Type == REQUEST_TYPE_STANDARD)
	{
		switch(USB_ControlRequest.bRequest)
		{
			case REQ_GetStatus:
			{
				if((bmRequestType.Value == (REQDIR_DEVICETOHOST | REQTYPE_STANDARD | REQREC_DEVICE)) ||
					(bmRequestType.Value == (REQDIR_DEVICETOHOST | REQTYPE_STANDARD | REQREC_ENDPOINT)))
				{
					USB_Device_GetStatus();
				}

				break;
			}

			case REQ_ClearFeature:
			case REQ_SetFeature:
			{
				if((bmRequestType.Value == (REQDIR_HOSTTODEVICE | REQTYPE_STANDARD | REQREC_DEVICE)) ||
					(bmRequestType.Value == (REQDIR_HOSTTODEVICE | REQTYPE_STANDARD | REQREC_ENDPOINT)))
				{
					USB_Device_ClearSetFeature();
				}

				break;
			}

			case REQ_SetAddress:
			{
				if(bmRequestType.Value == (REQDIR_HOSTTODEVICE | REQTYPE_STANDARD | REQREC_DEVICE))
				  USB_Device_SetAddress();
				break;
			}

			case REQ_GetDescriptor:
			{
				if((bmRequestType.Value == (REQDIR_DEVICETOHOST | REQTYPE_STANDARD | REQREC_DEVICE)) ||
					(bmRequestType.Value == (REQDIR_DEVICETOHOST | REQTYPE_STANDARD | REQREC_INTERFACE)))
				{
					USB_Device_GetDescriptor();
				}

				break;
			}

			case REQ_GetConfiguration:
			{
				if(bmRequestType.Value == (REQDIR_DEVICETOHOST | REQTYPE_STANDARD | REQREC_DEVICE))
				  USB_Device_GetConfiguration();

				break;
			}

			case REQ_SetConfiguration:
			{
				if(bmRequestType.Value == (REQDIR_HOSTTODEVICE | REQTYPE_STANDARD | REQREC_DEVICE))
				  USB_Device_SetConfiguration();

				break;
			}
		}
	}
	// ���������������� �������
	if(bmRequestType.BITMAP.Type == REQUEST_TYPE_VENDOR)
	{
		switch(USB_ControlRequest.bRequest)
		{
			case VENDOR_REQUEST_RESET_MODULE:
			{
				Ven_Request_ResetModule();
				break;
			}

			case VENDOR_REQUEST_PUT_ARRAY:
			{
				Ven_Request_PutArray();
				break;
			}

			case VENDOR_REQUEST_GET_ARRAY:
			{
				Ven_Request_GetArray();
				break;
			}

			case VENDOR_REQUEST_CODEC_START:
			{
				Ven_Request_CodecStart();
				break;
			}

			case VENDOR_REQUEST_ADC_START:
			{
				Ven_Request_AdcStart();
				break;
			}

			case VENDOR_REQUEST_GET_USB_SPEED:
			{
				Ven_Request_GetUsbSpeed();
				break;
			}

			case VENDOR_REQUEST_CODEC_STOP:
			{
				Ven_Request_CodecStop();
				break;
			}

			case VENDOR_REQUEST_ADC_STOP:
			{
				Ven_Request_AdcStop();
				break;
			}

			case VENDOR_REQUEST_GET_MODULE_NAME:
			{
				Ven_Request_GetModuleName();
				break;
			}

			case VENDOR_REQUEST_GET_SERIAL_NUMBER:
			{
				Ven_Request_GetSerialNumber();
				break;
			}
		}
	}
/*	if (Endpoint_IsSETUPReceived(corenum))
	{
		Endpoint_ClearSETUP(corenum);
		Endpoint_StallTransaction(corenum);
	}*/
}

//-----------------------------------------------------------------------------------
// ���������� CONTROL OUT �����
//-----------------------------------------------------------------------------------
void USB_Device_ProcessControlOutRequest(void)
{
	BM_REQUEST_TYPE	bmRequestType;

	// ���������� �������
	bmRequestType.Value = USB_ControlRequest.bmRequestType;

	// ����������� USB �������
	if(bmRequestType.BITMAP.Type == REQUEST_TYPE_STANDARD)
	{
		switch(USB_ControlRequest.bRequest)
		{
/*			case REQ_GetStatus:
			{
				break;
			}*/

			default:
			{
			}
		}
	}
	// ���������������� �������
	if(bmRequestType.BITMAP.Type == REQUEST_TYPE_VENDOR)
	{
		switch(USB_ControlRequest.bRequest)
		{
			case VENDOR_REQUEST_PUT_ARRAY:
			{
				if(CALLBACK_USB_PutArray(USB_ControlRequest) == NO_DESCRIPTOR)
				{
					Endpoint_StallTransaction(2 * ENDPOINT_CONTROL_LOG_NUMBER + 0x0);
				}
				else
				{
					// acknowledge
					Dcd_Send_Zero_Length_Control_Packet();
				}
				break;
			}

			default:
			{
			}
		}
	}
}

//-----------------------------------------------------------------------------------
//
//-----------------------------------------------------------------------------------
static void USB_Device_SetAddress(void)
{
	uint8_t DeviceAddress;

	DeviceAddress  = USB_ControlRequest.wValue & 0x7F;
	USB_Device_SetDeviceAddress(DeviceAddress);
	Dcd_Send_Zero_Length_Control_Packet();

/*	uint8_t    DeviceAddress    = (USB_ControlRequest.wValue & 0x7F);
	uint_reg_t CurrentGlobalInt = GetGlobalInterruptMask();

	Endpoint_ClearSETUP();

//	Endpoint_ClearStatusStage(corenum);

	while(!(Endpoint_IsINReady(corenum)));

	USB_Device_SetDeviceAddress(corenum, DeviceAddress);
	USB_DeviceState[corenum] = (DeviceAddress) ? DEVICE_STATE_Addressed : DEVICE_STATE_Default;*/
}

//-----------------------------------------------------------------------------------
//
//-----------------------------------------------------------------------------------
static void USB_Device_SetConfiguration(void)
{
	if((uint8_t)USB_ControlRequest.wValue <= FIXED_NUM_CONFIGURATIONS)
	{
		USB_Device_ConfigurationNumber = (uint8_t)USB_ControlRequest.wValue;
		USB_DeviceState = DEVICE_STATE_Configured;
		Dcd_Send_Zero_Length_Control_Packet();
	}
	else Endpoint_StallTransaction(ENDPOINT_CONTROL_LOG_NUMBER);

/*	if((uint8_t)USB_ControlRequest.wValue > FIXED_NUM_CONFIGURATIONS) return;

	Endpoint_ClearSETUP(void);

	USB_Device_ConfigurationNumber = (uint8_t)USB_ControlRequest.wValue;

	Endpoint_ClearStatusStage(void);

	if(USB_Device_ConfigurationNumber)
	  USB_DeviceState = DEVICE_STATE_Configured;
	else
	  USB_DeviceState = (USB_Device_IsAddressSet()) ? DEVICE_STATE_Configured : DEVICE_STATE_Powered;
*/
	EVENT_USB_Device_ConfigurationChanged();
}

//-----------------------------------------------------------------------------------
//
//-----------------------------------------------------------------------------------
static void USB_Device_GetConfiguration(void)
{
	if((USB_ControlRequest.wValue == 0x0) &&
					(USB_ControlRequest.wIndex == 0x0) &&
								(USB_ControlRequest.wLength == 0x1))
	{
		uint8_t ConfigurationValue;

		ConfigurationValue = (USB_DeviceState == DEVICE_STATE_Configured) ? 0x1 : 0x0;
		Dcd_Send_Control_Packet((uint8_t *)&ConfigurationValue, sizeof(uint8_t));
	}
	else
	{
		Endpoint_StallTransaction(ENDPOINT_CONTROL_LOG_NUMBER);
	}

	// For a USB mouse there is only one configuration, so this function
	// doesn't do anything with the configuration values sent by the host.
//	Dcd_Send_Zero_Length_Control_Packet();


/*	Endpoint_ClearSETUP(void);

	Endpoint_Write_8(USB_Device_ConfigurationNumber);
	Endpoint_ClearIN(void);

	Endpoint_ClearStatusStage(void);*/
}

//-----------------------------------------------------------------------------------
//
//-----------------------------------------------------------------------------------
static void USB_Device_GetDescriptor(void)
{
	void const* DescriptorPointer;
	uint16_t    DescriptorSize;

	if((DescriptorSize = CALLBACK_USB_GetDescriptor
		(
			USB_ControlRequest, &DescriptorPointer)
		) == NO_DESCRIPTOR)
	{
		Endpoint_StallTransaction(ENDPOINT_CONTROL_LOG_NUMBER);
	}
	else
	{
		Dcd_Send_Control_Packet((uint8_t *)DescriptorPointer, DescriptorSize);
	}


/*	const void* DescriptorPointer;
	uint16_t    DescriptorSize;

	if((DescriptorSize = CALLBACK_USB_GetDescriptor(USB_ControlRequest.wValue, USB_ControlRequest.wIndex,
	                                                 &DescriptorPointer)) == NO_DESCRIPTOR)
	{
		return;
	}
*/
/*	Endpoint_ClearSETUP(void);
	Endpoint_ClearOUT(void);*/
}

//-----------------------------------------------------------------------------------
//
//-----------------------------------------------------------------------------------
static void USB_Device_GetStatus(void)
{
	uint16_t CurrentStatus = 0x0;

	switch(USB_ControlRequest.bmRequestType)
	{

		case (REQDIR_DEVICETOHOST | REQTYPE_STANDARD | REQREC_DEVICE):
		{
			CurrentStatus = 0x0001;		// No Remote Wakeup, Self Powered;
			break;
		}

		case (REQDIR_DEVICETOHOST | REQTYPE_STANDARD | REQREC_INTERFACE):
		{
			CurrentStatus = 0x0000;
			break;
		}

		case (REQDIR_DEVICETOHOST | REQTYPE_STANDARD | REQREC_ENDPOINT):
		{
			CurrentStatus = Endpoint_IsStalled();
			break;
		}


		default:
			return;
	}
	//
	Dcd_Send_Control_Packet((uint8_t *)&CurrentStatus, sizeof(uint16_t));
}

//-----------------------------------------------------------------------------------
//
//-----------------------------------------------------------------------------------
static void USB_Device_ClearSetFeature(void)
{
	switch (USB_ControlRequest.bmRequestType & CONTROL_REQTYPE_RECIPIENT)
	{
		case REQREC_DEVICE:
/*			if ((uint8_t)USB_ControlRequest.wValue == FEATURE_SEL_DeviceRemoteWakeup)
			  USB_Device_RemoteWakeupEnabled = (USB_ControlRequest.bRequest == REQ_SetFeature);
			else
			  return;
*/
			break;

		case REQREC_ENDPOINT:
		{
			if ((uint8_t)USB_ControlRequest.wValue == FEATURE_SEL_EndpointHalt)
			{
				uint8_t EpLogNumber = ((uint8_t)USB_ControlRequest.wIndex & ENDPOINT_EPNUM_MASK);

				if(EpLogNumber != ENDPOINT_CONTROL_LOG_NUMBER)
				{
					uint8_t EpPhyNumber;

					//
					EpPhyNumber = 2 * EpLogNumber;
					EpPhyNumber += (USB_ControlRequest.wIndex & ENDPOINT_DIR_MASK) ? 0x1 : 0x0;
					//
					if (USB_ControlRequest.bRequest == REQ_SetFeature)
					{
						Endpoint_StallTransaction(EpPhyNumber);
					}
					else
					{
						Endpoint_ClearStall(EpPhyNumber);
//						Endpoint_ResetEndpoint(EndpointIndex);
						Endpoint_ResetDataToggle(EpPhyNumber);
					}
				}
			}

			break;
		}

		default:
			return;
	}

	Dcd_Send_Zero_Length_Control_Packet();
}

//-----------------------------------------------------------------------------------
//
//-----------------------------------------------------------------------------------
static void Ven_Request_ResetModule(void)
{
	// acknowledge
	Dcd_Send_Zero_Length_Control_Packet();
}

//-----------------------------------------------------------------------------------
//
//-----------------------------------------------------------------------------------
static void Ven_Request_PutArray(void)
{
	// receive a CONTROL OUT packet from the host
	Dcd_Receive_Control_Packet(Control_OUT_Buffer, USB_ControlRequest.wLength);
}

//-----------------------------------------------------------------------------------
//
//-----------------------------------------------------------------------------------
static void Ven_Request_GetArray(void)
{
	void const* DataPtr;
	uint16_t		DataSize;

	if((DataSize = CALLBACK_USB_GetArray(USB_ControlRequest, &DataPtr)) == NO_DESCRIPTOR)
	{
		Endpoint_StallTransaction(ENDPOINT_CONTROL_LOG_NUMBER);
	}
	else
	{
		Dcd_Send_Control_Packet((uint8_t *)DataPtr, DataSize);
	}

	// acknowledge
//	Dcd_Send_Zero_Length_Control_Packet();*/
}

//-----------------------------------------------------------------------------------
//
//-----------------------------------------------------------------------------------
static void Ven_Request_CodecStart(void)
{
	// ����� �� ������ � ��������
	if(USB_ControlRequest.wValue & 0x3)
	{
		bool AdcEnabled, DacEnabled;

		AdcEnabled = (USB_ControlRequest.wValue & IS_CODEC_IN_START) ? true : false;
		DacEnabled = (USB_ControlRequest.wValue & IS_CODEC_OUT_START) ? true : false;

		// ������ ���� �������
		StartCodecs_ADAU1361(AdcEnabled, DacEnabled);

/*		uint32_t i;

		for(i = 0x0; i < 0x1000; i++) InBuffer[i] = i;
		Dcd_Send_Bulk_Packet((uint8_t *)InBuffer, 0x1000);
		Dcd_Receive_Bulk_Packet((uint8_t *)OutBuffer, 0x1000);*/
	}
	// acknowledge
	Dcd_Send_Zero_Length_Control_Packet();
}

//-----------------------------------------------------------------------------------
//
//-----------------------------------------------------------------------------------
static void Ven_Request_AdcStart(void)
{
	// ������ ���������� ��� AD7895
	Start_AD7895();
	// acknowledge
	Dcd_Send_Zero_Length_Control_Packet();
}

//-----------------------------------------------------------------------------------
//
//-----------------------------------------------------------------------------------
static void Ven_Request_GetUsbSpeed(void)
{
	uint8_t UsbSpeed;

	UsbSpeed = 0x1;
//	UsbSpeed = (USB_FLAGS & FLAG_USB_HIGH_SPEED) ? 0x1 : 0x0;
	// acknowledge
	Dcd_Send_Control_Packet((uint8_t *)&UsbSpeed, 0x1);
}

//-----------------------------------------------------------------------------------
//
//-----------------------------------------------------------------------------------
static void Ven_Request_CodecStop(void)
{
	// ������� ���������� ��� AD7895
	Stop_AD7895();
	// ������� ���� �������
	StopCodecs_ADAU1361();

	// -=== flush BULK IN ===-
	do
	{
		// try to clear any primed buffers
		USB0_ENDPTFLUSH_bit.FETB2		= 0x1;
		// Wait until FETB2 is 0
		while(USB0_ENDPTFLUSH_bit.FETB2);
	}
	while(USB0_ENDPTSTAT_bit.ETBR2);
	USB0_ENDPTCOMPLETE_bit.ETCE2	= 0x1;
	// -=====================-

	// -=== flush BULK OUT ===-
	do
	{
		// try to clear any primed buffers
		USB0_ENDPTFLUSH_bit.FERB2		= 0x1;
		// Wait until FERB2 is 0
		while(USB0_ENDPTFLUSH_bit.FERB2);
	}
	while(USB0_ENDPTSTAT_bit.ERBR2);
	USB0_ENDPTCOMPLETE_bit.ERCE2	= 0x1;

	// ��������� ���������� NAK �� BULK OUT
	USB0_ENDPTNAKEN |= (0x1 << EP_Physical2BitPosition(2*ENDPOINT_BULK_LOG_NUMBER + 0x0));

	// ���������� ���������� FIFO ������� �������
//	FillCodecFifoPars(7*1024, 1024);
	// ��������� ���������� FIFO �������
	SetCodecFifoPars();
	// ��������� ���������� FIFO ���������� ��� AD7895
	SetAd7895FifoPars();

	// acknowledge
	Dcd_Send_Zero_Length_Control_Packet();
}

//-----------------------------------------------------------------------------------
//
//-----------------------------------------------------------------------------------
static void Ven_Request_AdcStop(void)
{
	Ven_Request_CodecStop();
}

//-----------------------------------------------------------------------------------
//
//-----------------------------------------------------------------------------------
static void Ven_Request_GetModuleName(void)
{
	uint8_t NBytes;
	char ModuleName[0x7] = "S203";

	NBytes = 0x7;
	if(NBytes > USB_ControlRequest.wLength) NBytes = USB_ControlRequest.wLength;
	//
	Dcd_Send_Control_Packet((uint8_t *)ModuleName, NBytes);
}

//-----------------------------------------------------------------------------------
//
//-----------------------------------------------------------------------------------
static void Ven_Request_GetSerialNumber(void)
{
	uint8_t NBytes;
	char SerialNumber[0x10] = "0123456789ABCDE";

	NBytes = 0x10;
	if(NBytes > USB_ControlRequest.wLength) NBytes = USB_ControlRequest.wLength;
	//
	Dcd_Send_Control_Packet((uint8_t *)SerialNumber, NBytes);
}
