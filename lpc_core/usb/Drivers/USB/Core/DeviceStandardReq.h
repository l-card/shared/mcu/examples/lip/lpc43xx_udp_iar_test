#ifndef __DEVICESTDREQ_H__
#define __DEVICESTDREQ_H__

	#include "../../../Common/Common.h"
	#include "StdDescriptors.h"
	#include "Events.h"
	#include "StdRequestType.h"
	#include "USBTask.h"
	#include "USBController.h"

	// Enable C linkage for C++ Compilers
	#if defined(__cplusplus)
		extern "C" {
	#endif

/* Public Interface - May be used in end-application: */
	/* Enums: */
		#if defined(ARCH_HAS_MULTI_ADDRESS_SPACE) || defined(__DOXYGEN__)
			/** Enum for the possible descriptor memory spaces, for the \c MemoryAddressSpace parameter of the
			 *  @ref CALLBACK_USB_GetDescriptor() function. This can be used when none of the \c USE_*_DESCRIPTORS
			 *  compile time options are used, to indicate in which memory space the descriptor is stored.
			 *
			 *  @ingroup Group_Device
			 */
			enum USB_DescriptorMemorySpaces_t
			{
				#if defined(ARCH_HAS_FLASH_ADDRESS_SPACE) || defined(__DOXYGEN__)
				MEMSPACE_FLASH    = 0, /**< Indicates the requested descriptor is located in FLASH memory. */
				#endif
				#if defined(ARCH_HAS_EEPROM_ADDRESS_SPACE) || defined(__DOXYGEN__)
				MEMSPACE_EEPROM   = 1, /**< Indicates the requested descriptor is located in EEPROM memory. */
				#endif
				MEMSPACE_RAM      = 2, /**< Indicates the requested descriptor is located in RAM memory. */
			};
		#endif

	/* Global Variables: */
		/** Indicates the currently set configuration number of the device. USB devices may have several
		 *  different configurations which the host can select between; this indicates the currently selected
		 *  value, or 0 if no configuration has been selected.
		 *
		 *  @note This variable should be treated as read-only in the user application, and never manually
		 *        changed in value.
		 *
		 *  @ingroup Group_Device
		 */
		extern uint8_t USB_Device_ConfigurationNumber;

		#if !defined(NO_DEVICE_REMOTE_WAKEUP)
			/** Indicates if the host is currently allowing the device to issue remote wakeup events. If this
			 *  flag is cleared, the device should not issue remote wakeup events to the host.
			 *
			 *  @note This variable should be treated as read-only in the user application, and never manually
			 *        changed in value.
			 *        \n\n
			 *
			 *  @note To reduce FLASH usage of the compiled applications where Remote Wakeup is not supported,
			 *        this global and the underlying management code can be disabled by defining the
			 *        \c NO_DEVICE_REMOTE_WAKEUP token in the project makefile and passing it to the compiler via
			 *        the -D switch.
			 *
			 *  @ingroup Group_Device
			 */
//			extern bool USB_Device_RemoteWakeupEnabled;
		#endif

		#if !defined(NO_DEVICE_SELF_POWER)
			/** Indicates if the device is currently being powered by its own power supply, rather than being
			 *  powered by the host's USB supply. This flag should remain cleared if the device does not
			 *  support self powered mode, as indicated in the device descriptors.
			 *
			 *  @ingroup Group_Device
			 */
//			extern bool USB_Device_CurrentlySelfPowered;
		#endif

		// ���������� SETUP �����
		void USB_Device_ProcessSetupRequest(void);
		// ���������� CONTROL OUT �����
		void USB_Device_ProcessControlOutRequest(void);

	// Disable C linkage for C++ Compilers
	#if defined(__cplusplus)
		}
	#endif

#endif
