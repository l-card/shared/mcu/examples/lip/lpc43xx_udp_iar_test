#ifndef __ENDPOINT_H__
#define __ENDPOINT_H__

	#include "../../../Common/Common.h"
	#include "USBTask.h"
//	#include "USBInterrupt.h"
//	#include "DCD/Endpoint_LPC18xx.h"


	// Enable C linkage for C++ Compilers
	#if defined(__cplusplus)
		extern "C" {
	#endif

	// -=== Macros ===-
		// Endpoint number mask, for masking against endpoint addresses to retrieve the endpoint's numerical address in the device.
		#define ENDPOINT_EPNUM_MASK                     0x0F

		// Endpoint address for the default control endpoint, which always resides in address 0.
		//	This is defined for convenience to give more readable code when used with the endpoint macros.
		#define ENDPOINT_CONTROL_LOG_NUMBER					(0x0)

		#define ENDPOINT_BULK_LOG_NUMBER						(0x2)


		// Mask for the bank mode selection for the @ref Endpoint_ConfigureEndpoint() macro. This indicates
		// that the endpoint should have one single bank, which requires less USB FIFO memory but results
		// in slower transfers as only one USB device (the LPC or the host) can access the endpoint's
		// bank at the one time.
		#define ENDPOINT_BANK_SINGLE                    (0 << 1)

		// Mask for the bank mode selection for the @ref Endpoint_ConfigureEndpoint() macro. This indicates
		// that the endpoint should have two banks, which requires more USB FIFO memory but results
		// in faster transfers as one USB device (the LPC or the host) can access one bank while the other
		// accesses the second bank.
		#define ENDPOINT_BANK_DOUBLE                    (1 << 1)

		#define		ENDPOINT_CONTROLEP_DEFAULT_SIZE     64
		#define		BULK_EP_SIZE								(64)


		// Retrieves the maximum bank size in bytes of a given endpoint.
		// Note: This macro will only work correctly on endpoint indexes that are compile-time constants
		// defined by the preprocessor.
		// @param EPIndex  Endpoint number, a value between 0 and (\ref ENDPOINT_TOTAL_ENDPOINTS - 1)
		#define ENDPOINT_MAX_SIZE(EPIndex)              512

		// Enum for the possible error return codes of the @ref Endpoint_WaitUntilReady() function.
		enum Endpoint_WaitUntilReady_ErrorCodes_t
		{
			ENDPOINT_READYWAIT_NoError                 = 0,	/**< Endpoint is ready for next packet, no error. */
			ENDPOINT_READYWAIT_EndpointStalled         = 1,	/**< The endpoint was stalled during the stream
															 *   transfer by the host or device.
															 */
			ENDPOINT_READYWAIT_DeviceDisconnected      = 2,	/**< Device was disconnected from the host while
															 *   waiting for the endpoint to become ready.
															 */
			ENDPOINT_READYWAIT_BusSuspended            = 3,	/**< The USB bus has been suspended by the host and
															 *   no USB endpoint traffic can occur until the bus
															 *   has resumed.
															 */
			ENDPOINT_READYWAIT_Timeout                 = 4,	/**< The host failed to accept or send the next packet
															 *   within the software timeout period set by the
															 *   @ref USB_STREAM_TIMEOUT_MS macro.
															 */
		};

		#include "DCD/Endpoint_LPC18xx.h"


		/**
		 * @brief  Get the endpoint address of the currently selected endpoint. This is typically used to save
		 *  the currently selected endpoint number so that it can be restored after another endpoint has
		 *  been manipulated.
		 * @param  corenum :        ID Number of USB Core to be processed.
		 * @return Index of the currently selected endpoint
		 */
		PRAGMA_ALWAYS_INLINE
		static inline uint8_t Endpoint_GetCurrentEndpoint(uint8_t corenum) ATTR_WARN_UNUSED_RESULT ATTR_ALWAYS_INLINE;

		static inline uint8_t Endpoint_GetCurrentEndpoint(uint8_t corenum)
		{
//				return endpointselected[corenum];
return 0;
		}

		/**
		 * @brief  Selects the given endpoint number. If the address from the device descriptors is used, the
		 *  value should be masked with the @ref ENDPOINT_EPNUM_MASK constant to extract only the endpoint
		 *  number (and discarding the endpoint direction bit).
		 *
		 *  Any endpoint operations which do not require the endpoint number to be indicated will operate on
		 *  the currently selected endpoint.
		 *
		 * @param  corenum :        ID Number of USB Core to be processed.
		 * @param  EndpointNumber : Endpoint number to select
		 * @return Nothing
		 */
		PRAGMA_ALWAYS_INLINE
		static inline void Endpoint_SelectEndpoint(uint8_t corenum, const uint8_t EndpointNumber) ATTR_ALWAYS_INLINE;

		static inline void Endpoint_SelectEndpoint(uint8_t corenum, const uint8_t EndpointNumber)
		{
//				endpointselected[corenum] = EndpointNumber;
			// usb_data_buffer_index = 0;
		}

		/**
		 * @brief  Reads one byte from the currently selected endpoint's bank, for OUT direction endpoints.
		 *
		 *  @ingroup Group_EndpointPrimitiveRW
		 *
		 * @param  corenum :        ID Number of USB Core to be processed.
		 * @return Next byte in the currently selected endpoint's FIFO buffer
		 */
		PRAGMA_ALWAYS_INLINE
		static inline uint8_t Endpoint_Read_8(uint8_t corenum) ATTR_WARN_UNUSED_RESULT ATTR_ALWAYS_INLINE;
		static inline uint8_t Endpoint_Read_8(uint8_t corenum)
		{
/*				uint8_t tem;
				if (endpointselected[corenum] == ENDPOINT_CONTROLEP) {
					tem = usb_data_buffer[corenum][usb_data_buffer_index[corenum]];
					usb_data_buffer_index[corenum]++;
					usb_data_buffer_size[corenum]--;
				}
				else {
					tem = usb_data_buffer_OUT[corenum][usb_data_buffer_OUT_index[corenum]];
					usb_data_buffer_OUT_index[corenum]++;
					usb_data_buffer_OUT_size[corenum]--;
				}
				return tem;*/
			return 0;
		}

		/**
		 * @brief  Determines the currently selected endpoint's direction
		 * @param  corenum :        ID Number of USB Core to be processed.
		 * @return The currently selected endpoint's direction, as a \c ENDPOINT_DIR_* mask.
		 */
		PRAGMA_ALWAYS_INLINE
		static inline uint8_t Endpoint_GetEndpointDirection(uint8_t corenum) ATTR_WARN_UNUSED_RESULT ATTR_ALWAYS_INLINE;

		static inline uint8_t Endpoint_GetEndpointDirection(uint8_t corenum)
		{
//				return (endpointhandle(corenum)[endpointselected[corenum]] % 2) ? ENDPOINT_DIR_IN : ENDPOINT_DIR_OUT;
return 0;
		}

		/**
		 * @brief  Determines if the currently selected endpoint may be read from (if data is waiting in the endpoint
		 *  bank and the endpoint is an OUT direction, or if the bank is not yet full if the endpoint is an IN
		 *  direction). This function will return false if an error has occurred in the endpoint, if the endpoint
		 *  is an OUT direction and no packet (or an empty packet) has been received, or if the endpoint is an IN
		 *  direction and the endpoint bank is full.
		 *
		 *  @ingroup Group_EndpointPacketManagement
		 *
		 * @param  corenum :        ID Number of USB Core to be processed.
		 *  @return Boolean \c true if the currently selected endpoint may be read from or written to, depending
		 *          on its direction.
		 */
		PRAGMA_ALWAYS_INLINE
		static inline bool Endpoint_IsReadWriteAllowed(uint8_t corenum) ATTR_WARN_UNUSED_RESULT ATTR_ALWAYS_INLINE;

		static inline bool Endpoint_IsReadWriteAllowed(uint8_t corenum)
		{
//				return (Endpoint_GetEndpointDirection(corenum) == ENDPOINT_DIR_OUT) ? Endpoint_IsOUTReceived(corenum) : Endpoint_IsINReady(corenum);
	return 0;
		}

		/**
		 * @brief  Writes one byte to the currently selected endpoint's bank, for IN direction endpoints.
		 *
		 *  @ingroup Group_EndpointPrimitiveRW
		 *
		 * @param  corenum :        ID Number of USB Core to be processed.
		 * @param  Data : Data to write into the the currently selected endpoint's FIFO buffer
		 * @return Nothing
		 */
		PRAGMA_ALWAYS_INLINE
		static inline void Endpoint_Write_8(const uint8_t Data) ATTR_ALWAYS_INLINE;
		static inline void Endpoint_Write_8(const uint8_t Data)
		{
/*				if (endpointselected[corenum] == ENDPOINT_CONTROLEP) {
					usb_data_buffer[corenum][usb_data_buffer_index[corenum]] = Data;
					usb_data_buffer_index[corenum]++;
				}
				else {
					usb_data_buffer_IN[corenum][usb_data_buffer_IN_index[corenum]] = Data;
					usb_data_buffer_IN_index[corenum]++;
				}*/
		}

		/**
		 * @brief  Discards one byte from the currently selected endpoint's bank, for OUT direction endpoints.
		 * @param  corenum :        ID Number of USB Core to be processed.
		 * @return Nothing
		 *
		 *  @ingroup Group_EndpointPrimitiveRW
		 */
		PRAGMA_ALWAYS_INLINE
		static inline void Endpoint_Discard_8(uint8_t corenum) ATTR_ALWAYS_INLINE;

		static inline void Endpoint_Discard_8(uint8_t corenum)
		{
			volatile uint8_t dummy;
			dummy = Endpoint_Read_8(corenum);
			dummy = dummy;
		}

		/**
		 * @brief  Reads two bytes from the currently selected endpoint's bank in little endian format, for OUT
		 *  direction endpoints.
		 *
		 *  @ingroup Group_EndpointPrimitiveRW
		 *  @param  corenum :        ID Number of USB Core to be processed.
		 *  @return Next two bytes in the currently selected endpoint's FIFO buffer.
		 */
		PRAGMA_ALWAYS_INLINE
		static inline uint16_t Endpoint_Read_16_LE(void) ATTR_WARN_UNUSED_RESULT ATTR_ALWAYS_INLINE;
		static inline uint16_t Endpoint_Read_16_LE(void)
		{
/*			uint16_t tem = 0;
			uint8_t tem1, tem2;

			tem1 = Endpoint_Read_8(void);
			tem2 = Endpoint_Read_8(void);
			tem = (tem2 << 8) | tem1;
			return tem;*/
			return 0;
		}

		/**
		 * @brief  Reads two bytes from the currently selected endpoint's bank in big endian format, for OUT
		 *  direction endpoints.
		 *
		 *  @ingroup Group_EndpointPrimitiveRW
		 *
		 * @param  corenum :        ID Number of USB Core to be processed.
		 *  @return Next two bytes in the currently selected endpoint's FIFO buffer.
		 */
		PRAGMA_ALWAYS_INLINE
		static inline uint16_t Endpoint_Read_16_BE(uint8_t corenum) ATTR_WARN_UNUSED_RESULT ATTR_ALWAYS_INLINE;

		static inline uint16_t Endpoint_Read_16_BE(uint8_t corenum)
		{
			uint16_t tem = 0;
			uint8_t tem1, tem2;

			tem1 = Endpoint_Read_8(corenum);
			tem2 = Endpoint_Read_8(corenum);
			tem = (tem1 << 8) | tem2;
			return tem;
		}

		/**
		 * @brief  Writes two bytes to the currently selected endpoint's bank in little endian format, for IN
		 *  direction endpoints.
		 *
		 *  @ingroup Group_EndpointPrimitiveRW
		 *
		 * @param  corenum :        ID Number of USB Core to be processed.
		 * @param  Data : Data to write to the currently selected endpoint's FIFO buffer
		 * @return Nothing
		 */
		PRAGMA_ALWAYS_INLINE
		static inline void Endpoint_Write_16_LE(const uint16_t Data) ATTR_ALWAYS_INLINE;
		static inline void Endpoint_Write_16_LE(const uint16_t Data)
		{
			Endpoint_Write_8(Data & 0xFF);
			Endpoint_Write_8((Data >> 8) & 0xFF);
		}

		/**
		 * @brief  Writes two bytes to the currently selected endpoint's bank in big endian format, for IN
		 *  direction endpoints.
		 *
		 *  @ingroup Group_EndpointPrimitiveRW
		 *
		 * @param  corenum :        ID Number of USB Core to be processed.
		 * @param  Data : Data to write to the currently selected endpoint's FIFO buffer
		 * @return Nothing
		 */
		PRAGMA_ALWAYS_INLINE
		static inline void Endpoint_Write_16_BE(uint8_t corenum, const uint16_t Data) ATTR_ALWAYS_INLINE;
		static inline void Endpoint_Write_16_BE(uint8_t corenum, const uint16_t Data)
		{
/*			Endpoint_Write_8(corenum, (Data >> 8) & 0xFF);
			Endpoint_Write_8(corenum, Data & 0xFF);*/
		}

		/**
		 * @brief  Discards two bytes from the currently selected endpoint's bank, for OUT direction endpoints.
		 * @return Nothing
		 *
		 *  @ingroup Group_EndpointPrimitiveRW
		 * @param  corenum :        ID Number of USB Core to be processed.
		 */
		PRAGMA_ALWAYS_INLINE
		static inline void Endpoint_Discard_16(uint8_t corenum) ATTR_ALWAYS_INLINE;

		static inline void Endpoint_Discard_16(uint8_t corenum)
		{
/*			uint8_t tem;
			tem = Endpoint_Read_8(corenum);
			tem = Endpoint_Read_8(corenum);
			tem  = tem;*/
		}

		/**
		 * @brief  Reads four bytes from the currently selected endpoint's bank in little endian format, for OUT
		 *  direction endpoints.
		 *
		 *  @ingroup Group_EndpointPrimitiveRW
		 *
		 * @param  corenum :        ID Number of USB Core to be processed.
		 *  @return Next four bytes in the currently selected endpoint's FIFO buffer.
		 */
		PRAGMA_ALWAYS_INLINE
		static inline uint32_t Endpoint_Read_32_LE(uint8_t corenum) ATTR_WARN_UNUSED_RESULT ATTR_ALWAYS_INLINE;

		static inline uint32_t Endpoint_Read_32_LE(uint8_t corenum)
		{
/*			uint32_t tem = 0;
			uint8_t tem1, tem2, tem3, tem4;

			tem1 = Endpoint_Read_8(corenum);
			tem2 = Endpoint_Read_8(corenum);
			tem3 = Endpoint_Read_8(corenum);
			tem4 = Endpoint_Read_8(corenum);
			tem = (tem4 << 24) | (tem3 << 16) | (tem2 << 8) | tem1;
			return tem;*/
			return 0;
		}

		/**
		 * @brief  Reads four bytes from the currently selected endpoint's bank in big endian format, for OUT
		 *  direction endpoints.
		 *
		 *  @ingroup Group_EndpointPrimitiveRW
		 *
		 *  @param  corenum :        ID Number of USB Core to be processed.
		 *  @return Next four bytes in the currently selected endpoint's FIFO buffer.
		 */
		PRAGMA_ALWAYS_INLINE
		static inline uint32_t Endpoint_Read_32_BE(uint8_t corenum) ATTR_WARN_UNUSED_RESULT ATTR_ALWAYS_INLINE;

		static inline uint32_t Endpoint_Read_32_BE(uint8_t corenum)
		{
			uint32_t tem = 0;
			uint8_t tem1, tem2, tem3, tem4;

			tem1 = Endpoint_Read_8(corenum);
			tem2 = Endpoint_Read_8(corenum);
			tem3 = Endpoint_Read_8(corenum);
			tem4 = Endpoint_Read_8(corenum);
			tem = (tem1 << 24) | (tem2 << 16) | (tem3 << 8) | tem4;
			return tem;
		}

		/**
		 * @brief  Writes four bytes to the currently selected endpoint's bank in little endian format, for IN
		 *  direction endpoints.
		 *
		 *  @ingroup Group_EndpointPrimitiveRW
		 *
		 * @param  corenum :        ID Number of USB Core to be processed.
		 * @param  Data : Data to write to the currently selected endpoint's FIFO buffer
		 * @return Nothing
		 */
		PRAGMA_ALWAYS_INLINE
		static inline void Endpoint_Write_32_LE(uint8_t corenum, const uint32_t Data) ATTR_ALWAYS_INLINE;

		static inline void Endpoint_Write_32_LE(uint8_t corenum, const uint32_t Data)
		{
/*			Endpoint_Write_8(corenum, Data & 0xFF);
			Endpoint_Write_8(corenum, (Data >> 8) & 0xFF);
			Endpoint_Write_8(corenum, (Data >> 16) & 0xFF);
			Endpoint_Write_8(corenum, (Data >> 24) & 0xFF);*/
		}

		/**
		 * @brief  Writes four bytes to the currently selected endpoint's bank in big endian format, for IN
		 *  direction endpoints.
		 *
		 *  @ingroup Group_EndpointPrimitiveRW
		 *
		 * @param  corenum :        ID Number of USB Core to be processed.
		 * @param  Data : Data to write to the currently selected endpoint's FIFO buffer
		 * @return Nothing
		 */
		PRAGMA_ALWAYS_INLINE
		static inline void Endpoint_Write_32_BE(uint8_t corenum, const uint32_t Data) ATTR_ALWAYS_INLINE;

		static inline void Endpoint_Write_32_BE(uint8_t corenum, const uint32_t Data)
		{
/*			Endpoint_Write_8(corenum, (Data >> 24) & 0xFF);
			Endpoint_Write_8(corenum, (Data >> 16) & 0xFF);
			Endpoint_Write_8(corenum, (Data >> 8) & 0xFF);
			Endpoint_Write_8(corenum, Data & 0xFF);*/
		}

		/**
		 * @brief  Discards four bytes from the currently selected endpoint's bank, for OUT direction endpoints.
		 * @return Nothing
		 *
		 *  @ingroup Group_EndpointPrimitiveRW
		 * @param  corenum :        ID Number of USB Core to be processed.
		 */
		PRAGMA_ALWAYS_INLINE
		static inline void Endpoint_Discard_32(uint8_t corenum) ATTR_ALWAYS_INLINE;

		static inline void Endpoint_Discard_32(uint8_t corenum)
		{
			uint8_t tem;
			tem = Endpoint_Read_8(corenum);
			tem = Endpoint_Read_8(corenum);
			tem = Endpoint_Read_8(corenum);
			tem = Endpoint_Read_8(corenum);
			tem = tem;
		}

		void Endpoint_GetSetupPackage(uint8_t *pData);

		// Interrupt Handler (Device side).
		// This handler is known as interrupt service routine of USB Device.
		void DcdIrqHandler(void);

		/* Disable C linkage for C++ Compilers: */
		#if defined(__cplusplus)
			}
		#endif

#endif

/** @} */

