#include "USB0_HAL.h"
#include "../USBTask.h"
#include "../Device.h"

//IP_USBHS_001_T * const USB_REG_BASE_ADDR[LPC18_43_MAX_USB_CORE] = {LPC_USB0, LPC_USB1};
//
static bool Usb0CoreEnabled = false;

//-----------------------------------------------------------------------------------
// �������� ������� ������������� USB0 �����������
// This function is used in device mode to pull up resistor on USB pin D+
// Normally, this function is called when every setup or initial are done.
//-----------------------------------------------------------------------------------
void HAL_USB0Connect(bool flag)
{
	if(flag)
		USB0_USBCMD_D_bit.RS = 0x1;
//		USB_REG(corenum)->USBCMD_D |= (1 << 0);
	else
		USB0_USBCMD_D_bit.RS = 0x0;
//		USB_REG(corenum)->USBCMD_D &= ~(1 << 0);
}

//-----------------------------------------------------------------------------------
// �������� ������� ������������� USB0 �����������
// Normally, this function will do the following:
// 	- Configure USB pins
// 	- Setup USB core clock
// 	- Call HAL_RESET to setup needed USB operating registers, set device address 0 if running device mode
//-----------------------------------------------------------------------------------
bool HAL_USB0Init(void)
{
	// Just exit if already enabled
	if(!Usb0CoreEnabled)
	{
		// enable USB PLL first
		Chip_Clock_EnablePLL(CGU_USB_PLL);
		// Wait for PLL lock
		while(!(Chip_Clock_GetPLLStatus(CGU_USB_PLL) & CGU_PLL_LOCKED));

		// For core 0, enable USB0 base clock
		Chip_Clock_EnableBaseClock(CLK_BASE_USB0);
		Chip_Clock_EnableOpts(CLK_MX_USB0, true, true, 0x1);
		// Turn on the phy
		Chip_CREG_EnableUSB0Phy(true);
		//
		Usb0CoreEnabled = true;
	}

	// before initiating a device controller reset
	// the USBCMD Run/Stop bit should be set to 0
	if(USB0_USBCMD_D_bit.RS) return false;

	// flush all primed endpoints
	USB0_ENDPTFLUSH = 0xFFFFFFFF;
	while(USB0_ENDPTFLUSH);		// Wait until all bits are 0
	// reset the controller
	USB0_USBCMD_D_bit.RST = 0x1;
//	USB_REG(corenum)->USBCMD_D = USBCMD_D_Reset;
	// wait for reset to complete
	while(USB0_USBCMD_D_bit.RST)
//	while(USB_REG(corenum)->USBCMD_D & USBCMD_D_Reset) ;

	// Program the controller to be the USB device controller
	USB0_USBMODE_D_bit.CM		= 0x2;
	// Program the controller to use Setup Buffer Tripwire in USBCMD
	USB0_USBMODE_D_bit.SLOM		= 0x1;
//	USB_REG(corenum)->USBMODE_D =   (0x2 << 0) /*| (1<<4)*//*| (1<<3)*/;

	// set OTG transcever in proper state, device is present
	// on the port(CCS=1), port enable/disable status change(PES=1).
	USB0_OTGSC_bit.VD = 0x1;	// VBUS_Discharge
										// Setting this bit to 1 causes VBUS to discharge through a resistor.
	USB0_OTGSC_bit.OT = 0x1;	// OTG termination
										// This bit must be set to 1 when the OTG controller is in device mode.
										// This controls the pull-down on USB_DM.
//	LPC_USB0->OTGSC = (1 << 3) | (1 << 0) /*| (1<<16)| (1<<24)| (1<<25)| (1<<26)| (1<<27)| (1<<28)| (1<<29)| (1<<30)*/;
	//
	HAL_USB0Reset();

	// �� ������
	return true;
}

//-----------------------------------------------------------------------------------
// This function usage is opposite to HAL_USB0Init
//-----------------------------------------------------------------------------------
void HAL_USB0DeInit(void)
{
	// disable USB0 interrupts
	HAL_DisableUSB0Interrupt();

	// Clear all pending interrupts
	USB0_ENDPTNAK		= 0xFFFFFFFFUL;
	USB0_ENDPTNAKEN	= 0x00000000UL;
	USB0_USBSTS_D		= 0xFFFFFFFFUL;	// USB Status register in device mode
	USB0_ENDPTSETUPSTAT	= USB0_ENDPTSETUPSTAT;
	USB0_ENDPTCOMPLETE	= USB0_ENDPTCOMPLETE;
	while(USB0_ENDPTPRIME);		// Wait until all bits are 0
	USB0_ENDPTFLUSH		= 0xFFFFFFFF;
	while(USB0_ENDPTFLUSH);		// Wait until all bits are 0
	/* Clear all pending interrupts */
/*	USB_REG(corenum)->USBSTS_D   = 0xFFFFFFFF;
	USB_REG(corenum)->ENDPTNAK   = 0xFFFFFFFF;
	USB_REG(corenum)->ENDPTNAKEN = 0;
	USB_REG(corenum)->ENDPTSETUPSTAT = USB_REG(corenum)->ENDPTSETUPSTAT;
	USB_REG(corenum)->ENDPTCOMPLETE  = USB_REG(corenum)->ENDPTCOMPLETE;
	while (USB_REG(corenum)->ENDPTPRIME);
	USB_REG(corenum)->ENDPTFLUSH = 0xFFFFFFFF;
	while (USB_REG(corenum)->ENDPTFLUSH);*/

	// Turn off the phy (prior to PLL disabled)
	Chip_CREG_EnableUSB0Phy(false);

	// Power down USB clocking
	Chip_Clock_Disable(CLK_MX_USB0);
	Chip_Clock_DisableBaseClock(CLK_BASE_USB0);

	// Disable USB PLL
	Chip_Clock_DisablePLL(CGU_USB_PLL);

	Usb0CoreEnabled = false;
}

//-----------------------------------------------------------------------------------
// enable USB0 interrupts
//-----------------------------------------------------------------------------------
void HAL_EnableUSB0Interrupt(void)
{
	NVIC_ClearPendingIRQ(USB0_IRQn);
	NVIC_EnableIRQ(USB0_IRQn);
}

//-----------------------------------------------------------------------------------
// disable USB0 interrupts
//-----------------------------------------------------------------------------------
void HAL_DisableUSB0Interrupt(void)
{
	NVIC_DisableIRQ(USB0_IRQn);
	NVIC_ClearPendingIRQ(USB0_IRQn);
}

//-----------------------------------------------------------------------------------
// disable USB0 interrupts
//-----------------------------------------------------------------------------------
void USB0_IRQHandler(void)
{
	DcdIrqHandler();
}

//-----------------------------------------------------------------------------------
// Reset USB0 core
//-----------------------------------------------------------------------------------
void HAL_USB0Reset(void)
{
	uint32_t i;
//	IP_USBHS_001_T * USB_Reg;
//	USB_Reg = USB_REG(corenum);

	// �������� USB �����������
	USB0_PORTSC1_D_bit.PIC = 0x0;

	// disable all EPs
//	USB0_ENDPTCTRL0_bit.RXE = USB0_ENDPTCTRL0_bit.TXE = 0x0;	// Control endpoint 0 is always enabled.
	USB0_ENDPTCTRL1_bit.RXE = USB0_ENDPTCTRL1_bit.TXE = 0x0;
	USB0_ENDPTCTRL2_bit.RXE = USB0_ENDPTCTRL2_bit.TXE = 0x0;
	USB0_ENDPTCTRL3_bit.RXE = USB0_ENDPTCTRL3_bit.TXE = 0x0;
	USB0_ENDPTCTRL4_bit.RXE = USB0_ENDPTCTRL4_bit.TXE = 0x0;
	USB0_ENDPTCTRL5_bit.RXE = USB0_ENDPTCTRL5_bit.TXE = 0x0;
/*	USB_Reg->ENDPTCTRL[0] &= ~(ENDPTCTRL_RxEnable | ENDPTCTRL_TxEnable);
	USB_Reg->ENDPTCTRL[1] &= ~(ENDPTCTRL_RxEnable | ENDPTCTRL_TxEnable);
	USB_Reg->ENDPTCTRL[2] &= ~(ENDPTCTRL_RxEnable | ENDPTCTRL_TxEnable);
	USB_Reg->ENDPTCTRL[3] &= ~(ENDPTCTRL_RxEnable | ENDPTCTRL_TxEnable);
	USB_Reg->ENDPTCTRL[4] &= ~(ENDPTCTRL_RxEnable | ENDPTCTRL_TxEnable);
	USB_Reg->ENDPTCTRL[5] &= ~(ENDPTCTRL_RxEnable | ENDPTCTRL_TxEnable);*/

	// Clear all pending interrupts
	USB0_ENDPTNAK		= 0xFFFFFFFFUL;
	USB0_ENDPTNAKEN	= 0x00000000UL;
	USB0_USBSTS_D		= 0xFFFFFFFFUL;	// USB Status register in device mode
	USB0_ENDPTSETUPSTAT	= USB0_ENDPTSETUPSTAT;
	USB0_ENDPTCOMPLETE	= USB0_ENDPTCOMPLETE;
	while(USB0_ENDPTPRIME);		// Wait until all bits are 0
	USB0_ENDPTFLUSH		= 0xFFFFFFFF;
	while(USB0_ENDPTFLUSH);		// Wait until all bits are 0
/*	USB_Reg->ENDPTNAK     = 0xFFFFFFFF;
	USB_Reg->ENDPTNAKEN       = 0;
	USB_Reg->USBSTS_D     = 0xFFFFFFFF;
	USB_Reg->ENDPTSETUPSTAT   = USB_Reg->ENDPTSETUPSTAT;
	USB_Reg->ENDPTCOMPLETE    = USB_Reg->ENDPTCOMPLETE;
	while(USB_Reg->ENDPTPRIME);		// Wait until all bits are 0
	USB_Reg->ENDPTFLUSH = 0xFFFFFFFF;
	while(USB_Reg->ENDPTFLUSH);		// Wait until all bits are 0*/

	// Set the Interrupt Threshold Control interval to 0
	USB0_USBCMD_D_bit.ITC = 0x0;

	//
	for(i = 0x0; i < USED_PHYSICAL_ENDPOINTS0; i++)
	{
		memset((void *)&dQueueHead[i], 0x0, sizeof(DEVICE_QUEUE_HEAD));
		dQueueHead[i].Overlay_dTD.Next_dTD	= 0xDEAD0000 | NEXT_LINK_POINTER_IS_TERMINATED;
	}

	// Configure the Endpoint List Address
	// make sure it in on 64 byte boundary !!!
	USB0_ENDPOINTLISTADDR = (uint32_t)dQueueHead;

	// Enable interrupts:
	USB0_USBINTR_D_bit.UE	= 0x1;		// USB interrupt enable
	USB0_USBINTR_D_bit.PCE	= 0x1;		// Port change detect enable
	USB0_USBINTR_D_bit.URE	= 0x1;		// USB reset enable
	USB0_USBINTR_D_bit.NAKE	= 0x1;		// NAK interrupt enable
/*	USB0_USBINTR_D =  USBINTR_D_UsbIntEnable | USBINTR_D_UsbErrorIntEnable |
									 USBINTR_D_PortChangeIntEnable | USBINTR_D_UsbResetEnable |
									 USBINTR_D_SuspendEnable | USBINTR_D_NAKEnable;// | USBINTR_D_SofReceivedEnable;*/

//	USB_Device_SetDeviceAddress(0x0);

/*	endpointselected[corenum] = 0;
	for (i = 0; i < ENDPOINT_TOTAL_ENDPOINTS(corenum); i++)
		endpointhandle(corenum)[i] = 0;

	usb_data_buffer_size[corenum] = 0;
	usb_data_buffer_index[corenum] = 0;

	usb_data_buffer_OUT_size[corenum] = 0;
	usb_data_buffer_OUT_index[corenum] = 0;

	// usb_data_buffer_IN_size = 0;
	usb_data_buffer_IN_index[corenum] = 0;
	Stream_Variable[corenum].stream_total_packets = 0;*/
}

