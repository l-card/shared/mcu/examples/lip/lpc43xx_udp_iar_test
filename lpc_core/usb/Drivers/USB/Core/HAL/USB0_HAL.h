#ifndef __LPC_HAL_H__
#define __LPC_HAL_H__

	#include <stdint.h>
	#include <stdio.h>
	#include <stdbool.h>
	#include <nxp/iolpc4333.h>
	#include "chip.h"

	// This function is called to do the initialization for chip's USB core.
	// Normally, this function will do the following:
	// 	- Configure USB pins
	// 	- Setup USB core clock
	// 	- Call HAL_RESET to setup needed USB operating registers, set device address 0 if running device mode
	bool HAL_USB0Init(void);

	// This function usage is opposite to HAL_USBInit
	void HAL_USB0DeInit(void);

	// This function used to enable USB interrupt
	void HAL_EnableUSB0Interrupt(void);

	// This function usage is opposite to HAL_EnableUSB0Interrupt
	void HAL_DisableUSB0Interrupt(void);

	// This function is used in device mode to pull up resistor on USB pin D+
	// Normally, this function is called when every setup or initial are done.
	void HAL_USB0Connect(bool flag);

	// Reset USB0 core
	void HAL_USB0Reset(void);

#endif		/*__LPC_HAL_H__*/
