#include "USBController.h"

// Main function to initialize and start the USB0 Device interface
static void USB0_Device_Init(void);

//-----------------------------------------------------------------------------------
// �������� ������� ������������� USB ���������
//-----------------------------------------------------------------------------------
void USB_Init(void)
{
	HAL_USB0Init();
	USB0_Device_Init();
	USB_IsInitialized = true;
}

//-----------------------------------------------------------------------------------
// �������� ������� ������������� USB0 �����������
//-----------------------------------------------------------------------------------
void USB_Disable(void)
{
	USB_IsInitialized = false;

	HAL_USB0Connect(false);
	HAL_USB0DeInit();
}

//-----------------------------------------------------------------------------------
// Main function to initialize and start the USB0 Device interface
//-----------------------------------------------------------------------------------
static void USB0_Device_Init(void)
{
	ENDPOINT_CONFIG_PARS ecp;

	USB_DeviceState = DEVICE_STATE_Unattached;
	USB_Device_ConfigurationNumber  = 0;
//	USB_Device_RemoteWakeupEnabled  = false;
//	USB_Device_CurrentlySelfPowered = false;

	ecp.LogNumber		= ENDPOINT_CONTROL_LOG_NUMBER;
	ecp.Type				= EP_TYPE_CONTROL;
	ecp.Direction		= ENDPOINT_DIR_OUT;
	ecp.MaxPacketSize	= USB_Device_ControlEndpointSize;
	Dcd_ConfigureEndpoint(&ecp);

	HAL_EnableUSB0Interrupt();
	HAL_USB0Connect(true);
}
