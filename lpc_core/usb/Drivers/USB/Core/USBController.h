#ifndef __USBCONTROLLER_H__
#define __USBCONTROLLER_H__

		#include "../../../Common/Common.h"
//		#include "Events.h"
		#include "USBTask.h"
//		#include "USBInterrupt.h"

		#include "Device.h"
		#include "Endpoint.h"
		#include "DeviceStandardReq.h"

//		#include "DCD/Endpoint_LPC18xx.h"

		// Enable C linkage for C++ Compilers
		#if defined(__cplusplus)
			extern "C" {
		#endif

		// Endpoint Direction Masks */
		// Endpoint direction mask, for masking against endpoint addresses to retrieve the endpoint's
		// direction for comparing with the \c ENDPOINT_DIR_* masks.
		#define ENDPOINT_DIR_MASK                       0x80

		// Endpoint address direction mask for an OUT direction (Host to Device) endpoint. This may be ORed with
		// the index of the address within a device to obtain the full endpoint address.
		#define ENDPOINT_DIR_OUT                        0x00

		// Endpoint address direction mask for an IN direction (Device to Host) endpoint. This may be ORed with
		// the index of the address within a device to obtain the full endpoint address.
		#define ENDPOINT_DIR_IN                         0x80

		// Endpoint/Pipe Type Masks
		// Mask for determining the type of an endpoint from an endpoint descriptor. This should then be compared
		// with the \c EP_TYPE_* masks to determine the exact type of the endpoint.
		#define EP_TYPE_MASK                       0x03

		// Mask for a CONTROL type endpoint or pipe.
		// See Group_EndpointManagement and Group_PipeManagement for endpoint/pipe functions.
		#define EP_TYPE_CONTROL                    0x00

		// Mask for an ISOCHRONOUS type endpoint or pipe.
		//See Group_EndpointManagement and Group_PipeManagement for endpoint/pipe functions.
		#define EP_TYPE_ISOCHRONOUS                0x01

		// Mask for a BULK type endpoint or pipe.
		// See Group_EndpointManagement and Group_PipeManagement for endpoint/pipe functions.
		#define EP_TYPE_BULK                       0x02

		// Mask for an INTERRUPT type endpoint or pipe.
		// See Group_EndpointManagement and Group_PipeManagement for endpoint/pipe functions.
		#define EP_TYPE_INTERRUPT                  0x03

		// USB Controller Option Masks
		// Regulator disable option mask for USB_Init(). This indicates that the internal 3.3V USB data pad
		// regulator should be disabled and the LPC's VCC level used for the data pads.
		// See USB LPC data sheet for more information on the internal pad regulator.
		#define USB_OPT_REG_DISABLED               (1 << 1)

		// Regulator enable option mask for USB_Init(). This indicates that the internal 3.3V USB data pad
		// regulator should be enabled to regulate the data pin voltages from the VBUS level down to a level within
		// the range allowable by the USB standard.
		// See USB LPC data sheet for more information on the internal pad regulator.
		#define USB_OPT_REG_ENABLED                (0 << 1)

		// Detaches the device from the USB bus. This has the effect of removing the device from any
		// attached host, ceasing USB communications. If no host is present, this prevents any host from
		// enumerating the device once attached until @ref USB_Attach() is called.
		PRAGMA_ALWAYS_INLINE
		static inline void USB0_Device_Detach(void) ATTR_ALWAYS_INLINE;
		static inline void USB0_Device_Detach(void)
		{}

		// Attaches the device to the USB bus. This announces the device's presence to any attached
		//  USB host, starting the enumeration process. If no host is present, attaching the device
		// will allow for enumeration once a host is connected to the device.
		// This is inexplicably also required for proper operation while in host mode, to enable the
		// attachment of a device to the host. This is despite the bit being located in the device-mode
		// register and despite the datasheet making no mention of its requirement in host mode.
		PRAGMA_ALWAYS_INLINE
		static inline void USB0_Device_Attach(void) ATTR_ALWAYS_INLINE;
		static inline void USB0_Device_Attach(void)
		{}

		// Main function to initialize and start the USB0 Device interface
		void USB_Init(void);

		// Shuts down the USB interface. This turns off the USB interface after deallocating all USB FIFO
		// memory, endpoints and pipes. When turned off, no USB functionality can be used until the interface
		//  is restarted with the @ref USB_Init() function.
		void USB_Disable(void);

		// Disable C linkage for C++ Compilers
		#if defined(__cplusplus)
			}
		#endif

#endif
