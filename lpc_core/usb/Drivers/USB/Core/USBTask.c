#include "USBTask.h"

//
static void USB_DeviceTask(void);

volatile		bool	USB_IsInitialized = false;

// This structure must be 4 bytes aligned
PRAGMA_ALIGN_4
USB_REQUEST_HEADER	USB_ControlRequest	ATTR_ALIGNED(4) @ ".ahb_sram1";

volatile uint8_t     USB_DeviceState;

//extern uint32_t	dwDebugArray[1024];

//-----------------------------------------------------------------------------------
// The USB task must be serviced within 30ms while in device mode.
// The task may be serviced at all times.
//-----------------------------------------------------------------------------------
void USB_USBTask(void)
{
	USB_DeviceTask();
}

//-----------------------------------------------------------------------------------
//
//-----------------------------------------------------------------------------------
static void USB_DeviceTask(void)
{
	if(USB_DeviceState != DEVICE_STATE_Unattached)
	{
		// Setup �����
		if(Endpoint_IsSETUPReceived())
		{
			// ������ ������ � ��������� ������ �� CONTROL OUT
			dQueueHead[ENDPOINT_CONTROL_LOG_NUMBER + 0x0].IsOutReceived = false;
			// ���������� SETUP �����
			USB_Device_ProcessSetupRequest();
		}
		// ��������� �� CONTROL OUT ������ ����� � ������
		else if(dQueueHead[ENDPOINT_CONTROL_LOG_NUMBER + 0x0].IsOutReceived)
		{
			// ������ ������ � ��������� ������ �� CONTROL OUT
			dQueueHead[ENDPOINT_CONTROL_LOG_NUMBER + 0x0].IsOutReceived = false;
			// ���������� CONTROL OUT �����
			USB_Device_ProcessControlOutRequest();
		}
/*		// ����� �������� ������ �� BULK OUT
		if(dQueueHead[ENDPOINT_BULK_LOG_NUMBER + 0x0].IsOutReceived)
		{
			dQueueHead[ENDPOINT_BULK_LOG_NUMBER + 0x0].IsOutReceived = false;
		}*/
	}
}
