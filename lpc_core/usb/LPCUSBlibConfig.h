#ifndef LPCUSBLIB_CONFIG_H_
#define LPCUSBLIB_CONFIG_H_

	// Available configuration number in a device
	#define	FIXED_NUM_CONFIGURATIONS			(0x1)

	// Control endpoint max packet size
	#define	FIXED_CONTROL_ENDPOINT_SIZE		(64)

	// Size of share memory that a device uses to store data transfer to/ receive from host
	// or a host uses to store data transfer to/ receive from device.
	#define USBRAM_BUFFER_SIZE  (4*1024)

	// This option effects only on high speed parts that need to test full speed activities */
	#define USB_FORCED_FULLSPEED		0

#endif /* NXPUSBLIB_CONFIG_H_ */
