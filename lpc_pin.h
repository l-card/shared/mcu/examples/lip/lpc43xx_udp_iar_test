/*================================================================================================*
 * Управление коммутацией выводов LPC43xx и GPIO
 *================================================================================================*/

#ifndef LPC_PIN_H_
#define LPC_PIN_H_

/*================================================================================================*
 * Адрес ножки контроллера и ее функция задается одной константой
 * LPC_PIN_ID(p, q, n, m, f), где:
 *  p, q - адрес ножки в нотации Pp_q (в SCU) (например, P1_5)
 *  n, m - адрес ножки в нотации GPIOn[m] (в GPIO) (например, GPIO1[8])
 *  f    - номер цифровой функции, выбранной на ножке
 * Полный список всех функций перечислен ниже в виде
 * LPC_PIN_Pp_q_funcname, например:
 * #define LPC_PIN_P1_5_SSP1_SSEL   LPC_PIN_ID(1, 5, 1, 8, 5)
 * Настройка и подключение ножки:
 *  LPC_PIN_CONFIG(id, ...)
 *
 * Макросы LPC_PIN_... принимают в качестве параметра id = LPC_PIN_ID(...).
 *
 * Для ножек CLKn функция задается через LPC_PIN_CLKCONFIG(id, ...)
 *================================================================================================*/

/* PIN ID: 0x0pqqnmmf */
#define LPC_PIN_ID(p, q, n, m, f)   ((((p) & 0xF) << 24) | (((q) & 0x1F) << 16) | \
                                    (((n) & 0xF) << 12) | (((m) & 0x1F) << 4) | ((f) & 7))

/* Извлечение полей ID (internal use) */
#define LPC_PIN_ID_p(id)            (((id) >> 24) & 0xF)
#define LPC_PIN_ID_q(id)            (((id) >> 16) & 0x1F)
#define LPC_PIN_ID_n(id)            (((id) >> 12) & 0xF)
#define LPC_PIN_ID_m(id)            (((id) >> 4) & 0x1F)
#define LPC_PIN_ID_f(id)            ((id) & 7)

/* Адресация регистров LPC по pin ID */
#define LPC_PIN_CONFREG(id)         LPC_SCU->SFSP[LPC_PIN_ID_p(id)][LPC_PIN_ID_q(id)]
#define LPC_PIN_GPIOB(id)           LPC_GPIO_PORT->B[LPC_PIN_ID_n(id)][LPC_PIN_ID_m(id)]
#define LPC_PIN_GPIOW(id)           LPC_GPIO_PORT->W[LPC_PIN_ID_n(id)][LPC_PIN_ID_m(id)]
#define LPC_PIN_GPIOREG(reg, id)    LPC_GPIO_PORT->reg[LPC_PIN_ID_n(id)]
#define LPC_PIN_BITMSK(id)          (1UL << LPC_PIN_ID_m(id))

/* Константы для параметра pinmode LPC_PIN_CONFIG */
#define LPC_PIN_NOPULL              LPC_SCU_SFSP_EPUN
#define LPC_PIN_PULLUP              0
#define LPC_PIN_PULLDN              (LPC_SCU_SFSP_EPUN | LPC_SCU_SFSP_EPD)
#define LPC_PIN_REPEAT              LPC_SCU_SFSP_EPD
#define LPC_PIN_FAST                LPC_SCU_SFSP_EHS
#define LPC_PIN_NOFILTER            LPC_SCU_SFSP_ZIF
#define LPC_PIN_HD_4mA              0
#define LPC_PIN_HD_8mA              (1UL << 8)
#define LPC_PIN_HD_12mA             (2UL << 8)
#define LPC_PIN_HD_20mA             (3UL << 8)

/* Установка конфигурации ножки (функция внутри ID, остальные параметры явно) */
#define LPC_PIN_CONFIG(id, input_en, pinmode) \
    (LPC_PIN_CONFREG(id) = LPC_PIN_ID_f(id) | ((input_en) ? LPC_SCU_SFSP_EZI : 0) | (pinmode))

/* Установка конфигурации ножки CLKn */
#define LPC_PIN_CLKCONFIG(id, input_en, pinmode) \
    (LPC_SCU->SFSCLK[LPC_PIN_ID_q(id) & 3] = LPC_PIN_ID_f(id) | ((input_en) ? LPC_SCU_SFSP_EZI : 0) | (pinmode))

/* Управление ножками GPIO */
    /* управление направлением */
#define LPC_PIN_DIR_GET(id)         (!!(LPC_PIN_GPIOREG(DIR, (id)) & LPC_PIN_BITMSK(id)))
#define LPC_PIN_DIR_IN(id)          (LPC_PIN_GPIOREG(DIR, (id)) &= ~LPC_PIN_BITMSK(id))
#define LPC_PIN_DIR_OUT(id)         (LPC_PIN_GPIOREG(DIR, (id)) |= LPC_PIN_BITMSK(id))
    /* установка направления через bit banding */
#define LPC_PIN_DIR_BB(id)          BIT_BAND(LPC_PIN_GPIOREG(DIR, (id)), LPC_PIN_ID_m(id))
    /* установка уровня на ножке (0 или 1) */
#define LPC_PIN_OUT(id, val)        (LPC_PIN_GPIOB(id) = (val))
    /* чтение уровня на ножке (0 или 1) */
#define LPC_PIN_IN(id)              LPC_PIN_GPIOB(id)
    /* атомарное инвертирование уровня на ножке */
#define LPC_PIN_TOGGLE(id)          (LPC_PIN_GPIOREG(NOT, (id)) = LPC_PIN_BITMSK(id))
    /* состояние регистра выхода (независимо от физического уровня) */
#define LPC_PIN_GET_OUT_STATE(id)   (!!(LPC_PIN_GPIOREG(SET, (id)) & LPC_PIN_BITMSK(id)))
/*================================================================================================*/

/*================================================================================================*/
/* Список функций ножек */
/* Адрес GPIO "8, 0" означает, что ножка не имеет функции GPIO */

/* P0 */
#define LPC_PIN_P0_0_GPIO0_0        LPC_PIN_ID(0, 0, 0, 0, 0)
#define LPC_PIN_P0_0_SSP1_MISO      LPC_PIN_ID(0, 0, 0, 0, 1)
#define LPC_PIN_P0_0_ENET_RXD1      LPC_PIN_ID(0, 0, 0, 0, 2)
#define LPC_PIN_P0_0_SGPIO0         LPC_PIN_ID(0, 0, 0, 0, 3)
#define LPC_PIN_P0_0_I2S0_TX_WS     LPC_PIN_ID(0, 0, 0, 0, 6)
#define LPC_PIN_P0_0_I2S1_TX_WS     LPC_PIN_ID(0, 0, 0, 0, 7)

#define LPC_PIN_P0_1_GPIO0_1        LPC_PIN_ID(0, 1, 0, 1, 0)
#define LPC_PIN_P0_1_SSP1_MOSI      LPC_PIN_ID(0, 1, 0, 1, 1)
#define LPC_PIN_P0_1_ENET_COL       LPC_PIN_ID(0, 1, 0, 1, 2)
#define LPC_PIN_P0_1_SGPIO1         LPC_PIN_ID(0, 1, 0, 1, 3)
#define LPC_PIN_P0_1_ENET_TX_EN     LPC_PIN_ID(0, 1, 0, 1, 6)
#define LPC_PIN_P0_1_I2S1_TX_SD     LPC_PIN_ID(0, 1, 0, 1, 7)

/* P1 */
#define LPC_PIN_P1_0_GPIO0_4        LPC_PIN_ID(1, 0, 0, 4, 0)
#define LPC_PIN_P1_0_CTIN_3         LPC_PIN_ID(1, 0, 0, 4, 1)
#define LPC_PIN_P1_0_EMC_A5         LPC_PIN_ID(1, 0, 0, 4, 2)
#define LPC_PIN_P1_0_SSP0_SSEL      LPC_PIN_ID(1, 0, 0, 4, 5)
#define LPC_PIN_P1_0_SGPIO7         LPC_PIN_ID(1, 0, 0, 4, 6)

#define LPC_PIN_P1_1_GPIO0_8        LPC_PIN_ID(1, 1, 0, 8, 0)
#define LPC_PIN_P1_1_CTOUT_7        LPC_PIN_ID(1, 1, 0, 8, 1)
#define LPC_PIN_P1_1_EMC_A6         LPC_PIN_ID(1, 1, 0, 8, 2)
#define LPC_PIN_P1_1_SGPIO8         LPC_PIN_ID(1, 1, 0, 8, 3)
#define LPC_PIN_P1_1_SSP0_MISO      LPC_PIN_ID(1, 1, 0, 8, 5)

#define LPC_PIN_P1_2_GPIO0_9        LPC_PIN_ID(1, 2, 0, 9, 0)
#define LPC_PIN_P1_2_CTOUT_6        LPC_PIN_ID(1, 2, 0, 9, 1)
#define LPC_PIN_P1_2_EMC_A7         LPC_PIN_ID(1, 2, 0, 9, 2)
#define LPC_PIN_P1_2_SGPIO9         LPC_PIN_ID(1, 2, 0, 9, 3)
#define LPC_PIN_P1_2_SSP0_MOSI      LPC_PIN_ID(1, 2, 0, 9, 5)

#define LPC_PIN_P1_3_GPIO0_10       LPC_PIN_ID(1, 3, 0, 10, 0)
#define LPC_PIN_P1_3_CTOUT_8        LPC_PIN_ID(1, 3, 0, 10, 1)
#define LPC_PIN_P1_3_SGPIO10        LPC_PIN_ID(1, 3, 0, 10, 2)
#define LPC_PIN_P1_3_EMC_OE         LPC_PIN_ID(1, 3, 0, 10, 3)
#define LPC_PIN_P1_3_USB0_IND1      LPC_PIN_ID(1, 3, 0, 10, 4)
#define LPC_PIN_P1_3_SSP1_MISO      LPC_PIN_ID(1, 3, 0, 10, 5)
#define LPC_PIN_P1_3_SD_RST         LPC_PIN_ID(1, 3, 0, 10, 7)

#define LPC_PIN_P1_4_GPIO0_11       LPC_PIN_ID(1, 4, 0, 11, 0)
#define LPC_PIN_P1_4_CTOUT_9        LPC_PIN_ID(1, 4, 0, 11, 1)
#define LPC_PIN_P1_4_SGPIO11        LPC_PIN_ID(1, 4, 0, 11, 2)
#define LPC_PIN_P1_4_EMC_BLS0       LPC_PIN_ID(1, 4, 0, 11, 3)
#define LPC_PIN_P1_4_USB0_IND0      LPC_PIN_ID(1, 4, 0, 11, 4)
#define LPC_PIN_P1_4_SSP1_MOSI      LPC_PIN_ID(1, 4, 0, 11, 5)
#define LPC_PIN_P1_4_SD_VOLT1       LPC_PIN_ID(1, 4, 0, 11, 7)

#define LPC_PIN_P1_5_GPIO1_8        LPC_PIN_ID(1, 5, 1, 8, 0)
#define LPC_PIN_P1_5_CTOUT_10       LPC_PIN_ID(1, 5, 1, 8, 1)
#define LPC_PIN_P1_5_EMC_CS0        LPC_PIN_ID(1, 5, 1, 8, 3)
#define LPC_PIN_P1_5_USB0_PWR_FAULT LPC_PIN_ID(1, 5, 1, 8, 4)
#define LPC_PIN_P1_5_SSP1_SSEL      LPC_PIN_ID(1, 5, 1, 8, 5)
#define LPC_PIN_P1_5_SGPIO15        LPC_PIN_ID(1, 5, 1, 8, 6)
#define LPC_PIN_P1_5_SD_POW         LPC_PIN_ID(1, 5, 1, 8, 7)

#define LPC_PIN_P1_6_GPIO1_9        LPC_PIN_ID(1, 6, 1, 9, 0)
#define LPC_PIN_P1_6_CTIN_5         LPC_PIN_ID(1, 6, 1, 9, 1)
#define LPC_PIN_P1_6_EMC_WE         LPC_PIN_ID(1, 6, 1, 9, 3)
#define LPC_PIN_P1_6_SGPIO14        LPC_PIN_ID(1, 6, 1, 9, 6)
#define LPC_PIN_P1_6_SD_CMD         LPC_PIN_ID(1, 6, 1, 9, 7)

#define LPC_PIN_P1_7_GPIO1_0        LPC_PIN_ID(1, 7, 1, 0, 0)
#define LPC_PIN_P1_7_U1_DSR         LPC_PIN_ID(1, 7, 1, 0, 1)
#define LPC_PIN_P1_7_CTOUT_13       LPC_PIN_ID(1, 7, 1, 0, 2)
#define LPC_PIN_P1_7_EMC_D0         LPC_PIN_ID(1, 7, 1, 0, 3)
#define LPC_PIN_P1_7_USB0_PPWR      LPC_PIN_ID(1, 7, 1, 0, 4)

#define LPC_PIN_P1_8_GPIO1_1        LPC_PIN_ID(1, 8, 1, 1, 0)
#define LPC_PIN_P1_8_U1_DTR         LPC_PIN_ID(1, 8, 1, 1, 1)
#define LPC_PIN_P1_8_CTOUT_12       LPC_PIN_ID(1, 8, 1, 1, 2)
#define LPC_PIN_P1_8_EMC_D1         LPC_PIN_ID(1, 8, 1, 1, 3)
#define LPC_PIN_P1_8_SD_VOLT0       LPC_PIN_ID(1, 8, 1, 1, 7)

#define LPC_PIN_P1_9_GPIO1_2        LPC_PIN_ID(1, 9, 1, 2, 0)
#define LPC_PIN_P1_9_U1_RTS         LPC_PIN_ID(1, 9, 1, 2, 1)
#define LPC_PIN_P1_9_CTOUT_11       LPC_PIN_ID(1, 9, 1, 2, 2)
#define LPC_PIN_P1_9_EMC_D2         LPC_PIN_ID(1, 9, 1, 2, 3)
#define LPC_PIN_P1_9_SD_DAT0        LPC_PIN_ID(1, 9, 1, 2, 7)

#define LPC_PIN_P1_10_GPIO1_3       LPC_PIN_ID(1, 10, 1, 3, 0)
#define LPC_PIN_P1_10_U1_RI         LPC_PIN_ID(1, 10, 1, 3, 1)
#define LPC_PIN_P1_10_CTOUT_14      LPC_PIN_ID(1, 10, 1, 3, 2)
#define LPC_PIN_P1_10_EMC_D3        LPC_PIN_ID(1, 10, 1, 3, 3)
#define LPC_PIN_P1_10_SD_DAT1       LPC_PIN_ID(1, 10, 1, 3, 7)

#define LPC_PIN_P1_11_GPIO1_4       LPC_PIN_ID(1, 11, 1, 4, 0)
#define LPC_PIN_P1_11_U1_CTS        LPC_PIN_ID(1, 11, 1, 4, 1)
#define LPC_PIN_P1_11_CTOUT_15      LPC_PIN_ID(1, 11, 1, 4, 2)
#define LPC_PIN_P1_11_EMC_D4        LPC_PIN_ID(1, 11, 1, 4, 3)
#define LPC_PIN_P1_11_SD_DAT2       LPC_PIN_ID(1, 11, 1, 4, 7)

#define LPC_PIN_P1_12_GPIO1_5       LPC_PIN_ID(1, 12, 1, 5, 0)
#define LPC_PIN_P1_12_U1_DCD        LPC_PIN_ID(1, 12, 1, 5, 1)
#define LPC_PIN_P1_12_EMC_D5        LPC_PIN_ID(1, 12, 1, 5, 3)
#define LPC_PIN_P1_12_T0_CAP1       LPC_PIN_ID(1, 12, 1, 5, 4)
#define LPC_PIN_P1_12_SGPIO8        LPC_PIN_ID(1, 12, 1, 5, 6)
#define LPC_PIN_P1_12_SD_DAT3       LPC_PIN_ID(1, 12, 1, 5, 7)

#define LPC_PIN_P1_13_GPIO1_6       LPC_PIN_ID(1, 13, 1, 6, 0)
#define LPC_PIN_P1_13_U1_TXD        LPC_PIN_ID(1, 13, 1, 6, 1)
#define LPC_PIN_P1_13_EMC_D6        LPC_PIN_ID(1, 13, 1, 6, 3)
#define LPC_PIN_P1_13_T0_CAP0       LPC_PIN_ID(1, 13, 1, 6, 4)
#define LPC_PIN_P1_13_SGPIO9        LPC_PIN_ID(1, 13, 1, 6, 6)
#define LPC_PIN_P1_13_SD_CD         LPC_PIN_ID(1, 13, 1, 6, 7)

#define LPC_PIN_P1_14_GPIO1_7       LPC_PIN_ID(1, 14, 1, 7, 0)
#define LPC_PIN_P1_14_U1_RXD        LPC_PIN_ID(1, 14, 1, 7, 1)
#define LPC_PIN_P1_14_EMC_D7        LPC_PIN_ID(1, 14, 1, 7, 3)
#define LPC_PIN_P1_14_T0_MAT2       LPC_PIN_ID(1, 14, 1, 7, 4)
#define LPC_PIN_P1_14_SGPIO10       LPC_PIN_ID(1, 14, 1, 7, 6)

#define LPC_PIN_P1_15_GPIO0_2       LPC_PIN_ID(1, 15, 0, 2, 0)
#define LPC_PIN_P1_15_U2_TXD        LPC_PIN_ID(1, 15, 0, 2, 1)
#define LPC_PIN_P1_15_SGPIO2        LPC_PIN_ID(1, 15, 0, 2, 2)
#define LPC_PIN_P1_15_ENET_RXD0     LPC_PIN_ID(1, 15, 0, 2, 3)
#define LPC_PIN_P1_15_T0_MAT1       LPC_PIN_ID(1, 15, 0, 2, 4)

#define LPC_PIN_P1_16_GPIO0_3       LPC_PIN_ID(1, 16, 0, 3, 0)
#define LPC_PIN_P1_16_U2_RXD        LPC_PIN_ID(1, 16, 0, 3, 1)
#define LPC_PIN_P1_16_SGPIO3        LPC_PIN_ID(1, 16, 0, 3, 2)
#define LPC_PIN_P1_16_ENET_CRS      LPC_PIN_ID(1, 16, 0, 3, 3)
#define LPC_PIN_P1_16_T0_MAT0       LPC_PIN_ID(1, 16, 0, 3, 4)
#define LPC_PIN_P1_16_ENET_RX_DV    LPC_PIN_ID(1, 16, 0, 3, 7)

#define LPC_PIN_P1_17_GPIO0_12      LPC_PIN_ID(1, 17, 0, 12, 0)
#define LPC_PIN_P1_17_U2_UCLK       LPC_PIN_ID(1, 17, 0, 12, 1)
#define LPC_PIN_P1_17_ENET_MDIO     LPC_PIN_ID(1, 17, 0, 12, 3)
#define LPC_PIN_P1_17_T0_CAP3       LPC_PIN_ID(1, 17, 0, 12, 4)
#define LPC_PIN_P1_17_CAN1_TD       LPC_PIN_ID(1, 17, 0, 12, 5)
#define LPC_PIN_P1_17_SGPIO11       LPC_PIN_ID(1, 17, 0, 12, 6)

#define LPC_PIN_P1_18_GPIO0_13      LPC_PIN_ID(1, 18, 0, 13, 0)
#define LPC_PIN_P1_18_U2_DIR        LPC_PIN_ID(1, 18, 0, 13, 1)
#define LPC_PIN_P1_18_ENET_TXD0     LPC_PIN_ID(1, 18, 0, 13, 3)
#define LPC_PIN_P1_18_T0_MAT3       LPC_PIN_ID(1, 18, 0, 13, 4)
#define LPC_PIN_P1_18_CAN1_RD       LPC_PIN_ID(1, 18, 0, 13, 5)
#define LPC_PIN_P1_18_SGPIO12       LPC_PIN_ID(1, 18, 0, 13, 6)

#define LPC_PIN_P1_19_ENET_TX_CLK   LPC_PIN_ID(1, 19, 8, 0, 0) /* a.k.a. ENET_REF_CLK */
#define LPC_PIN_P1_19_SSP1_SCK      LPC_PIN_ID(1, 19, 8, 0, 1)
#define LPC_PIN_P1_19_CLKOUT        LPC_PIN_ID(1, 19, 8, 0, 4)
#define LPC_PIN_P1_19_I2S0_RX_MCLK  LPC_PIN_ID(1, 19, 8, 0, 6)
#define LPC_PIN_P1_19_I2S1_TX_SCK   LPC_PIN_ID(1, 19, 8, 0, 7)

#define LPC_PIN_P1_20_GPIO0_15      LPC_PIN_ID(1, 20, 0, 15, 0)
#define LPC_PIN_P1_20_SSP1_SSEL     LPC_PIN_ID(1, 20, 0, 15, 1)
#define LPC_PIN_P1_20_ENET_TXD1     LPC_PIN_ID(1, 20, 0, 15, 3)
#define LPC_PIN_P1_20_T0_CAP2       LPC_PIN_ID(1, 20, 0, 15, 4)
#define LPC_PIN_P1_20_SGPIO13       LPC_PIN_ID(1, 20, 0, 15, 6)

/* P2 */
#define LPC_PIN_P2_0_SGPIO4         LPC_PIN_ID(2, 0, 5, 0, 0)
#define LPC_PIN_P2_0_U0_TXD         LPC_PIN_ID(2, 0, 5, 0, 1)
#define LPC_PIN_P2_0_EMC_A13        LPC_PIN_ID(2, 0, 5, 0, 2)
#define LPC_PIN_P2_0_USB0_PPWR      LPC_PIN_ID(2, 0, 5, 0, 3)
#define LPC_PIN_P2_0_GPIO5_0        LPC_PIN_ID(2, 0, 5, 0, 4)
#define LPC_PIN_P2_0_T3_CAP0        LPC_PIN_ID(2, 0, 5, 0, 6)
#define LPC_PIN_P2_0_ENET_MDC       LPC_PIN_ID(2, 0, 5, 0, 7)

#define LPC_PIN_P2_1_SGPIO5         LPC_PIN_ID(2, 1, 5, 1, 0)
#define LPC_PIN_P2_1_U0_RXD         LPC_PIN_ID(2, 1, 5, 1, 1)
#define LPC_PIN_P2_1_EMC_A12        LPC_PIN_ID(2, 1, 5, 1, 2)
#define LPC_PIN_P2_1_USB0_PWR_FAULT LPC_PIN_ID(2, 1, 5, 1, 3)
#define LPC_PIN_P2_1_GPIO5_1        LPC_PIN_ID(2, 1, 5, 1, 4)
#define LPC_PIN_P2_1_T3_CAP1        LPC_PIN_ID(2, 1, 5, 1, 6)

#define LPC_PIN_P2_2_SGPIO6         LPC_PIN_ID(2, 2, 5, 2, 0)
#define LPC_PIN_P2_2_U0_UCLK        LPC_PIN_ID(2, 2, 5, 2, 1)
#define LPC_PIN_P2_2_EMC_A11        LPC_PIN_ID(2, 2, 5, 2, 2)
#define LPC_PIN_P2_2_USB0_IND1      LPC_PIN_ID(2, 2, 5, 2, 3)
#define LPC_PIN_P2_2_GPIO5_2        LPC_PIN_ID(2, 2, 5, 2, 4)
#define LPC_PIN_P2_2_CTIN_6         LPC_PIN_ID(2, 2, 5, 2, 5)
#define LPC_PIN_P2_2_T3_CAP2        LPC_PIN_ID(2, 2, 5, 2, 6)

#define LPC_PIN_P2_3_SGPIO12        LPC_PIN_ID(2, 3, 5, 3, 0)
#define LPC_PIN_P2_3_I2C1_SDA       LPC_PIN_ID(2, 3, 5, 3, 1)
#define LPC_PIN_P2_3_U3_TXD         LPC_PIN_ID(2, 3, 5, 3, 2)
#define LPC_PIN_P2_3_CTIN_1         LPC_PIN_ID(2, 3, 5, 3, 3)
#define LPC_PIN_P2_3_GPIO5_3        LPC_PIN_ID(2, 3, 5, 3, 4)
#define LPC_PIN_P2_3_T3_MAT0        LPC_PIN_ID(2, 3, 5, 3, 6)
#define LPC_PIN_P2_3_USB0_PPWR      LPC_PIN_ID(2, 3, 5, 3, 7)

#define LPC_PIN_P2_4_SGPIO13        LPC_PIN_ID(2, 4, 5, 4, 0)
#define LPC_PIN_P2_4_I2C1_SCL       LPC_PIN_ID(2, 4, 5, 4, 1)
#define LPC_PIN_P2_4_U3_RXD         LPC_PIN_ID(2, 4, 5, 4, 2)
#define LPC_PIN_P2_4_CTIN_0         LPC_PIN_ID(2, 4, 5, 4, 3)
#define LPC_PIN_P2_4_GPIO5_4        LPC_PIN_ID(2, 4, 5, 4, 4)
#define LPC_PIN_P2_4_T3_MAT1        LPC_PIN_ID(2, 4, 5, 4, 6)
#define LPC_PIN_P2_4_USB0_PWR_FAULT LPC_PIN_ID(2, 4, 5, 4, 7)

#define LPC_PIN_P2_5_SGPIO14        LPC_PIN_ID(2, 5, 5, 5, 0)
#define LPC_PIN_P2_5_CTIN_2         LPC_PIN_ID(2, 5, 5, 5, 1)
#define LPC_PIN_P2_5_USB1_VBUS      LPC_PIN_ID(2, 5, 5, 5, 2)
#define LPC_PIN_P2_5_ADCTRIG1       LPC_PIN_ID(2, 5, 5, 5, 3)
#define LPC_PIN_P2_5_GPIO5_5        LPC_PIN_ID(2, 5, 5, 5, 4)
#define LPC_PIN_P2_5_T3_MAT2        LPC_PIN_ID(2, 5, 5, 5, 6)
#define LPC_PIN_P2_5_USB0_IND0      LPC_PIN_ID(2, 5, 5, 5, 7)

#define LPC_PIN_P2_6_SGPIO7         LPC_PIN_ID(2, 6, 5, 6, 0)
#define LPC_PIN_P2_6_U0_DIR         LPC_PIN_ID(2, 6, 5, 6, 1)
#define LPC_PIN_P2_6_EMC_A10        LPC_PIN_ID(2, 6, 5, 6, 2)
#define LPC_PIN_P2_6_USB0_IND0      LPC_PIN_ID(2, 6, 5, 6, 3)
#define LPC_PIN_P2_6_GPIO5_6        LPC_PIN_ID(2, 6, 5, 6, 4)
#define LPC_PIN_P2_6_CTIN_7         LPC_PIN_ID(2, 6, 5, 6, 5)
#define LPC_PIN_P2_6_T3_CAP3        LPC_PIN_ID(2, 6, 5, 6, 6)

#define LPC_PIN_P2_7_GPIO0_7        LPC_PIN_ID(2, 7, 0, 7, 0)
#define LPC_PIN_P2_7_CTOUT_1        LPC_PIN_ID(2, 7, 0, 7, 1)
#define LPC_PIN_P2_7_U3_UCLK        LPC_PIN_ID(2, 7, 0, 7, 2)
#define LPC_PIN_P2_7_EMC_A9         LPC_PIN_ID(2, 7, 0, 7, 3)
#define LPC_PIN_P2_7_T3_MAT3        LPC_PIN_ID(2, 7, 0, 7, 6)

#define LPC_PIN_P2_8_SGPIO15        LPC_PIN_ID(2, 8, 5, 7, 0)
#define LPC_PIN_P2_8_CTOUT_0        LPC_PIN_ID(2, 8, 5, 7, 1)
#define LPC_PIN_P2_8_U3_DIR         LPC_PIN_ID(2, 8, 5, 7, 2)
#define LPC_PIN_P2_8_EMC_A8         LPC_PIN_ID(2, 8, 5, 7, 3)
#define LPC_PIN_P2_8_GPIO5_7        LPC_PIN_ID(2, 8, 5, 7, 4)

#define LPC_PIN_P2_9_GPIO1_10       LPC_PIN_ID(2, 9, 1, 10, 0)
#define LPC_PIN_P2_9_CTOUT_3        LPC_PIN_ID(2, 9, 1, 10, 1)
#define LPC_PIN_P2_9_U3_BAUD        LPC_PIN_ID(2, 9, 1, 10, 2)
#define LPC_PIN_P2_9_EMC_A0         LPC_PIN_ID(2, 9, 1, 10, 3)

#define LPC_PIN_P2_10_GPIO0_14      LPC_PIN_ID(2, 10, 0, 14, 0)
#define LPC_PIN_P2_10_CTOUT_2       LPC_PIN_ID(2, 10, 0, 14, 1)
#define LPC_PIN_P2_10_U2_TXD        LPC_PIN_ID(2, 10, 0, 14, 2)
#define LPC_PIN_P2_10_EMC_A1        LPC_PIN_ID(2, 10, 0, 14, 3)

#define LPC_PIN_P2_11_GPIO1_11      LPC_PIN_ID(2, 11, 1, 11, 0)
#define LPC_PIN_P2_11_CTOUT_5       LPC_PIN_ID(2, 11, 1, 11, 1)
#define LPC_PIN_P2_11_U2_RXD        LPC_PIN_ID(2, 11, 1, 11, 2)
#define LPC_PIN_P2_11_EMC_A2        LPC_PIN_ID(2, 11, 1, 11, 3)

#define LPC_PIN_P2_12_GPIO1_12      LPC_PIN_ID(2, 12, 1, 12, 0)
#define LPC_PIN_P2_12_CTOUT_4       LPC_PIN_ID(2, 12, 1, 12, 1)
#define LPC_PIN_P2_12_EMC_A3        LPC_PIN_ID(2, 12, 1, 12, 3)
#define LPC_PIN_P2_12_U2_UCLK       LPC_PIN_ID(2, 12, 1, 12, 7)

#define LPC_PIN_P2_13_GPIO1_13      LPC_PIN_ID(2, 13, 1, 13, 0)
#define LPC_PIN_P2_13_CTIN_4        LPC_PIN_ID(2, 13, 1, 13, 1)
#define LPC_PIN_P2_13_EMC_A4        LPC_PIN_ID(2, 13, 1, 13, 3)
#define LPC_PIN_P2_13_U2_DIR        LPC_PIN_ID(2, 13, 1, 13, 7)

/* P3 */
#define LPC_PIN_P3_0_I2S0_RX_SCK    LPC_PIN_ID(3, 0, 8, 0, 0)
#define LPC_PIN_P3_0_I2S0_RX_MCLK   LPC_PIN_ID(3, 0, 8, 0, 1)
#define LPC_PIN_P3_0_I2S0_TX_SCK    LPC_PIN_ID(3, 0, 8, 0, 2)
#define LPC_PIN_P3_0_I2S0_TX_MCLK   LPC_PIN_ID(3, 0, 8, 0, 3)
#define LPC_PIN_P3_0_SSP0_SCK       LPC_PIN_ID(3, 0, 8, 0, 4)

#define LPC_PIN_P3_1_I2S0_TX_WS     LPC_PIN_ID(3, 1, 5, 8, 0)
#define LPC_PIN_P3_1_I2S0_RX_WS     LPC_PIN_ID(3, 1, 5, 8, 1)
#define LPC_PIN_P3_1_CAN0_RD        LPC_PIN_ID(3, 1, 5, 8, 2)
#define LPC_PIN_P3_1_USB1_IND1      LPC_PIN_ID(3, 1, 5, 8, 3)
#define LPC_PIN_P3_1_GPIO5_8        LPC_PIN_ID(3, 1, 5, 8, 4)
#define LPC_PIN_P3_1_LCD_VD15       LPC_PIN_ID(3, 1, 5, 8, 6)

#define LPC_PIN_P3_2_I2S0_TX_SDA    LPC_PIN_ID(3, 2, 5, 9, 0)
#define LPC_PIN_P3_2_I2S0_RX_SDA    LPC_PIN_ID(3, 2, 5, 9, 1)
#define LPC_PIN_P3_2_CAN0_TD        LPC_PIN_ID(3, 2, 5, 9, 2)
#define LPC_PIN_P3_2_USB1_IND0      LPC_PIN_ID(3, 2, 5, 9, 3)
#define LPC_PIN_P3_2_GPIO5_9        LPC_PIN_ID(3, 2, 5, 9, 4)
#define LPC_PIN_P3_2_LCD_VD14       LPC_PIN_ID(3, 2, 5, 9, 6)

#define LPC_PIN_P3_3_SPI_SCK        LPC_PIN_ID(3, 3, 8, 0, 1)
#define LPC_PIN_P3_3_SSP0_SCK       LPC_PIN_ID(3, 3, 8, 0, 2)
#define LPC_PIN_P3_3_SPIFI_SCK      LPC_PIN_ID(3, 3, 8, 0, 3)
#define LPC_PIN_P3_3_CGU_OUT1       LPC_PIN_ID(3, 3, 8, 0, 4)
#define LPC_PIN_P3_3_I2S0_TX_MCLK   LPC_PIN_ID(3, 3, 8, 0, 6)
#define LPC_PIN_P3_3_I2S1_TX_SCK    LPC_PIN_ID(3, 3, 8, 0, 7)

#define LPC_PIN_P3_4_GPIO1_14       LPC_PIN_ID(3, 4, 1, 14, 0)
#define LPC_PIN_P3_4_SPIFI_SIO3     LPC_PIN_ID(3, 4, 1, 14, 3)
#define LPC_PIN_P3_4_U1_TXD         LPC_PIN_ID(3, 4, 1, 14, 4)
#define LPC_PIN_P3_4_I2S0_TX_WS     LPC_PIN_ID(3, 4, 1, 14, 5)
#define LPC_PIN_P3_4_I2S1_RX_SDA    LPC_PIN_ID(3, 4, 1, 14, 6)
#define LPC_PIN_P3_4_LCD_VD13       LPC_PIN_ID(3, 4, 1, 14, 7)

#define LPC_PIN_P3_5_GPIO1_15       LPC_PIN_ID(3, 5, 1, 15, 0)
#define LPC_PIN_P3_5_SPIFI_SIO2     LPC_PIN_ID(3, 5, 1, 15, 3)
#define LPC_PIN_P3_5_U1_RXD         LPC_PIN_ID(3, 5, 1, 15, 4)
#define LPC_PIN_P3_5_I2S0_TX_SDA    LPC_PIN_ID(3, 5, 1, 15, 5)
#define LPC_PIN_P3_5_I2S1_RX_WS     LPC_PIN_ID(3, 5, 1, 15, 6)
#define LPC_PIN_P3_5_LCD_VD12       LPC_PIN_ID(3, 5, 1, 15, 7)

#define LPC_PIN_P3_6_GPIO0_6        LPC_PIN_ID(3, 6, 0, 6, 0)
#define LPC_PIN_P3_6_SPI_MISO       LPC_PIN_ID(3, 6, 0, 6, 1)
#define LPC_PIN_P3_6_SSP0_SSEL      LPC_PIN_ID(3, 6, 0, 6, 2)
#define LPC_PIN_P3_6_SPIFI_MISO     LPC_PIN_ID(3, 6, 0, 6, 3)
#define LPC_PIN_P3_6_SSP0_MISO      LPC_PIN_ID(3, 6, 0, 6, 5)

#define LPC_PIN_P3_7_SPI_MOSI       LPC_PIN_ID(3, 7, 5, 10, 1)
#define LPC_PIN_P3_7_SSP0_MISO      LPC_PIN_ID(3, 7, 5, 10, 2)
#define LPC_PIN_P3_7_SPIFI_MOSI     LPC_PIN_ID(3, 7, 5, 10, 3)
#define LPC_PIN_P3_7_GPIO5_10       LPC_PIN_ID(3, 7, 5, 10, 4)
#define LPC_PIN_P3_7_SSP0_MOSI      LPC_PIN_ID(3, 7, 5, 10, 5)

#define LPC_PIN_P3_8_SPI_SSEL       LPC_PIN_ID(3, 8, 5, 11, 1)
#define LPC_PIN_P3_8_SSP0_MOSI      LPC_PIN_ID(3, 8, 5, 11, 2)
#define LPC_PIN_P3_8_SPIFI_CS       LPC_PIN_ID(3, 8, 5, 11, 3)
#define LPC_PIN_P3_8_GPIO5_11       LPC_PIN_ID(3, 8, 5, 11, 4)
#define LPC_PIN_P3_8_SSP0_SSEL      LPC_PIN_ID(3, 8, 5, 11, 5)

/* P4 */
#define LPC_PIN_P4_0_GPIO2_0        LPC_PIN_ID(4, 0, 2, 0, 0)
#define LPC_PIN_P4_0_MCOA0          LPC_PIN_ID(4, 0, 2, 0, 1)
#define LPC_PIN_P4_0_NMI            LPC_PIN_ID(4, 0, 2, 0, 2)
#define LPC_PIN_P4_0_LCD_VD13       LPC_PIN_ID(4, 0, 2, 0, 5)
#define LPC_PIN_P4_0_U3_UCLK        LPC_PIN_ID(4, 0, 2, 0, 6)

#define LPC_PIN_P4_1_GPIO2_1        LPC_PIN_ID(4, 1, 2, 1, 0)
#define LPC_PIN_P4_1_CTOUT_1        LPC_PIN_ID(4, 1, 2, 1, 1)
#define LPC_PIN_P4_1_LCD_VD0        LPC_PIN_ID(4, 1, 2, 1, 2)
#define LPC_PIN_P4_1_LCD_VD19       LPC_PIN_ID(4, 1, 2, 1, 5)
#define LPC_PIN_P4_1_U3_TXD         LPC_PIN_ID(4, 1, 2, 1, 6)
#define LPC_PIN_P4_1_ENET_COL       LPC_PIN_ID(4, 1, 2, 1, 7)

#define LPC_PIN_P4_2_GPIO2_2        LPC_PIN_ID(4, 2, 2, 2, 0)
#define LPC_PIN_P4_2_CTOUT_0        LPC_PIN_ID(4, 2, 2, 2, 1)
#define LPC_PIN_P4_2_LCD_VD3        LPC_PIN_ID(4, 2, 2, 2, 2)
#define LPC_PIN_P4_2_LCD_VD12       LPC_PIN_ID(4, 2, 2, 2, 5)
#define LPC_PIN_P4_2_U3_RXD         LPC_PIN_ID(4, 2, 2, 2, 6)
#define LPC_PIN_P4_2_SGPIO8         LPC_PIN_ID(4, 2, 2, 2, 7)

#define LPC_PIN_P4_3_GPIO2_3        LPC_PIN_ID(4, 3, 2, 3, 0)
#define LPC_PIN_P4_3_CTOUT_3        LPC_PIN_ID(4, 3, 2, 3, 1)
#define LPC_PIN_P4_3_LCD_VD2        LPC_PIN_ID(4, 3, 2, 3, 2)
#define LPC_PIN_P4_3_LCD_VD21       LPC_PIN_ID(4, 3, 2, 3, 5)
#define LPC_PIN_P4_3_U3_BAUD        LPC_PIN_ID(4, 3, 2, 3, 6)
#define LPC_PIN_P4_3_SGPIO9         LPC_PIN_ID(4, 3, 2, 3, 7)

#define LPC_PIN_P4_4_GPIO2_4        LPC_PIN_ID(4, 4, 2, 4, 0)
#define LPC_PIN_P4_4_CTOUT_2        LPC_PIN_ID(4, 4, 2, 4, 1)
#define LPC_PIN_P4_4_LCD_VD1        LPC_PIN_ID(4, 4, 2, 4, 2)
#define LPC_PIN_P4_4_LCD_VD20       LPC_PIN_ID(4, 4, 2, 4, 5)
#define LPC_PIN_P4_4_U3_DIR         LPC_PIN_ID(4, 4, 2, 4, 6)
#define LPC_PIN_P4_4_SGPIO10        LPC_PIN_ID(4, 4, 2, 4, 7)

#define LPC_PIN_P4_5_GPIO2_5        LPC_PIN_ID(4, 5, 2, 5, 0)
#define LPC_PIN_P4_5_CTOUT_5        LPC_PIN_ID(4, 5, 2, 5, 1)
#define LPC_PIN_P4_5_LCD_FP         LPC_PIN_ID(4, 5, 2, 5, 2)
#define LPC_PIN_P4_5_SGPIO11        LPC_PIN_ID(4, 5, 2, 5, 7)

#define LPC_PIN_P4_6_GPIO2_6        LPC_PIN_ID(4, 6, 2, 6, 0)
#define LPC_PIN_P4_6_CTOUT_4        LPC_PIN_ID(4, 6, 2, 6, 1)
#define LPC_PIN_P4_6_LCD_ENAB_M     LPC_PIN_ID(4, 6, 2, 6, 2)
#define LPC_PIN_P4_6_SGPIO12        LPC_PIN_ID(4, 6, 2, 6, 7)

#define LPC_PIN_P4_7_LCD_DCLK       LPC_PIN_ID(4, 7, 8, 0, 0)
#define LPC_PIN_P4_7_GP_CLKIN       LPC_PIN_ID(4, 7, 8, 0, 1)
#define LPC_PIN_P4_7_I2S1_TX_SCK    LPC_PIN_ID(4, 7, 8, 0, 6)
#define LPC_PIN_P4_7_I2S0_TX_SCK    LPC_PIN_ID(4, 7, 8, 0, 7)

#define LPC_PIN_P4_8_CTIN_5         LPC_PIN_ID(4, 8, 5, 12, 1)
#define LPC_PIN_P4_8_LCD_VD9        LPC_PIN_ID(4, 8, 5, 12, 2)
#define LPC_PIN_P4_8_GPIO5_12       LPC_PIN_ID(4, 8, 5, 12, 4)
#define LPC_PIN_P4_8_LCD_VD22       LPC_PIN_ID(4, 8, 5, 12, 5)
#define LPC_PIN_P4_8_CAN1_TD        LPC_PIN_ID(4, 8, 5, 12, 6)
#define LPC_PIN_P4_8_SGPIO13        LPC_PIN_ID(4, 8, 5, 12, 7)

#define LPC_PIN_P4_9_CTIN_6         LPC_PIN_ID(4, 9, 5, 13, 1)
#define LPC_PIN_P4_9_LCD_VD11       LPC_PIN_ID(4, 9, 5, 13, 2)
#define LPC_PIN_P4_9_GPIO5_13       LPC_PIN_ID(4, 9, 5, 13, 4)
#define LPC_PIN_P4_9_LCD_VD15       LPC_PIN_ID(4, 9, 5, 13, 5)
#define LPC_PIN_P4_9_CAN1_RD        LPC_PIN_ID(4, 9, 5, 13, 6)
#define LPC_PIN_P4_9_SGPIO14        LPC_PIN_ID(4, 9, 5, 13, 7)

#define LPC_PIN_P4_10_CTIN_2        LPC_PIN_ID(4, 10, 5, 14, 1)
#define LPC_PIN_P4_10_LCD_VD10      LPC_PIN_ID(4, 10, 5, 14, 2)
#define LPC_PIN_P4_10_GPIO5_14      LPC_PIN_ID(4, 10, 5, 14, 4)
#define LPC_PIN_P4_10_LCD_VD14      LPC_PIN_ID(4, 10, 5, 14, 5)
#define LPC_PIN_P4_10_SGPIO15       LPC_PIN_ID(4, 10, 5, 14, 7)

/* P5 */
#define LPC_PIN_P5_0_GPIO2_9        LPC_PIN_ID(5, 0, 2, 9, 0)
#define LPC_PIN_P5_0_MCOB2          LPC_PIN_ID(5, 0, 2, 9, 1)
#define LPC_PIN_P5_0_EMC_D12        LPC_PIN_ID(5, 0, 2, 9, 2)
#define LPC_PIN_P5_0_U1_DSR         LPC_PIN_ID(5, 0, 2, 9, 4)
#define LPC_PIN_P5_0_T1_CAP0        LPC_PIN_ID(5, 0, 2, 9, 5)

#define LPC_PIN_P5_1_GPIO2_10       LPC_PIN_ID(5, 1, 2, 10, 0)
#define LPC_PIN_P5_1_MCI2           LPC_PIN_ID(5, 1, 2, 10, 1)
#define LPC_PIN_P5_1_EMC_D13        LPC_PIN_ID(5, 1, 2, 10, 2)
#define LPC_PIN_P5_1_U1_DTR         LPC_PIN_ID(5, 1, 2, 10, 4)
#define LPC_PIN_P5_1_T1_CAP1        LPC_PIN_ID(5, 1, 2, 10, 5)

#define LPC_PIN_P5_2_GPIO2_11       LPC_PIN_ID(5, 2, 2, 11, 0)
#define LPC_PIN_P5_2_MCI1           LPC_PIN_ID(5, 2, 2, 11, 1)
#define LPC_PIN_P5_2_EMC_D14        LPC_PIN_ID(5, 2, 2, 11, 2)
#define LPC_PIN_P5_2_U1_RTS         LPC_PIN_ID(5, 2, 2, 11, 4)
#define LPC_PIN_P5_2_T1_CAP2        LPC_PIN_ID(5, 2, 2, 11, 5)

#define LPC_PIN_P5_3_GPIO2_12       LPC_PIN_ID(5, 3, 2, 12, 0)
#define LPC_PIN_P5_3_MCI0           LPC_PIN_ID(5, 3, 2, 12, 1)
#define LPC_PIN_P5_3_EMC_D15        LPC_PIN_ID(5, 3, 2, 12, 2)
#define LPC_PIN_P5_3_U1_RI          LPC_PIN_ID(5, 3, 2, 12, 4)
#define LPC_PIN_P5_3_T1_CAP3        LPC_PIN_ID(5, 3, 2, 12, 5)

#define LPC_PIN_P5_4_GPIO2_13       LPC_PIN_ID(5, 4, 2, 13, 0)
#define LPC_PIN_P5_4_MCOB0          LPC_PIN_ID(5, 4, 2, 13, 1)
#define LPC_PIN_P5_4_EMC_D8         LPC_PIN_ID(5, 4, 2, 13, 2)
#define LPC_PIN_P5_4_U1_CTS         LPC_PIN_ID(5, 4, 2, 13, 4)
#define LPC_PIN_P5_4_T1_MAT0        LPC_PIN_ID(5, 4, 2, 13, 5)

#define LPC_PIN_P5_5_GPIO2_14       LPC_PIN_ID(5, 5, 2, 14, 0)
#define LPC_PIN_P5_5_MCOA1          LPC_PIN_ID(5, 5, 2, 14, 1)
#define LPC_PIN_P5_5_EMC_D9         LPC_PIN_ID(5, 5, 2, 14, 2)
#define LPC_PIN_P5_5_U1_DCD         LPC_PIN_ID(5, 5, 2, 14, 4)
#define LPC_PIN_P5_5_T1_MAT1        LPC_PIN_ID(5, 5, 2, 14, 5)

#define LPC_PIN_P5_6_GPIO2_15       LPC_PIN_ID(5, 6, 2, 15, 0)
#define LPC_PIN_P5_6_MCOB1          LPC_PIN_ID(5, 6, 2, 15, 1)
#define LPC_PIN_P5_6_EMC_D10        LPC_PIN_ID(5, 6, 2, 15, 2)
#define LPC_PIN_P5_6_U1_TXD         LPC_PIN_ID(5, 6, 2, 15, 4)
#define LPC_PIN_P5_6_T1_MAT2        LPC_PIN_ID(5, 6, 2, 15, 5)

#define LPC_PIN_P5_7_GPIO2_7        LPC_PIN_ID(5, 7, 2, 7, 0)
#define LPC_PIN_P5_7_MCOA2          LPC_PIN_ID(5, 7, 2, 7, 1)
#define LPC_PIN_P5_7_EMC_D11        LPC_PIN_ID(5, 7, 2, 7, 2)
#define LPC_PIN_P5_7_U1_RXD         LPC_PIN_ID(5, 7, 2, 7, 4)
#define LPC_PIN_P5_7_T1_MAT3        LPC_PIN_ID(5, 7, 2, 7, 5)

/* P6 */
#define LPC_PIN_P6_0_I2S0_RX_MCLK   LPC_PIN_ID(6, 0, 8, 0, 1)
#define LPC_PIN_P6_0_I2S0_RX_SCK    LPC_PIN_ID(6, 0, 8, 0, 4)

#define LPC_PIN_P6_1_GPIO3_0        LPC_PIN_ID(6, 1, 3, 0, 0)
#define LPC_PIN_P6_1_EMC_DYCS1      LPC_PIN_ID(6, 1, 3, 0, 1)
#define LPC_PIN_P6_1_U0_UCLK        LPC_PIN_ID(6, 1, 3, 0, 2)
#define LPC_PIN_P6_1_I2S0_RX_WS     LPC_PIN_ID(6, 1, 3, 0, 3)
#define LPC_PIN_P6_1_T2_CAP0        LPC_PIN_ID(6, 1, 3, 0, 5)

#define LPC_PIN_P6_2_GPIO3_1        LPC_PIN_ID(6, 2, 3, 1, 0)
#define LPC_PIN_P6_2_EMC_CKEOUT1    LPC_PIN_ID(6, 2, 3, 1, 1)
#define LPC_PIN_P6_2_U0_DIR         LPC_PIN_ID(6, 2, 3, 1, 2)
#define LPC_PIN_P6_2_I2S0_RX_SDA    LPC_PIN_ID(6, 2, 3, 1, 3)
#define LPC_PIN_P6_2_T2_CAP1        LPC_PIN_ID(6, 2, 3, 1, 5)

#define LPC_PIN_P6_3_GPIO3_2        LPC_PIN_ID(6, 3, 3, 2, 0)
#define LPC_PIN_P6_3_USB0_PPWR      LPC_PIN_ID(6, 3, 3, 2, 1)
#define LPC_PIN_P6_3_SGPIO4         LPC_PIN_ID(6, 3, 3, 2, 2)
#define LPC_PIN_P6_3_EMC_CS1        LPC_PIN_ID(6, 3, 3, 2, 3)
#define LPC_PIN_P6_3_T2_CAP2        LPC_PIN_ID(6, 3, 3, 2, 5)

#define LPC_PIN_P6_4_GPIO3_3        LPC_PIN_ID(6, 4, 3, 3, 0)
#define LPC_PIN_P6_4_CTIN_6         LPC_PIN_ID(6, 4, 3, 3, 1)
#define LPC_PIN_P6_4_U0_TXD         LPC_PIN_ID(6, 4, 3, 3, 2)
#define LPC_PIN_P6_4_EMC_CAS        LPC_PIN_ID(6, 4, 3, 3, 3)

#define LPC_PIN_P6_5_GPIO3_4        LPC_PIN_ID(6, 5, 3, 4, 0)
#define LPC_PIN_P6_5_CTOUT_6        LPC_PIN_ID(6, 5, 3, 4, 1)
#define LPC_PIN_P6_5_U0_RXD         LPC_PIN_ID(6, 5, 3, 4, 2)
#define LPC_PIN_P6_5_EMC_RAS        LPC_PIN_ID(6, 5, 3, 4, 3)

#define LPC_PIN_P6_6_GPIO0_5        LPC_PIN_ID(6, 6, 0, 5, 0)
#define LPC_PIN_P6_6_EMC_BLS1       LPC_PIN_ID(6, 6, 0, 5, 1)
#define LPC_PIN_P6_6_SGPIO5         LPC_PIN_ID(6, 6, 0, 5, 2)
#define LPC_PIN_P6_6_USB0_PWR_FAULT LPC_PIN_ID(6, 6, 0, 5, 3)
#define LPC_PIN_P6_6_T2_CAP3        LPC_PIN_ID(6, 6, 0, 5, 5)

#define LPC_PIN_P6_7_EMC_A15        LPC_PIN_ID(6, 7, 5, 15, 1)
#define LPC_PIN_P6_7_SGPIO6         LPC_PIN_ID(6, 7, 5, 15, 2)
#define LPC_PIN_P6_7_USB0_IND1      LPC_PIN_ID(6, 7, 5, 15, 3)
#define LPC_PIN_P6_7_GPIO5_15       LPC_PIN_ID(6, 7, 5, 15, 4)
#define LPC_PIN_P6_7_T2_MAT0        LPC_PIN_ID(6, 7, 5, 15, 5)

#define LPC_PIN_P6_8_EMC_A14        LPC_PIN_ID(6, 8, 5, 16, 1)
#define LPC_PIN_P6_8_SGPIO7         LPC_PIN_ID(6, 8, 5, 16, 2)
#define LPC_PIN_P6_8_USB0_IND0      LPC_PIN_ID(6, 8, 5, 16, 3)
#define LPC_PIN_P6_8_GPIO5_16       LPC_PIN_ID(6, 8, 5, 16, 4)
#define LPC_PIN_P6_8_T2_MAT1        LPC_PIN_ID(6, 8, 5, 16, 5)

#define LPC_PIN_P6_9_GPIO3_5        LPC_PIN_ID(6, 9, 3, 5, 0)
#define LPC_PIN_P6_9_EMC_DYCS0      LPC_PIN_ID(6, 9, 3, 5, 3)
#define LPC_PIN_P6_9_T2_MAT2        LPC_PIN_ID(6, 9, 3, 5, 5)

#define LPC_PIN_P6_10_GPIO3_6       LPC_PIN_ID(6, 10, 3, 6, 0)
#define LPC_PIN_P6_10_MCABORT       LPC_PIN_ID(6, 10, 3, 6, 1)
#define LPC_PIN_P6_10_EMC_DQMOUT1   LPC_PIN_ID(6, 10, 3, 6, 3)

#define LPC_PIN_P6_11_GPIO3_7       LPC_PIN_ID(6, 11, 3, 7, 0)
#define LPC_PIN_P6_11_EMC_CKEOUT0   LPC_PIN_ID(6, 11, 3, 7, 3)
#define LPC_PIN_P6_11_T2_MAT3       LPC_PIN_ID(6, 11, 3, 7, 5)

#define LPC_PIN_P6_12_GPIO2_8       LPC_PIN_ID(6, 12, 2, 8, 0)
#define LPC_PIN_P6_12_CTOUT_7       LPC_PIN_ID(6, 12, 2, 8, 1)
#define LPC_PIN_P6_12_EMC_DQMOUT0   LPC_PIN_ID(6, 12, 2, 8, 3)

/* P7 */
#define LPC_PIN_P7_0_GPIO3_8        LPC_PIN_ID(7, 0, 3, 8, 0)
#define LPC_PIN_P7_0_CTOUT_14       LPC_PIN_ID(7, 0, 3, 8, 1)
#define LPC_PIN_P7_0_LCD_LE         LPC_PIN_ID(7, 0, 3, 8, 3)
#define LPC_PIN_P7_0_SGPIO4         LPC_PIN_ID(7, 0, 3, 8, 7)

#define LPC_PIN_P7_1_GPIO3_9        LPC_PIN_ID(7, 1, 3, 9, 0)
#define LPC_PIN_P7_1_CTOUT_15       LPC_PIN_ID(7, 1, 3, 9, 1)
#define LPC_PIN_P7_1_I2S0_TX_WS     LPC_PIN_ID(7, 1, 3, 9, 2)
#define LPC_PIN_P7_1_LCD_VD19       LPC_PIN_ID(7, 1, 3, 9, 3)
#define LPC_PIN_P7_1_LCD_VD7        LPC_PIN_ID(7, 1, 3, 9, 4)
#define LPC_PIN_P7_1_U2_TXD         LPC_PIN_ID(7, 1, 3, 9, 6)
#define LPC_PIN_P7_1_SGPIO5         LPC_PIN_ID(7, 1, 3, 9, 7)

#define LPC_PIN_P7_2_GPIO3_10       LPC_PIN_ID(7, 2, 3, 10, 0)
#define LPC_PIN_P7_2_CTIN_4         LPC_PIN_ID(7, 2, 3, 10, 1)
#define LPC_PIN_P7_2_I2S0_TX_SDA    LPC_PIN_ID(7, 2, 3, 10, 2)
#define LPC_PIN_P7_2_LCD_VD18       LPC_PIN_ID(7, 2, 3, 10, 3)
#define LPC_PIN_P7_2_LCD_VD6        LPC_PIN_ID(7, 2, 3, 10, 4)
#define LPC_PIN_P7_2_U2_RXD         LPC_PIN_ID(7, 2, 3, 10, 6)
#define LPC_PIN_P7_2_SGPIO6         LPC_PIN_ID(7, 2, 3, 10, 7)

#define LPC_PIN_P7_3_GPIO3_11       LPC_PIN_ID(7, 3, 3, 11, 0)
#define LPC_PIN_P7_3_CTIN_3         LPC_PIN_ID(7, 3, 3, 11, 1)
#define LPC_PIN_P7_3_LCD_VD17       LPC_PIN_ID(7, 3, 3, 11, 3)
#define LPC_PIN_P7_3_LCD_VD5        LPC_PIN_ID(7, 3, 3, 11, 4)

#define LPC_PIN_P7_4_GPIO3_12       LPC_PIN_ID(7, 4, 3, 12, 0)
#define LPC_PIN_P7_4_CTOUT_13       LPC_PIN_ID(7, 4, 3, 12, 1)
#define LPC_PIN_P7_4_LCD_VD16       LPC_PIN_ID(7, 4, 3, 12, 3)
#define LPC_PIN_P7_4_LCD_VD4        LPC_PIN_ID(7, 4, 3, 12, 4)
#define LPC_PIN_P7_4_TRACEDATA0     LPC_PIN_ID(7, 4, 3, 12, 5)

#define LPC_PIN_P7_5_GPIO3_13       LPC_PIN_ID(7, 5, 3, 13, 0)
#define LPC_PIN_P7_5_CTOUT_12       LPC_PIN_ID(7, 5, 3, 13, 1)
#define LPC_PIN_P7_5_LCD_VD8        LPC_PIN_ID(7, 5, 3, 13, 3)
#define LPC_PIN_P7_5_LCD_VD23       LPC_PIN_ID(7, 5, 3, 13, 4)
#define LPC_PIN_P7_5_TRACEDATA1     LPC_PIN_ID(7, 5, 3, 13, 5)

#define LPC_PIN_P7_6_GPIO3_14       LPC_PIN_ID(7, 6, 3, 14, 0)
#define LPC_PIN_P7_6_CTOUT_11       LPC_PIN_ID(7, 6, 3, 14, 1)
#define LPC_PIN_P7_6_LCD_LP         LPC_PIN_ID(7, 6, 3, 14, 3)
#define LPC_PIN_P7_6_TRACEDATA2     LPC_PIN_ID(7, 6, 3, 14, 5)

#define LPC_PIN_P7_7_GPIO3_15       LPC_PIN_ID(7, 7, 3, 15, 0)
#define LPC_PIN_P7_7_CTOUT_8        LPC_PIN_ID(7, 7, 3, 15, 1)
#define LPC_PIN_P7_7_LCD_PWR        LPC_PIN_ID(7, 7, 3, 15, 3)
#define LPC_PIN_P7_7_TRACEDATA3     LPC_PIN_ID(7, 7, 3, 15, 5)
#define LPC_PIN_P7_7_ENET_MDC       LPC_PIN_ID(7, 7, 3, 15, 6)
#define LPC_PIN_P7_7_SGPIO7         LPC_PIN_ID(7, 7, 3, 15, 7)

/* P8 */
#define LPC_PIN_P8_0_GPIO4_0        LPC_PIN_ID(8, 0, 4, 0, 0) /* not present on LQFP144 */
#define LPC_PIN_P8_0_USB0_PWR_FAULT LPC_PIN_ID(8, 0, 4, 0, 1) /* not present on LQFP144 */
#define LPC_PIN_P8_0_MCI2           LPC_PIN_ID(8, 0, 4, 0, 3) /* not present on LQFP144 */
#define LPC_PIN_P8_0_SGPIO8         LPC_PIN_ID(8, 0, 4, 0, 4) /* not present on LQFP144 */
#define LPC_PIN_P8_0_T0_MAT0        LPC_PIN_ID(8, 0, 4, 0, 7) /* not present on LQFP144 */

#define LPC_PIN_P8_1_GPIO4_1        LPC_PIN_ID(8, 1, 4, 1, 0) /* not present on LQFP144 */
#define LPC_PIN_P8_1_USB0_IND1      LPC_PIN_ID(8, 1, 4, 1, 1) /* not present on LQFP144 */
#define LPC_PIN_P8_1_MCI1           LPC_PIN_ID(8, 1, 4, 1, 3) /* not present on LQFP144 */
#define LPC_PIN_P8_1_SGPIO9         LPC_PIN_ID(8, 1, 4, 1, 4) /* not present on LQFP144 */
#define LPC_PIN_P8_1_T0_MAT1        LPC_PIN_ID(8, 1, 4, 1, 7) /* not present on LQFP144 */

#define LPC_PIN_P8_2_GPIO4_2        LPC_PIN_ID(8, 2, 4, 2, 0) /* not present on LQFP144 */
#define LPC_PIN_P8_2_USB0_IND0      LPC_PIN_ID(8, 2, 4, 2, 1) /* not present on LQFP144 */
#define LPC_PIN_P8_2_MCI0           LPC_PIN_ID(8, 2, 4, 2, 3) /* not present on LQFP144 */
#define LPC_PIN_P8_2_SGPIO10        LPC_PIN_ID(8, 2, 4, 2, 4) /* not present on LQFP144 */
#define LPC_PIN_P8_2_T0_MAT2        LPC_PIN_ID(8, 2, 4, 2, 7) /* not present on LQFP144 */

#define LPC_PIN_P8_3_GPIO4_3        LPC_PIN_ID(8, 3, 4, 3, 0) /* not present on LQFP144 */
#define LPC_PIN_P8_3_USB1_ULPI_D2   LPC_PIN_ID(8, 3, 4, 3, 1) /* not present on LQFP144 */
#define LPC_PIN_P8_3_LCD_VD12       LPC_PIN_ID(8, 3, 4, 3, 3) /* not present on LQFP144 */
#define LPC_PIN_P8_3_LCD_VD19       LPC_PIN_ID(8, 3, 4, 3, 4) /* not present on LQFP144 */
#define LPC_PIN_P8_3_T0_MAT3        LPC_PIN_ID(8, 3, 4, 3, 7) /* not present on LQFP144 */

#define LPC_PIN_P8_4_GPIO4_4        LPC_PIN_ID(8, 4, 4, 4, 0) /* not present on LQFP144 */
#define LPC_PIN_P8_4_USB1_ULPI_D1   LPC_PIN_ID(8, 4, 4, 4, 1) /* not present on LQFP144 */
#define LPC_PIN_P8_4_LCD_VD7        LPC_PIN_ID(8, 4, 4, 4, 3) /* not present on LQFP144 */
#define LPC_PIN_P8_4_LCD_VD16       LPC_PIN_ID(8, 4, 4, 4, 4) /* not present on LQFP144 */
#define LPC_PIN_P8_4_T0_CAP0        LPC_PIN_ID(8, 4, 4, 4, 7) /* not present on LQFP144 */

#define LPC_PIN_P8_5_GPIO4_5        LPC_PIN_ID(8, 5, 4, 5, 0) /* not present on LQFP144 */
#define LPC_PIN_P8_5_USB1_ULPI_D0   LPC_PIN_ID(8, 5, 4, 5, 1) /* not present on LQFP144 */
#define LPC_PIN_P8_5_LCD_VD6        LPC_PIN_ID(8, 5, 4, 5, 3) /* not present on LQFP144 */
#define LPC_PIN_P8_5_LCD_VD8        LPC_PIN_ID(8, 5, 4, 5, 4) /* not present on LQFP144 */
#define LPC_PIN_P8_5_T0_CAP1        LPC_PIN_ID(8, 5, 4, 5, 7) /* not present on LQFP144 */

#define LPC_PIN_P8_6_GPIO4_6        LPC_PIN_ID(8, 6, 4, 6, 0) /* not present on LQFP144 */
#define LPC_PIN_P8_6_USB1_ULPI_NXT  LPC_PIN_ID(8, 6, 4, 6, 1) /* not present on LQFP144 */
#define LPC_PIN_P8_6_LCD_VD5        LPC_PIN_ID(8, 6, 4, 6, 3) /* not present on LQFP144 */
#define LPC_PIN_P8_6_LCD_LP         LPC_PIN_ID(8, 6, 4, 6, 4) /* not present on LQFP144 */
#define LPC_PIN_P8_6_T0_CAP2        LPC_PIN_ID(8, 6, 4, 6, 7) /* not present on LQFP144 */

#define LPC_PIN_P8_7_GPIO4_7        LPC_PIN_ID(8, 7, 4, 7, 0) /* not present on LQFP144 */
#define LPC_PIN_P8_7_USB1_ULPI_STP  LPC_PIN_ID(8, 7, 4, 7, 1) /* not present on LQFP144 */
#define LPC_PIN_P8_7_LCD_VD4        LPC_PIN_ID(8, 7, 4, 7, 3) /* not present on LQFP144 */
#define LPC_PIN_P8_7_LCD_PWR        LPC_PIN_ID(8, 7, 4, 7, 4) /* not present on LQFP144 */
#define LPC_PIN_P8_7_T0_CAP3        LPC_PIN_ID(8, 7, 4, 7, 7) /* not present on LQFP144 */

#define LPC_PIN_P8_8_USB1_ULPI_CLK  LPC_PIN_ID(8, 8, 8, 0, 1) /* not present on LQFP144 */
#define LPC_PIN_P8_8_CGU_OUT0       LPC_PIN_ID(8, 8, 8, 0, 6) /* not present on LQFP144 */
#define LPC_PIN_P8_8_I2S1_TX_MCLK   LPC_PIN_ID(8, 8, 8, 0, 7) /* not present on LQFP144 */

/* P9 */
#define LPC_PIN_P9_0_GPIO4_12       LPC_PIN_ID(9, 0, 4, 12, 0) /* not present on LQFP144 */
#define LPC_PIN_P9_0_MCABORT        LPC_PIN_ID(9, 0, 4, 12, 1) /* not present on LQFP144 */
#define LPC_PIN_P9_0_ENET_CRS       LPC_PIN_ID(9, 0, 4, 12, 5) /* not present on LQFP144 */
#define LPC_PIN_P9_0_SGPIO0         LPC_PIN_ID(9, 0, 4, 12, 6) /* not present on LQFP144 */
#define LPC_PIN_P9_0_SSP0_SSEL      LPC_PIN_ID(9, 0, 4, 12, 7) /* not present on LQFP144 */

#define LPC_PIN_P9_1_GPIO4_13       LPC_PIN_ID(9, 1, 4, 13, 0) /* not present on LQFP144 */
#define LPC_PIN_P9_1_MCOA2          LPC_PIN_ID(9, 1, 4, 13, 1) /* not present on LQFP144 */
#define LPC_PIN_P9_1_I2S0_TX_WS     LPC_PIN_ID(9, 1, 4, 13, 4) /* not present on LQFP144 */
#define LPC_PIN_P9_1_ENET_RX_ER     LPC_PIN_ID(9, 1, 4, 13, 5) /* not present on LQFP144 */
#define LPC_PIN_P9_1_SGPIO1         LPC_PIN_ID(9, 1, 4, 13, 6) /* not present on LQFP144 */
#define LPC_PIN_P9_1_SSP0_MISO      LPC_PIN_ID(9, 1, 4, 13, 7) /* not present on LQFP144 */

#define LPC_PIN_P9_2_GPIO4_14       LPC_PIN_ID(9, 2, 4, 14, 0) /* not present on LQFP144 */
#define LPC_PIN_P9_2_MCOB2          LPC_PIN_ID(9, 2, 4, 14, 1) /* not present on LQFP144 */
#define LPC_PIN_P9_2_I2S0_TX_SDA    LPC_PIN_ID(9, 2, 4, 14, 4) /* not present on LQFP144 */
#define LPC_PIN_P9_2_ENET_RXD3      LPC_PIN_ID(9, 2, 4, 14, 5) /* not present on LQFP144 */
#define LPC_PIN_P9_2_SGPIO2         LPC_PIN_ID(9, 2, 4, 14, 6) /* not present on LQFP144 */
#define LPC_PIN_P9_2_SSP0_MOSI      LPC_PIN_ID(9, 2, 4, 14, 7) /* not present on LQFP144 */

#define LPC_PIN_P9_3_GPIO4_15       LPC_PIN_ID(9, 3, 4, 15, 0) /* not present on LQFP144 */
#define LPC_PIN_P9_3_MCOA0          LPC_PIN_ID(9, 3, 4, 15, 1) /* not present on LQFP144 */
#define LPC_PIN_P9_3_USB1_IND1      LPC_PIN_ID(9, 3, 4, 15, 2) /* not present on LQFP144 */
#define LPC_PIN_P9_3_ENET_RXD2      LPC_PIN_ID(9, 3, 4, 15, 5) /* not present on LQFP144 */
#define LPC_PIN_P9_3_SGPIO9         LPC_PIN_ID(9, 3, 4, 15, 6) /* not present on LQFP144 */
#define LPC_PIN_P9_3_U3_TXD         LPC_PIN_ID(9, 3, 4, 15, 7) /* not present on LQFP144 */

#define LPC_PIN_P9_4_MCOB0          LPC_PIN_ID(9, 4, 5, 17, 1) /* not present on LQFP144 */
#define LPC_PIN_P9_4_USB1_IND0      LPC_PIN_ID(9, 4, 5, 17, 2) /* not present on LQFP144 */
#define LPC_PIN_P9_4_GPIO5_17       LPC_PIN_ID(9, 4, 5, 17, 4) /* not present on LQFP144 */
#define LPC_PIN_P9_4_ENET_TXD2      LPC_PIN_ID(9, 4, 5, 17, 5) /* not present on LQFP144 */
#define LPC_PIN_P9_4_SGPIO4         LPC_PIN_ID(9, 4, 5, 17, 6) /* not present on LQFP144 */
#define LPC_PIN_P9_4_U3_RXD         LPC_PIN_ID(9, 4, 5, 17, 7) /* not present on LQFP144 */

#define LPC_PIN_P9_5_MCOA1          LPC_PIN_ID(9, 5, 5, 18, 1)
#define LPC_PIN_P9_5_USB1_PPWR      LPC_PIN_ID(9, 5, 5, 18, 2)
#define LPC_PIN_P9_5_GPIO5_18       LPC_PIN_ID(9, 5, 5, 18, 4)
#define LPC_PIN_P9_5_ENET_TXD3      LPC_PIN_ID(9, 5, 5, 18, 5)
#define LPC_PIN_P9_5_SGPIO3         LPC_PIN_ID(9, 5, 5, 18, 6)
#define LPC_PIN_P9_5_U0_TXD         LPC_PIN_ID(9, 5, 5, 18, 7)

#define LPC_PIN_P9_6_GPIO4_11       LPC_PIN_ID(9, 6, 4, 11, 0)
#define LPC_PIN_P9_6_MCOB1          LPC_PIN_ID(9, 6, 4, 11, 1)
#define LPC_PIN_P9_6_USB1_PWR_FAULT LPC_PIN_ID(9, 6, 4, 11, 2)
#define LPC_PIN_P9_6_ENET_COL       LPC_PIN_ID(9, 6, 4, 11, 5)
#define LPC_PIN_P9_6_SGPIO8         LPC_PIN_ID(9, 6, 4, 11, 6)
#define LPC_PIN_P9_6_U0_RXD         LPC_PIN_ID(9, 6, 4, 11, 7)

/* PA */
#define LPC_PIN_PA_0_I2S1_RX_MCLK   LPC_PIN_ID(10, 0, 8, 0, 5) /* not present on LQFP144 */
#define LPC_PIN_PA_0_CGU_OUT1       LPC_PIN_ID(10, 0, 8, 0, 6) /* not present on LQFP144 */

#define LPC_PIN_PA_1_GPIO4_8        LPC_PIN_ID(10, 1, 4, 8, 0) /* not present on LQFP144 */
#define LPC_PIN_PA_1_QEI_IDX        LPC_PIN_ID(10, 1, 4, 8, 1) /* not present on LQFP144 */
#define LPC_PIN_PA_1_U2_TXD         LPC_PIN_ID(10, 1, 4, 8, 3) /* not present on LQFP144 */

#define LPC_PIN_PA_2_GPIO4_9        LPC_PIN_ID(10, 2, 4, 9, 0) /* not present on LQFP144 */
#define LPC_PIN_PA_2_QEI_PHB        LPC_PIN_ID(10, 2, 4, 9, 1) /* not present on LQFP144 */
#define LPC_PIN_PA_2_U2_RXD         LPC_PIN_ID(10, 2, 4, 9, 3) /* not present on LQFP144 */

#define LPC_PIN_PA_3_GPIO4_10       LPC_PIN_ID(10, 3, 4, 10, 0) /* not present on LQFP144 */
#define LPC_PIN_PA_3_QEI_PHA        LPC_PIN_ID(10, 3, 4, 10, 1) /* not present on LQFP144 */

#define LPC_PIN_PA_4_CTOUT_9        LPC_PIN_ID(10, 4, 5, 19, 1) /* not present on LQFP144 */
#define LPC_PIN_PA_4_EMC_A23        LPC_PIN_ID(10, 4, 5, 19, 3) /* not present on LQFP144 */
#define LPC_PIN_PA_4_GPIO5_19       LPC_PIN_ID(10, 4, 5, 19, 4) /* not present on LQFP144 */

/* PB */
#define LPC_PIN_PB_0_CTOUT_10       LPC_PIN_ID(11, 0, 5, 20, 1) /* not present on LQFP144 */
#define LPC_PIN_PB_0_LCD_VD23       LPC_PIN_ID(11, 0, 5, 20, 2) /* not present on LQFP144 */
#define LPC_PIN_PB_0_GPIO5_20       LPC_PIN_ID(11, 0, 5, 20, 4) /* not present on LQFP144 */

#define LPC_PIN_PB_1_USB1_ULPI_DIR  LPC_PIN_ID(11, 1, 5, 21, 1) /* not present on LQFP144 */
#define LPC_PIN_PB_1_LCD_VD22       LPC_PIN_ID(11, 1, 5, 21, 2) /* not present on LQFP144 */
#define LPC_PIN_PB_1_GPIO5_21       LPC_PIN_ID(11, 1, 5, 21, 4) /* not present on LQFP144 */
#define LPC_PIN_PB_1_CTOUT_6        LPC_PIN_ID(11, 1, 5, 21, 5) /* not present on LQFP144 */

#define LPC_PIN_PB_2_USB1_ULPI_D7   LPC_PIN_ID(11, 2, 5, 22, 1) /* not present on LQFP144 */
#define LPC_PIN_PB_2_LCD_VD21       LPC_PIN_ID(11, 2, 5, 22, 2) /* not present on LQFP144 */
#define LPC_PIN_PB_2_GPIO5_22       LPC_PIN_ID(11, 2, 5, 22, 4) /* not present on LQFP144 */
#define LPC_PIN_PB_2_CTOUT_7        LPC_PIN_ID(11, 2, 5, 22, 5) /* not present on LQFP144 */

#define LPC_PIN_PB_3_USB1_ULPI_D6   LPC_PIN_ID(11, 3, 5, 23, 1) /* not present on LQFP144 */
#define LPC_PIN_PB_3_LCD_VD20       LPC_PIN_ID(11, 3, 5, 23, 2) /* not present on LQFP144 */
#define LPC_PIN_PB_3_GPIO5_23       LPC_PIN_ID(11, 3, 5, 23, 4) /* not present on LQFP144 */
#define LPC_PIN_PB_3_CTOUT_8        LPC_PIN_ID(11, 3, 5, 23, 5) /* not present on LQFP144 */

#define LPC_PIN_PB_4_USB1_ULPI_D5   LPC_PIN_ID(11, 4, 5, 24, 1) /* not present on LQFP144 */
#define LPC_PIN_PB_4_LCD_VD15       LPC_PIN_ID(11, 4, 5, 24, 2) /* not present on LQFP144 */
#define LPC_PIN_PB_4_GPIO5_24       LPC_PIN_ID(11, 4, 5, 24, 4) /* not present on LQFP144 */
#define LPC_PIN_PB_4_CTIN_5         LPC_PIN_ID(11, 4, 5, 24, 5) /* not present on LQFP144 */

#define LPC_PIN_PB_5_USB1_ULPI_D4   LPC_PIN_ID(11, 5, 5, 25, 1) /* not present on LQFP144 */
#define LPC_PIN_PB_5_LCD_VD14       LPC_PIN_ID(11, 5, 5, 25, 2) /* not present on LQFP144 */
#define LPC_PIN_PB_5_GPIO5_25       LPC_PIN_ID(11, 5, 5, 25, 4) /* not present on LQFP144 */
#define LPC_PIN_PB_5_CTIN_7         LPC_PIN_ID(11, 5, 5, 25, 5) /* not present on LQFP144 */
#define LPC_PIN_PB_5_LCD_PWR        LPC_PIN_ID(11, 5, 5, 25, 6) /* not present on LQFP144 */

#define LPC_PIN_PB_6_USB1_ULPI_D3   LPC_PIN_ID(11, 6, 5, 26, 1 ) /* not present on LQFP144 */
#define LPC_PIN_PB_6_LCD_VD13       LPC_PIN_ID(11, 6, 5, 26, 2 ) /* not present on LQFP144 */
#define LPC_PIN_PB_6_GPIO5_26       LPC_PIN_ID(11, 6, 5, 26, 4 ) /* not present on LQFP144 */
#define LPC_PIN_PB_6_CTIN_6         LPC_PIN_ID(11, 6, 5, 26, 5 ) /* not present on LQFP144 */
#define LPC_PIN_PB_6_LCD_VD19       LPC_PIN_ID(11, 6, 5, 26, 6 ) /* not present on LQFP144 */

/* PC */
#define LPC_PIN_PC_0_USB1_ULPI_CLK  LPC_PIN_ID(12, 0, 8, 0, 1) /* not present on LQFP144 */
#define LPC_PIN_PC_0_ENET_RX_CLK    LPC_PIN_ID(12, 0, 8, 0, 3) /* not present on LQFP144 */
#define LPC_PIN_PC_0_LCD_DCLK       LPC_PIN_ID(12, 0, 8, 0, 4) /* not present on LQFP144 */
#define LPC_PIN_PC_0_SD_CLK         LPC_PIN_ID(12, 0, 8, 0, 7) /* not present on LQFP144 */

#define LPC_PIN_PC_1_USB1_ULPI_D7   LPC_PIN_ID(12, 1, 6, 0, 0) /* not present on LQFP144 */
#define LPC_PIN_PC_1_U1_RI          LPC_PIN_ID(12, 1, 6, 0, 2) /* not present on LQFP144 */
#define LPC_PIN_PC_1_ENET_MDC       LPC_PIN_ID(12, 1, 6, 0, 3) /* not present on LQFP144 */
#define LPC_PIN_PC_1_GPIO6_0        LPC_PIN_ID(12, 1, 6, 0, 4) /* not present on LQFP144 */
#define LPC_PIN_PC_1_T3_CAP0        LPC_PIN_ID(12, 1, 6, 0, 6) /* not present on LQFP144 */
#define LPC_PIN_PC_1_SD_VOLT0       LPC_PIN_ID(12, 1, 6, 0, 7) /* not present on LQFP144 */

#define LPC_PIN_PC_2_USB1_ULPI_D6   LPC_PIN_ID(12, 2, 6, 1, 0) /* not present on LQFP144 */
#define LPC_PIN_PC_2_U1_CTS         LPC_PIN_ID(12, 2, 6, 1, 2) /* not present on LQFP144 */
#define LPC_PIN_PC_2_ENET_TXD2      LPC_PIN_ID(12, 2, 6, 1, 3) /* not present on LQFP144 */
#define LPC_PIN_PC_2_GPIO6_1        LPC_PIN_ID(12, 2, 6, 1, 4) /* not present on LQFP144 */
#define LPC_PIN_PC_2_SD_RST         LPC_PIN_ID(12, 2, 6, 1, 7) /* not present on LQFP144 */

#define LPC_PIN_PC_3_USB1_ULPI_D5   LPC_PIN_ID(12, 3, 6, 2, 0) /* not present on LQFP144 */
#define LPC_PIN_PC_3_U1_RTS         LPC_PIN_ID(12, 3, 6, 2, 2) /* not present on LQFP144 */
#define LPC_PIN_PC_3_ENET_TXD3      LPC_PIN_ID(12, 3, 6, 2, 3) /* not present on LQFP144 */
#define LPC_PIN_PC_3_GPIO6_2        LPC_PIN_ID(12, 3, 6, 2, 4) /* not present on LQFP144 */
#define LPC_PIN_PC_3_SD_VOLT1       LPC_PIN_ID(12, 3, 6, 2, 7) /* not present on LQFP144 */

#define LPC_PIN_PC_4_USB1_ULPI_D4   LPC_PIN_ID(12, 4, 6, 3, 1) /* not present on LQFP144 */
#define LPC_PIN_PC_4_ENET_TX_EN     LPC_PIN_ID(12, 4, 6, 3, 3) /* not present on LQFP144 */
#define LPC_PIN_PC_4_GPIO6_3        LPC_PIN_ID(12, 4, 6, 3, 4) /* not present on LQFP144 */
#define LPC_PIN_PC_4_T3_CAP1        LPC_PIN_ID(12, 4, 6, 3, 6) /* not present on LQFP144 */
#define LPC_PIN_PC_4_SD_DAT0        LPC_PIN_ID(12, 4, 6, 3, 7) /* not present on LQFP144 */

#define LPC_PIN_PC_5_USB1_ULPI_D3   LPC_PIN_ID(12, 5, 6, 4, 1) /* not present on LQFP144 */
#define LPC_PIN_PC_5_ENET_TX_ER     LPC_PIN_ID(12, 5, 6, 4, 3) /* not present on LQFP144 */
#define LPC_PIN_PC_5_GPIO6_4        LPC_PIN_ID(12, 5, 6, 4, 4) /* not present on LQFP144 */
#define LPC_PIN_PC_5_T3_CAP2        LPC_PIN_ID(12, 5, 6, 4, 6) /* not present on LQFP144 */
#define LPC_PIN_PC_5_SD_DAT1        LPC_PIN_ID(12, 5, 6, 4, 7) /* not present on LQFP144 */

#define LPC_PIN_PC_6_USB1_ULPI_D2   LPC_PIN_ID(12, 6, 6, 5, 1) /* not present on LQFP144 */
#define LPC_PIN_PC_6_ENET_RXD2      LPC_PIN_ID(12, 6, 6, 5, 3) /* not present on LQFP144 */
#define LPC_PIN_PC_6_GPIO6_5        LPC_PIN_ID(12, 6, 6, 5, 4) /* not present on LQFP144 */
#define LPC_PIN_PC_6_T3_CAP3        LPC_PIN_ID(12, 6, 6, 5, 6) /* not present on LQFP144 */
#define LPC_PIN_PC_6_SD_DAT2        LPC_PIN_ID(12, 6, 6, 5, 7) /* not present on LQFP144 */

#define LPC_PIN_PC_7_USB1_ULPI_D1   LPC_PIN_ID(12, 7, 6, 6, 1) /* not present on LQFP144 */
#define LPC_PIN_PC_7_ENET_RXD3      LPC_PIN_ID(12, 7, 6, 6, 3) /* not present on LQFP144 */
#define LPC_PIN_PC_7_GPIO6_6        LPC_PIN_ID(12, 7, 6, 6, 4) /* not present on LQFP144 */
#define LPC_PIN_PC_7_T3_MAT0        LPC_PIN_ID(12, 7, 6, 6, 6) /* not present on LQFP144 */
#define LPC_PIN_PC_7_SD_DAT3        LPC_PIN_ID(12, 7, 6, 6, 7) /* not present on LQFP144 */

#define LPC_PIN_PC_8_USB1_ULPI_D0   LPC_PIN_ID(12, 8, 6, 7, 1) /* not present on LQFP144 */
#define LPC_PIN_PC_8_ENET_RX_DV     LPC_PIN_ID(12, 8, 6, 7, 3) /* not present on LQFP144 */
#define LPC_PIN_PC_8_GPIO6_7        LPC_PIN_ID(12, 8, 6, 7, 4) /* not present on LQFP144 */
#define LPC_PIN_PC_8_T3_MAT1        LPC_PIN_ID(12, 8, 6, 7, 6) /* not present on LQFP144 */
#define LPC_PIN_PC_8_SD_CD          LPC_PIN_ID(12, 8, 6, 7, 7) /* not present on LQFP144 */

#define LPC_PIN_PC_9_USB1_ULPI_NXT  LPC_PIN_ID(12, 9, 6, 8, 1) /* not present on LQFP144 */
#define LPC_PIN_PC_9_ENET_RX_ER     LPC_PIN_ID(12, 9, 6, 8, 3) /* not present on LQFP144 */
#define LPC_PIN_PC_9_GPIO6_8        LPC_PIN_ID(12, 9, 6, 8, 4) /* not present on LQFP144 */
#define LPC_PIN_PC_9_T3_MAT2        LPC_PIN_ID(12, 9, 6, 8, 6) /* not present on LQFP144 */
#define LPC_PIN_PC_9_SD_POW         LPC_PIN_ID(12, 9, 6, 8, 7) /* not present on LQFP144 */

#define LPC_PIN_PC_10_USB1_ULPI_STP LPC_PIN_ID(12, 10, 6, 9, 1) /* not present on LQFP144 */
#define LPC_PIN_PC_10_U1_DSR        LPC_PIN_ID(12, 10, 6, 9, 2) /* not present on LQFP144 */
#define LPC_PIN_PC_10_GPIO6_9       LPC_PIN_ID(12, 10, 6, 9, 4) /* not present on LQFP144 */
#define LPC_PIN_PC_10_T3_MAT3       LPC_PIN_ID(12, 10, 6, 9, 6) /* not present on LQFP144 */
#define LPC_PIN_PC_10_SD_CMD        LPC_PIN_ID(12, 10, 6, 9, 7) /* not present on LQFP144 */

#define LPC_PIN_PC_11_USB1_ULPI_DIR LPC_PIN_ID(12, 11, 6, 10, 1) /* not present on LQFP144 */
#define LPC_PIN_PC_11_U1_DCD        LPC_PIN_ID(12, 11, 6, 10, 2) /* not present on LQFP144 */
#define LPC_PIN_PC_11_GPIO6_10      LPC_PIN_ID(12, 11, 6, 10, 4) /* not present on LQFP144 */
#define LPC_PIN_PC_11_SD_DAT4       LPC_PIN_ID(12, 11, 6, 10, 7) /* not present on LQFP144 */

#define LPC_PIN_PC_12_U1_DTR        LPC_PIN_ID(12, 12, 6, 11, 2) /* not present on LQFP144 */
#define LPC_PIN_PC_12_GPIO6_11      LPC_PIN_ID(12, 12, 6, 11, 4) /* not present on LQFP144 */
#define LPC_PIN_PC_12_SGPIO11       LPC_PIN_ID(12, 12, 6, 11, 5) /* not present on LQFP144 */
#define LPC_PIN_PC_12_I2S0_TX_SDA   LPC_PIN_ID(12, 12, 6, 11, 6) /* not present on LQFP144 */
#define LPC_PIN_PC_12_SD_DAT5       LPC_PIN_ID(12, 12, 6, 11, 7) /* not present on LQFP144 */

#define LPC_PIN_PC_13_U1_TXD        LPC_PIN_ID(12, 13, 6, 12, 2) /* not present on LQFP144 */
#define LPC_PIN_PC_13_GPIO6_12      LPC_PIN_ID(12, 13, 6, 12, 4) /* not present on LQFP144 */
#define LPC_PIN_PC_13_SGPIO12       LPC_PIN_ID(12, 13, 6, 12, 5) /* not present on LQFP144 */
#define LPC_PIN_PC_13_I2S0_TX_WS    LPC_PIN_ID(12, 13, 6, 12, 6) /* not present on LQFP144 */
#define LPC_PIN_PC_13_SD_DAT6       LPC_PIN_ID(12, 13, 6, 12, 7) /* not present on LQFP144 */

#define LPC_PIN_PC_14_U1_RXD        LPC_PIN_ID(12, 14, 6, 13, 2) /* not present on LQFP144 */
#define LPC_PIN_PC_14_GPIO6_13      LPC_PIN_ID(12, 14, 6, 13, 4) /* not present on LQFP144 */
#define LPC_PIN_PC_14SGPIO13_       LPC_PIN_ID(12, 14, 6, 13, 5) /* not present on LQFP144 */
#define LPC_PIN_PC_14_ENET_TX_ER    LPC_PIN_ID(12, 14, 6, 13, 6) /* not present on LQFP144 */
#define LPC_PIN_PC_14_SD_DAT7       LPC_PIN_ID(12, 14, 6, 13, 7) /* not present on LQFP144 */

/* PD */
#define LPC_PIN_PD_0_CTOUT_15       LPC_PIN_ID(13, 0, 6, 14, 1) /* not present on LQFP144 */
#define LPC_PIN_PD_0_EMC_DQMOUT2    LPC_PIN_ID(13, 0, 6, 14, 2) /* not present on LQFP144 */
#define LPC_PIN_PD_0_GPIO6_14       LPC_PIN_ID(13, 0, 6, 14, 4) /* not present on LQFP144 */
#define LPC_PIN_PD_0_SGPIO4         LPC_PIN_ID(13, 0, 6, 14, 7) /* not present on LQFP144 */

#define LPC_PIN_PD_1_EMC_CKEOUT2    LPC_PIN_ID(13, 1, 6, 15, 2) /* not present on LQFP144 */
#define LPC_PIN_PD_1_GPIO6_15       LPC_PIN_ID(13, 1, 6, 15, 4) /* not present on LQFP144 */
#define LPC_PIN_PD_1_SD_POW         LPC_PIN_ID(13, 1, 6, 15, 5) /* not present on LQFP144 */
#define LPC_PIN_PD_1_SGPIO5         LPC_PIN_ID(13, 1, 6, 15, 7) /* not present on LQFP144 */

#define LPC_PIN_PD_2_CTOUT_7        LPC_PIN_ID(13, 2, 6, 16, 1) /* not present on LQFP144 */
#define LPC_PIN_PD_2_EMC_D16        LPC_PIN_ID(13, 2, 6, 16, 2) /* not present on LQFP144 */
#define LPC_PIN_PD_2_GPIO6_16       LPC_PIN_ID(13, 2, 6, 16, 4) /* not present on LQFP144 */
#define LPC_PIN_PD_2_SGPIO6         LPC_PIN_ID(13, 2, 6, 16, 7) /* not present on LQFP144 */

#define LPC_PIN_PD_3_CTOUT_6        LPC_PIN_ID(13, 3, 6, 17, 1) /* not present on LQFP144 */
#define LPC_PIN_PD_3_EMC_D17        LPC_PIN_ID(13, 3, 6, 17, 2) /* not present on LQFP144 */
#define LPC_PIN_PD_3_GPIO6_17       LPC_PIN_ID(13, 3, 6, 17, 4) /* not present on LQFP144 */
#define LPC_PIN_PD_3_SGPIO7         LPC_PIN_ID(13, 3, 6, 17, 7) /* not present on LQFP144 */

#define LPC_PIN_PD_4_CTOUT_8        LPC_PIN_ID(13, 4, 6, 18, 1) /* not present on LQFP144 */
#define LPC_PIN_PD_4_EMC_D18        LPC_PIN_ID(13, 4, 6, 18, 2) /* not present on LQFP144 */
#define LPC_PIN_PD_4_GPIO6_18       LPC_PIN_ID(13, 4, 6, 18, 4) /* not present on LQFP144 */
#define LPC_PIN_PD_4_SGPIO8         LPC_PIN_ID(13, 4, 6, 18, 7) /* not present on LQFP144 */

#define LPC_PIN_PD_5_CTOUT_9        LPC_PIN_ID(13, 5, 6, 19, 1) /* not present on LQFP144 */
#define LPC_PIN_PD_5_EMC_D19        LPC_PIN_ID(13, 5, 6, 19, 2) /* not present on LQFP144 */
#define LPC_PIN_PD_5_GPIO6_19       LPC_PIN_ID(13, 5, 6, 19, 4) /* not present on LQFP144 */
#define LPC_PIN_PD_5_SGPIO9         LPC_PIN_ID(13, 5, 6, 19, 7) /* not present on LQFP144 */

#define LPC_PIN_PD_6_CTOUT_10       LPC_PIN_ID(13, 6, 6, 20, 1) /* not present on LQFP144 */
#define LPC_PIN_PD_6_EMC_D20        LPC_PIN_ID(13, 6, 6, 20, 2) /* not present on LQFP144 */
#define LPC_PIN_PD_6_GPIO6_20       LPC_PIN_ID(13, 6, 6, 20, 4) /* not present on LQFP144 */
#define LPC_PIN_PD_6_SGPIO10        LPC_PIN_ID(13, 6, 6, 20, 7) /* not present on LQFP144 */

#define LPC_PIN_PD_7_CTIN_5         LPC_PIN_ID(13, 7, 6, 21, 1) /* not present on LQFP144 */
#define LPC_PIN_PD_7_EMC_D21        LPC_PIN_ID(13, 7, 6, 21, 2) /* not present on LQFP144 */
#define LPC_PIN_PD_7_GPIO6_21       LPC_PIN_ID(13, 7, 6, 21, 4) /* not present on LQFP144 */
#define LPC_PIN_PD_7_SGPIO11        LPC_PIN_ID(13, 7, 6, 21, 7) /* not present on LQFP144 */

#define LPC_PIN_PD_8_CTIN_6         LPC_PIN_ID(13, 8, 6, 22, 1) /* not present on LQFP144 */
#define LPC_PIN_PD_8_EMC_D22        LPC_PIN_ID(13, 8, 6, 22, 2) /* not present on LQFP144 */
#define LPC_PIN_PD_8_GPIO6_22       LPC_PIN_ID(13, 8, 6, 22, 4) /* not present on LQFP144 */
#define LPC_PIN_PD_8_SGPIO12        LPC_PIN_ID(13, 8, 6, 22, 7) /* not present on LQFP144 */

#define LPC_PIN_PD_9_CTOUT_13       LPC_PIN_ID(13, 9, 6, 23, 1) /* not present on LQFP144 */
#define LPC_PIN_PD_9_EMC_D23        LPC_PIN_ID(13, 9, 6, 23, 2) /* not present on LQFP144 */
#define LPC_PIN_PD_9_GPIO6_23       LPC_PIN_ID(13, 9, 6, 23, 4) /* not present on LQFP144 */
#define LPC_PIN_PD_9_SGPIO13        LPC_PIN_ID(13, 9, 6, 23, 7) /* not present on LQFP144 */

#define LPC_PIN_PD_10_CTIN_1        LPC_PIN_ID(13, 10, 6, 24, 1) /* not present on LQFP144 */
#define LPC_PIN_PD_10_EMC_BLS3      LPC_PIN_ID(13, 10, 6, 24, 2) /* not present on LQFP144 */
#define LPC_PIN_PD_10_GPIO6_24      LPC_PIN_ID(13, 10, 6, 24, 4) /* not present on LQFP144 */

#define LPC_PIN_PD_11_EMC_CS3       LPC_PIN_ID(13, 11, 6, 25, 2) /* not present on LQFP144 */
#define LPC_PIN_PD_11_GPIO6_25      LPC_PIN_ID(13, 11, 6, 25, 4) /* not present on LQFP144 */
#define LPC_PIN_PD_11_USB1_ULPI_D0  LPC_PIN_ID(13, 11, 6, 25, 5) /* not present on LQFP144 */
#define LPC_PIN_PD_11_CTOUT_14      LPC_PIN_ID(13, 11, 6, 25, 6) /* not present on LQFP144 */

#define LPC_PIN_PD_12_EMC_CS2       LPC_PIN_ID(13, 12, 6, 26, 2) /* not present on LQFP144 */
#define LPC_PIN_PD_12_GPIO6_26      LPC_PIN_ID(13, 12, 6, 26, 4) /* not present on LQFP144 */
#define LPC_PIN_PD_12_CTOUT_10      LPC_PIN_ID(13, 12, 6, 26, 6) /* not present on LQFP144 */

#define LPC_PIN_PD_13_CTIN_0        LPC_PIN_ID(13, 13, 6, 27, 1) /* not present on LQFP144 */
#define LPC_PIN_PD_13_EMC_BLS2      LPC_PIN_ID(13, 13, 6, 27, 2) /* not present on LQFP144 */
#define LPC_PIN_PD_13_GPIO6_27      LPC_PIN_ID(13, 13, 6, 27, 4) /* not present on LQFP144 */
#define LPC_PIN_PD_13_CTOUT_13      LPC_PIN_ID(13, 13, 6, 27, 6) /* not present on LQFP144 */

#define LPC_PIN_PD_14_EMC_DYCS2     LPC_PIN_ID(13, 14, 6, 28, 2) /* not present on LQFP144 */
#define LPC_PIN_PD_14_GPIO6_28      LPC_PIN_ID(13, 14, 6, 28, 4) /* not present on LQFP144 */
#define LPC_PIN_PD_14_CTOUT_11      LPC_PIN_ID(13, 14, 6, 28, 6) /* not present on LQFP144 */

#define LPC_PIN_PD_15_EMC_A17       LPC_PIN_ID(13, 15, 6, 29, 2) /* not present on LQFP144 */
#define LPC_PIN_PD_15_GPIO6_29      LPC_PIN_ID(13, 15, 6, 29, 4) /* not present on LQFP144 */
#define LPC_PIN_PD_15_SD_WP         LPC_PIN_ID(13, 15, 6, 29, 5) /* not present on LQFP144 */
#define LPC_PIN_PD_15_CTOUT_8       LPC_PIN_ID(13, 15, 6, 29, 6) /* not present on LQFP144 */

#define LPC_PIN_PD_16_EMC_A16       LPC_PIN_ID(13, 16, 6, 30, 2) /* not present on LQFP144 */
#define LPC_PIN_PD_16_GPIO6_30      LPC_PIN_ID(13, 16, 6, 30, 4) /* not present on LQFP144 */
#define LPC_PIN_PD_16_SD_VOLT2      LPC_PIN_ID(13, 16, 6, 30, 5) /* not present on LQFP144 */
#define LPC_PIN_PD_16_CTOUT_12      LPC_PIN_ID(13, 16, 6, 30, 6) /* not present on LQFP144 */

/* PE */
#define LPC_PIN_PE_0_EMC_A18        LPC_PIN_ID(14, 1, 7, 0, 3) /* not present on LQFP144 */
#define LPC_PIN_PE_0_GPIO7_0        LPC_PIN_ID(14, 1, 7, 0, 4) /* not present on LQFP144 */
#define LPC_PIN_PE_0_CAN1_TD        LPC_PIN_ID(14, 1, 7, 0, 5) /* not present on LQFP144 */

#define LPC_PIN_PE_1_EMC_A19        LPC_PIN_ID(14, 1, 7, 1, 3) /* not present on LQFP144 */
#define LPC_PIN_PE_1_GPIO7_1        LPC_PIN_ID(14, 1, 7, 1, 4) /* not present on LQFP144 */
#define LPC_PIN_PE_1_CAN1_RD        LPC_PIN_ID(14, 1, 7, 1, 5) /* not present on LQFP144 */

#define LPC_PIN_PE_2_ADCTRIG0       LPC_PIN_ID(14, 2, 7, 2, 0) /* not present on LQFP144 */
#define LPC_PIN_PE_2_CAN0_RD        LPC_PIN_ID(14, 2, 7, 2, 1) /* not present on LQFP144 */
#define LPC_PIN_PE_2_EMC_A20        LPC_PIN_ID(14, 2, 7, 2, 3) /* not present on LQFP144 */
#define LPC_PIN_PE_2_GPIO7_2        LPC_PIN_ID(14, 2, 7, 2, 4) /* not present on LQFP144 */

#define LPC_PIN_PE_3_CAN0_TD        LPC_PIN_ID(14, 3, 7, 3, 1) /* not present on LQFP144 */
#define LPC_PIN_PE_3_ADCTRIG1       LPC_PIN_ID(14, 3, 7, 3, 2) /* not present on LQFP144 */
#define LPC_PIN_PE_3_EMC_A21        LPC_PIN_ID(14, 3, 7, 3, 3) /* not present on LQFP144 */
#define LPC_PIN_PE_3_GPIO7_3        LPC_PIN_ID(14, 3, 7, 3, 4) /* not present on LQFP144 */

#define LPC_PIN_PE_4_NMI            LPC_PIN_ID(14, 4, 7, 4, 1) /* not present on LQFP144 */
#define LPC_PIN_PE_4_EMC_A22        LPC_PIN_ID(14, 4, 7, 4, 3) /* not present on LQFP144 */
#define LPC_PIN_PE_4_GPIO7_4        LPC_PIN_ID(14, 4, 7, 4, 4) /* not present on LQFP144 */

#define LPC_PIN_PE_5_CTOUT_3        LPC_PIN_ID(14, 5, 7, 5, 1) /* not present on LQFP144 */
#define LPC_PIN_PE_5_U1_RTS         LPC_PIN_ID(14, 5, 7, 5, 2) /* not present on LQFP144 */
#define LPC_PIN_PE_5_EMC_D24        LPC_PIN_ID(14, 5, 7, 5, 3) /* not present on LQFP144 */
#define LPC_PIN_PE_5_GPIO7_5        LPC_PIN_ID(14, 5, 7, 5, 4) /* not present on LQFP144 */

#define LPC_PIN_PE_6_CTOUT_2        LPC_PIN_ID(14, 6, 7, 6, 1) /* not present on LQFP144 */
#define LPC_PIN_PE_6_U1_RI          LPC_PIN_ID(14, 6, 7, 6, 2) /* not present on LQFP144 */
#define LPC_PIN_PE_6_EMC_D25        LPC_PIN_ID(14, 6, 7, 6, 3) /* not present on LQFP144 */
#define LPC_PIN_PE_6_GPIO7_6        LPC_PIN_ID(14, 6, 7, 6, 4) /* not present on LQFP144 */

#define LPC_PIN_PE_7_CTOUT_5        LPC_PIN_ID(14, 7, 7, 7, 1) /* not present on LQFP144 */
#define LPC_PIN_PE_7_U1_CTS         LPC_PIN_ID(14, 7, 7, 7, 2) /* not present on LQFP144 */
#define LPC_PIN_PE_7_EMC_D26        LPC_PIN_ID(14, 7, 7, 7, 3) /* not present on LQFP144 */
#define LPC_PIN_PE_7_GPIO7_7        LPC_PIN_ID(14, 7, 7, 7, 4) /* not present on LQFP144 */

#define LPC_PIN_PE_8_CTOUT_4        LPC_PIN_ID(14, 8, 7, 8, 1) /* not present on LQFP144 */
#define LPC_PIN_PE_8_U1_DSR         LPC_PIN_ID(14, 8, 7, 8, 2) /* not present on LQFP144 */
#define LPC_PIN_PE_8_EMC_D27        LPC_PIN_ID(14, 8, 7, 8, 3) /* not present on LQFP144 */
#define LPC_PIN_PE_8_GPIO7_8        LPC_PIN_ID(14, 8, 7, 8, 4) /* not present on LQFP144 */

#define LPC_PIN_PE_9_CTIN_4         LPC_PIN_ID(14, 9, 7, 9, 1) /* not present on LQFP144 */
#define LPC_PIN_PE_9_U1_DCD         LPC_PIN_ID(14, 9, 7, 9, 2) /* not present on LQFP144 */
#define LPC_PIN_PE_9_EMC_D28        LPC_PIN_ID(14, 9, 7, 9, 3) /* not present on LQFP144 */
#define LPC_PIN_PE_9_GPIO7_9        LPC_PIN_ID(14, 9, 7, 9, 4) /* not present on LQFP144 */

#define LPC_PIN_PE_10_CTIN_3        LPC_PIN_ID(14, 10, 7, 10, 1) /* not present on LQFP144 */
#define LPC_PIN_PE_10_U1_DTR        LPC_PIN_ID(14, 10, 7, 10, 2) /* not present on LQFP144 */
#define LPC_PIN_PE_10_EMC_D29       LPC_PIN_ID(14, 10, 7, 10, 3) /* not present on LQFP144 */
#define LPC_PIN_PE_10_GPIO7_10      LPC_PIN_ID(14, 10, 7, 10, 4) /* not present on LQFP144 */

#define LPC_PIN_PE_11_CTOUT_12      LPC_PIN_ID(14, 11, 7, 11, 1) /* not present on LQFP144 */
#define LPC_PIN_PE_11_U1_TXD        LPC_PIN_ID(14, 11, 7, 11, 2) /* not present on LQFP144 */
#define LPC_PIN_PE_11_EMC_D30       LPC_PIN_ID(14, 11, 7, 11, 3) /* not present on LQFP144 */
#define LPC_PIN_PE_11_GPIO7_11      LPC_PIN_ID(14, 11, 7, 11, 4) /* not present on LQFP144 */

#define LPC_PIN_PE_12_CTOUT_11      LPC_PIN_ID(14, 12, 7, 12, 1) /* not present on LQFP144 */
#define LPC_PIN_PE_12_U1_RXD        LPC_PIN_ID(14, 12, 7, 12, 2) /* not present on LQFP144 */
#define LPC_PIN_PE_12_EMC_D31       LPC_PIN_ID(14, 12, 7, 12, 3) /* not present on LQFP144 */
#define LPC_PIN_PE_12_GPIO7_12      LPC_PIN_ID(14, 12, 7, 12, 4) /* not present on LQFP144 */

#define LPC_PIN_PE_13_CTOUT_14      LPC_PIN_ID(14, 13, 7, 13, 1) /* not present on LQFP144 */
#define LPC_PIN_PE_13_I2C1_SDA      LPC_PIN_ID(14, 13, 7, 13, 2) /* not present on LQFP144 */
#define LPC_PIN_PE_13_EMC_DQMOUT3   LPC_PIN_ID(14, 13, 7, 13, 3) /* not present on LQFP144 */
#define LPC_PIN_PE_13_GPIO7_13      LPC_PIN_ID(14, 13, 7, 13, 4) /* not present on LQFP144 */

#define LPC_PIN_PE_14_EMC_DYCS3     LPC_PIN_ID(14, 14, 7, 14, 3) /* not present on LQFP144 */
#define LPC_PIN_PE_14_GPIO7_14      LPC_PIN_ID(14, 14, 7, 14, 4) /* not present on LQFP144 */

#define LPC_PIN_PE_15_CTOUT_0       LPC_PIN_ID(14, 15, 7, 15, 1) /* not present on LQFP144 */
#define LPC_PIN_PE_15_I2C1_SCL      LPC_PIN_ID(14, 15, 7, 15, 2) /* not present on LQFP144 */
#define LPC_PIN_PE_15_EMC_CKEOUT3   LPC_PIN_ID(14, 15, 7, 15, 3) /* not present on LQFP144 */
#define LPC_PIN_PE_15_GPIO7_15      LPC_PIN_ID(14, 15, 7, 15, 4) /* not present on LQFP144 */

/* PF */
#define LPC_PIN_PF_0_SSP0_SCK       LPC_PIN_ID(15, 0, 8, 0, 0) /* not present on LQFP144 */
#define LPC_PIN_PF_0_GP_CLKIN       LPC_PIN_ID(15, 0, 8, 0, 1) /* not present on LQFP144 */
#define LPC_PIN_PF_0_I2S1_TX_MCLK   LPC_PIN_ID(15, 0, 8, 0, 7) /* not present on LQFP144 */

#define LPC_PIN_PF_1_SSP0_SSEL      LPC_PIN_ID(15, 1, 7, 16, 2) /* not present on LQFP144 */
#define LPC_PIN_PF_1_GPIO7_16       LPC_PIN_ID(15, 1, 7, 16, 4) /* not present on LQFP144 */
#define LPC_PIN_PF_1_SGPIO0         LPC_PIN_ID(15, 1, 7, 16, 6) /* not present on LQFP144 */

#define LPC_PIN_PF_2_U3_TXD         LPC_PIN_ID(15, 2, 7, 17, 1) /* not present on LQFP144 */
#define LPC_PIN_PF_2_SSP0_MISO      LPC_PIN_ID(15, 2, 7, 17, 2) /* not present on LQFP144 */
#define LPC_PIN_PF_2_GPIO7_17       LPC_PIN_ID(15, 2, 7, 17, 4) /* not present on LQFP144 */
#define LPC_PIN_PF_2_SGPIO1         LPC_PIN_ID(15, 2, 7, 17, 6) /* not present on LQFP144 */

#define LPC_PIN_PF_3_U3_RXD         LPC_PIN_ID(15, 3, 7, 18, 1) /* not present on LQFP144 */
#define LPC_PIN_PF_3_SSP0_MOSI      LPC_PIN_ID(15, 3, 7, 18, 2) /* not present on LQFP144 */
#define LPC_PIN_PF_3_GPIO7_18       LPC_PIN_ID(15, 3, 7, 18, 4) /* not present on LQFP144 */
#define LPC_PIN_PF_3_SGPIO2         LPC_PIN_ID(15, 3, 7, 18, 6) /* not present on LQFP144 */

#define LPC_PIN_PF_4_SSP1_SCK       LPC_PIN_ID(15, 4, 8, 0, 0)
#define LPC_PIN_PF_4_GP_CLKIN       LPC_PIN_ID(15, 4, 8, 0, 1)
#define LPC_PIN_PF_4_TRACECLK       LPC_PIN_ID(15, 4, 8, 0, 2)
#define LPC_PIN_PF_4_I2S0_TX_MCLK   LPC_PIN_ID(15, 4, 8, 0, 6)
#define LPC_PIN_PF_4_I2S0_RX_SCK    LPC_PIN_ID(15, 4, 8, 0, 7)

#define LPC_PIN_PF_5_U3_UCLK        LPC_PIN_ID(15, 5, 7, 19, 1) /* not present on LQFP144 */
#define LPC_PIN_PF_5_SSP1_SSEL      LPC_PIN_ID(15, 5, 7, 19, 2) /* not present on LQFP144 */
#define LPC_PIN_PF_5_TRACEDATA0     LPC_PIN_ID(15, 5, 7, 19, 3) /* not present on LQFP144 */
#define LPC_PIN_PF_5_GPIO7_19       LPC_PIN_ID(15, 5, 7, 19, 4) /* not present on LQFP144 */
#define LPC_PIN_PF_5_SGPIO4         LPC_PIN_ID(15, 5, 7, 19, 6) /* not present on LQFP144 */

#define LPC_PIN_PF_6_U3_DIR         LPC_PIN_ID(15, 6, 7, 20, 1) /* not present on LQFP144 */
#define LPC_PIN_PF_6_SSP1_MISO      LPC_PIN_ID(15, 6, 7, 20, 2) /* not present on LQFP144 */
#define LPC_PIN_PF_6_TRACEDATA1     LPC_PIN_ID(15, 6, 7, 20, 3) /* not present on LQFP144 */
#define LPC_PIN_PF_6_GPIO7_20       LPC_PIN_ID(15, 6, 7, 20, 4) /* not present on LQFP144 */
#define LPC_PIN_PF_6_SGPIO5         LPC_PIN_ID(15, 6, 7, 20, 6) /* not present on LQFP144 */
#define LPC_PIN_PF_6_I2S1_TX_SDA    LPC_PIN_ID(15, 6, 7, 20, 7) /* not present on LQFP144 */

#define LPC_PIN_PF_7_U3_BAUD        LPC_PIN_ID(15, 7, 7, 21, 1) /* not present on LQFP144 */
#define LPC_PIN_PF_7_SSP1_MOSI      LPC_PIN_ID(15, 7, 7, 21, 2) /* not present on LQFP144 */
#define LPC_PIN_PF_7_TRACEDATA2     LPC_PIN_ID(15, 7, 7, 21, 3) /* not present on LQFP144 */
#define LPC_PIN_PF_7_GPIO7_21       LPC_PIN_ID(15, 7, 7, 21, 4) /* not present on LQFP144 */
#define LPC_PIN_PF_7_SGPIO6         LPC_PIN_ID(15, 7, 7, 21, 6) /* not present on LQFP144 */
#define LPC_PIN_PF_7_I2S1_TX_WS     LPC_PIN_ID(15, 7, 7, 21, 7) /* not present on LQFP144 */

#define LPC_PIN_PF_8_U0_UCLK        LPC_PIN_ID(15, 8, 7, 22, 1) /* not present on LQFP144 */
#define LPC_PIN_PF_8_CTIN_2         LPC_PIN_ID(15, 8, 7, 22, 2) /* not present on LQFP144 */
#define LPC_PIN_PF_8_TRACEDATA3     LPC_PIN_ID(15, 8, 7, 22, 3) /* not present on LQFP144 */
#define LPC_PIN_PF_8_GPIO7_22       LPC_PIN_ID(15, 8, 7, 22, 4) /* not present on LQFP144 */
#define LPC_PIN_PF_8_SGPIO7         LPC_PIN_ID(15, 8, 7, 22, 6) /* not present on LQFP144 */

#define LPC_PIN_PF_9_U0_DIR         LPC_PIN_ID(15, 9, 7, 23, 1) /* not present on LQFP144 */
#define LPC_PIN_PF_9_CTOUT_1        LPC_PIN_ID(15, 9, 7, 23, 2) /* not present on LQFP144 */
#define LPC_PIN_PF_9_GPIO7_23       LPC_PIN_ID(15, 9, 7, 23, 4) /* not present on LQFP144 */
#define LPC_PIN_PF_9_SGPIO3         LPC_PIN_ID(15, 9, 7, 23, 6) /* not present on LQFP144 */

#define LPC_PIN_PF_10_U0_TXD        LPC_PIN_ID(15, 10, 7, 24, 1) /* not present on LQFP144 */
#define LPC_PIN_PF_10_GPIO7_24      LPC_PIN_ID(15, 10, 7, 24, 4) /* not present on LQFP144 */
#define LPC_PIN_PF_10_SD_WP         LPC_PIN_ID(15, 10, 7, 24, 6) /* not present on LQFP144 */

#define LPC_PIN_PF_11_U0_RXD        LPC_PIN_ID(15, 11, 7, 25, 1) /* not present on LQFP144 */
#define LPC_PIN_PF_11_GPIO7_25      LPC_PIN_ID(15, 11, 7, 25, 4) /* not present on LQFP144 */
#define LPC_PIN_PF_11_SD_VOLT2      LPC_PIN_ID(15, 11, 7, 25, 6) /* not present on LQFP144 */

/* CLK */
#define LPC_PIN_CLK0_EMC_CLK0       LPC_PIN_ID(0, 0, 8, 0, 0)
#define LPC_PIN_CLK0_CLKOUT         LPC_PIN_ID(0, 0, 8, 0, 1)
#define LPC_PIN_CLK0_SD_CLK         LPC_PIN_ID(0, 0, 8, 0, 4)
#define LPC_PIN_CLK0_EMC_CLK01      LPC_PIN_ID(0, 0, 8, 0, 5)
#define LPC_PIN_CLK0_SSP1_CLK       LPC_PIN_ID(0, 0, 8, 0, 6)
#define LPC_PIN_CLK0_ENET_TX_CLK    LPC_PIN_ID(0, 0, 8, 0, 7) /* a.k.a. ENET_REF_CLK */

#define LPC_PIN_CLK1_EMC_CLK1       LPC_PIN_ID(0, 1, 8, 0, 0) /* not present on LQFP144 */
#define LPC_PIN_CLK1_CLKOUT         LPC_PIN_ID(0, 1, 8, 0, 1) /* not present on LQFP144 */
#define LPC_PIN_CLK1_CGU_OUT0       LPC_PIN_ID(0, 1, 8, 0, 5) /* not present on LQFP144 */
#define LPC_PIN_CLK1_I2S1_TX_MCLK   LPC_PIN_ID(0, 1, 8, 0, 7) /* not present on LQFP144 */

#define LPC_PIN_CLK2_EMC_CLK3       LPC_PIN_ID(0, 2, 8, 0, 0)
#define LPC_PIN_CLK2_CLKOUT         LPC_PIN_ID(0, 2, 8, 0, 1)
#define LPC_PIN_CLK2_SD_CLK         LPC_PIN_ID(0, 2, 8, 0, 4)
#define LPC_PIN_CLK2_EMC_CLK23      LPC_PIN_ID(0, 2, 8, 0, 5)
#define LPC_PIN_CLK2_I2S0_TX_MCLK   LPC_PIN_ID(0, 2, 8, 0, 6)
#define LPC_PIN_CLK2_I2S1_RX_SCK    LPC_PIN_ID(0, 2, 8, 0, 7)

#define LPC_PIN_CLK3_EMC_CLK2       LPC_PIN_ID(0, 3, 8, 0, 0) /* not present on LQFP144 */
#define LPC_PIN_CLK3_CLKOUT         LPC_PIN_ID(0, 3, 8, 0, 1) /* not present on LQFP144 */
#define LPC_PIN_CLK3_CGU_OUT1       LPC_PIN_ID(0, 3, 8, 0, 5) /* not present on LQFP144 */
#define LPC_PIN_CLK3_I2S1_RX_SCK    LPC_PIN_ID(0, 3, 8, 0, 7) /* not present on LQFP144 */
/*================================================================================================*/

#endif /* LPC_PIN_H_ */
