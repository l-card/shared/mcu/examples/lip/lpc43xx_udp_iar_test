#include "board.h"

#include "chip.h"
#include "timer.h"
#include "lip.h"
#include "lpc_bits.h"
#include "lpc_pin.h"
#include "examples/udp_cntr/lip_sample_udp_cntr.h"
#include "examples/phy_loop/lip_sample_phy_loop.h"

#define BLINK_PIN   LPC_PIN_P2_5_GPIO5_5

//#define LOOPBACK_TST

#define UDP_PORT  11115

static const uint8_t mac[LIP_MAC_ADDR_SIZE] = {0x00, 0x05, 0xF7, 0xFF, 0xFF, 0xFE };
static const uint8_t ip[LIP_IPV4_ADDR_SIZE] = {192,168,12,250};
static const uint8_t mask[LIP_IPV4_ADDR_SIZE] = {255,255,255,0 };
static const uint8_t gate[LIP_IPV4_ADDR_SIZE] = {192,168,12,1};
static const uint8_t use_dhcp = 1;

//===================================================================================
// �������� ������� ����������
//===================================================================================
void main(void)
{
      // ������� �������� ������� ������ ����
      SystemCoreClock = Chip_Clock_GetRate(CLK_MX_MXCORE);
      
      /* ���� ip=NULL - ������������ DHCP, ����� - ���������� �������� ��������� IP */  
      lip_sample_udp_cntr_init(mac, use_dhcp ? NULL : ip, mask, gate, UDP_PORT);
      // ������ ����        
      while(true)
      {
          lip_sample_udp_cntr_pull();	
      }
}
